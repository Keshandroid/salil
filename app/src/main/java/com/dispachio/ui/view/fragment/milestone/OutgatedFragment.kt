package com.dispachio.ui.view.fragment.milestone

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.Intent.getIntent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dispachio.R
import com.dispachio.data.model.request.MileStoneRequest
import com.dispachio.data.model.response.loadSearch.LoadsByDistanceResponseItem
import com.dispachio.databinding.OutgatedBinding
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.base.toast
import com.dispachio.ui.view.activity.NavigationActivity
import com.dispachio.ui.viewmodel.DashboardViewModel
import com.dispachio.utils.MapCallBacks
import com.dispachio.utils.Utility
import com.example.dispachio.common.Status
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.here.android.mpa.common.GeoBoundingBox
import com.here.android.mpa.common.GeoCoordinate
import com.here.android.mpa.common.OnEngineInitListener
import com.here.android.mpa.mapping.Map
import com.here.android.mpa.mapping.MapRoute
import com.here.android.mpa.mapping.PositionIndicator
import com.here.android.mpa.routing.*
import com.here.android.mpa.venues3d.VenueMapFragment
import com.here.android.mpa.venues3d.VenueService


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [OutgatedFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class OutgatedFragment : BaseFragment<OutgatedBinding>(R.layout.outgated) {

    private val dashboardViewModel: DashboardViewModel by activityViewModels()
    private var freshBatteryData: LoadsByDistanceResponseItem? = null
    protected var localBroadcastManager: LocalBroadcastManager? = null

    //HERE map
    private val RUNTIME_PERMISSIONS = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.INTERNET,
        Manifest.permission.ACCESS_WIFI_STATE,
        Manifest.permission.ACCESS_NETWORK_STATE
    )
    private val TAG = "VenuesAndLogging.StartRoutingFragment"
    private var mLastMapCenter: GeoCoordinate? = null
    private val REQUEST_CODE_ASK_PERMISSIONS = 1
    private var m_geoBoundingBox: GeoBoundingBox? = null
    private var m_route: Route? = null
    private var mVenueMapFragment: VenueMapFragment? = null
    private lateinit var mMap: Map

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        localBroadcastManager = activity?.let { LocalBroadcastManager.getInstance(it) }

        arguments?.let {
            freshBatteryData = it.getParcelable(ARG_PARAM1)!!
        }
    }


    companion object {

        private const val LOCATION_PERMISSION_REQUEST_CODE = 1

        @JvmStatic
        fun newInstance(param1: LoadsByDistanceResponseItem?) =
            OutgatedFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM1, param1)
                }
            }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        val f = mActivity.fragmentManager.findFragmentById(R.id.routing_map) as VenueMapFragment?
        if (f != null) mActivity.fragmentManager.beginTransaction().remove(f).commit()

    }

    private fun hasPermissions(context: Context, permissions: Array<String>) : Boolean {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
            for ( permission: String in permissions){
                if (ActivityCompat.checkSelfPermission(context, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true
    }

    private fun getMapFragment(): VenueMapFragment? {
        return mActivity.fragmentManager.findFragmentById(R.id.outgatedMap) as VenueMapFragment?
    }

    private fun startVenueMaps(){
        mVenueMapFragment =  getMapFragment()

        mVenueMapFragment?.init({ error ->
            if (error == OnEngineInitListener.Error.NONE) {
                com.dispachio.utils.Log.v(
                    TAG,
                    "InitializeVenueMaps: OnEngineInitializationCompleted"
                )

                //mVenueService = mVenueMapFragment?.venueService
                //mRoutingController = mVenueMapFragment?.routingController
                /*if (mRoutingController != null) {
                    mRoutingController?.addListener(this)
                }*/
                // Setting venue service content based on menu option
                /*if (!mPrivateVenues) {
                    setVenueServiceContent(false, false) // Public only
                } else {
                    setVenueServiceContent(true, true) // Private + public
                }
                m_navigationManager = NavigationManager.getInstance()*/

            } else {
                AlertDialog.Builder(context).setMessage(
                    """
                            Error : ${error.name}
                            
                            ${error.details}
                            """.trimIndent()
                )
                    .setTitle(R.string.engine_init_error)
                    .setNegativeButton(android.R.string.cancel,
                        DialogInterface.OnClickListener { dialog, which -> mActivity.finish() })
                    .create().show()
            }
        }) { result ->
            com.dispachio.utils.Log.v(
                TAG,
                "VenueServiceListener: OnInitializationCompleted with result: %s",
                result
            )
            when (result) {
                VenueService.InitStatus.IN_PROGRESS -> {
                    com.dispachio.utils.Log.v(
                        TAG,
                        "Initialization of venue service is in progress..."
                    )
                    Toast.makeText(
                        context,
                        "Initialization of venue service is in progress...",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                VenueService.InitStatus.OFFLINE_SUCCESS, VenueService.InitStatus.ONLINE_SUCCESS -> {
                    // Adding venue listener to map fragment
                    //mVenueMapFragment!!.addListener(this)
                    // Set animations on for floor change and venue entering
                    mVenueMapFragment!!.setFloorChangingAnimation(true)
                    mVenueMapFragment!!.setVenueEnteringAnimation(true)
                    // Ask notification when venue visible; this notification is
                    // part of VenueMapFragment.VenueListener
                    mVenueMapFragment!!.setVenuesInViewportCallback(true)

                    // Add Gesture Listener for map fragment
                    //mVenueMapFragment?.mapGesture?.addOnGestureListener(this, 0, false)

                    // retrieve a reference of the map from the map fragment
                    mMap = mVenueMapFragment?.map!!
                    //mMap.addTransformListener(this)
                    mMap.zoomLevel = (mMap.maxZoomLevel - 3).toDouble()


                    // Start of Position Updates
                    try {
                        //startPositionUpdates()
                        mVenueMapFragment!!.positionIndicator.isVisible = true
                    } catch (ex: Exception) {
                        com.dispachio.utils.Log.w(
                            TAG,
                            "startPositionUpdates: Could not register for location updates: %s",
                            com.dispachio.utils.Log.getStackTraceString(ex)
                        )
                    }
                    if (mLastMapCenter == null) {
                        mMap.setCenter(
                            GeoCoordinate(
                                61.497961,
                                23.763606,
                                0.0
                            ), Map.Animation.NONE
                        )
                    } else {
                        mMap.setCenter(
                            mLastMapCenter!!,
                            Map.Animation.NONE
                        )
                    }

                    //display indicator

                    // Set positioning indicator visible
                    val positionIndicator: PositionIndicator = mMap.getPositionIndicator()
                    positionIndicator.isVisible = true

                    createRoute()
                }
            }
        }
    }

    private  fun createRoute() {

        val coreRouter = CoreRouter()
        val routePlan = RoutePlan()
        val routeOptions = RouteOptions()
        /* Other transport modes are also available e.g Pedestrian */
        routeOptions.transportMode = RouteOptions.TransportMode.CAR
        /* Disable highway in this route. */
        routeOptions.setHighwaysAllowed(false)
        /* Calculate the shortest route available. */
        routeOptions.routeType = RouteOptions.Type.SHORTEST
        /* Calculate 1 route. */
        routeOptions.routeCount = 1
        /* Finally set the route option */
        routePlan.routeOptions = routeOptions


        com.dispachio.utils.Log.d("LocationData","=Pickup Latitude : "+freshBatteryData?.pickupLatitude)
        com.dispachio.utils.Log.d("LocationData","=Pickup Longitude : "+freshBatteryData?.pickupLongitude)
        com.dispachio.utils.Log.d("LocationData","=Delivery Latitude : "+freshBatteryData?.deliveryLatitude)
        com.dispachio.utils.Log.d("LocationData","=Delivery Longitude : "+freshBatteryData?.deliveryLongitude)
        com.dispachio.utils.Log.d("LocationData","Pickup => Delivery : "+freshBatteryData?.pickupLocationAddress + " ::: " + freshBatteryData?.deliveryLocationAddress)
        com.dispachio.utils.Log.d("LocationData","Pickup = Delivery : "+freshBatteryData?.pickupLocationName + " ::: " + freshBatteryData?.deliveryLocationName)


        if (freshBatteryData?.pickupLatitude != null && freshBatteryData?.pickupLongitude != null &&
            freshBatteryData?.deliveryLatitude != null && freshBatteryData?.deliveryLongitude != null) {
            var startPoint: RouteWaypoint? = null
            var destination: RouteWaypoint? = null

            startPoint = RouteWaypoint(
                GeoCoordinate(
                    freshBatteryData?.pickupLatitude!!,
                    freshBatteryData?.pickupLongitude!!
                )
            )
            destination = RouteWaypoint(
                GeoCoordinate(
                    freshBatteryData?.deliveryLatitude!!,
                    freshBatteryData?.deliveryLongitude!!
                )
            )
            routePlan.addWaypoint(startPoint)
            routePlan.addWaypoint(destination)

            coreRouter
                .calculateRoute(
                    routePlan,
                    object : Router.Listener<List<RouteResult>, RoutingError> {
                        override fun onProgress(i: Int) {
                            // The calculation progress can be retrieved in this callback.
                        }

                        override fun onCalculateRouteFinished(
                            routeResults: List<RouteResult>,
                            routingError: RoutingError
                        ) {
                            if (routingError == RoutingError.NONE) {
                                if (routeResults[0].route != null) {
                                    m_route = routeResults[0].route
                                    val mapRoute = MapRoute(
                                        routeResults[0].route
                                    )

                                    //Show the maneuver number on top of the route
                                    mapRoute.isManeuverNumberVisible = true

                                    // Add the MapRoute to the map
                                    mMap.addMapObject(mapRoute)


                                    m_geoBoundingBox = routeResults[0].route.boundingBox
                                    m_geoBoundingBox?.let {
                                        mMap.zoomTo(
                                            it, Map.Animation.NONE,
                                            Map.MOVE_PRESERVE_ORIENTATION
                                        )
                                    }
                                    //mDataBinding.stopCtrlButton.setVisibility(View.VISIBLE)
                                    //startNavigation()
                                } else {
                                    Toast.makeText(
                                        mActivity,
                                        "Error:route results returned is not valid",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            } else {
                                Toast.makeText(
                                    mActivity, "Error:route calculation returned error code: "
                                            + routingError,
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    })
        }

    }

    override fun initView() {

        /*val mapFragment =
            childFragmentManager.findFragmentById(R.id.outgatedMap) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)*/

        mVenueMapFragment =  getMapFragment()
        setView()


        mDataBinding.confirmBtn.setOnClickListener { view: View? ->


            var path = dashboardViewModel.getPicturePath()
            var userid = dashboardViewModel.getUserId()

            var fileName: String? = null

            if (path != null) {
                fileName = userid + "_outgated1_" + System.currentTimeMillis()
                Utility.sendProfileImage(mActivity, path, fileName)
            }

            var mileStoneRequest = MileStoneRequest()
            mileStoneRequest.firstImageName = fileName
            mileStoneRequest.milestoneStatusId = 4
            mileStoneRequest.latitude = freshBatteryData?.pickupLatitude
            mileStoneRequest.longitude = freshBatteryData?.pickupLongitude
            mileStoneRequest.curDate = freshBatteryData?.pickupDate
            mileStoneRequest.curTime = freshBatteryData?.pickupTime
            mileStoneRequest.loadId = freshBatteryData?.loadId


            dashboardViewModel.milestonesCall(mileStoneRequest)
            initObserver()

        }
        mDataBinding.uploadImgBtn.setOnClickListener { view: View? ->
            Utility.openGallery(mActivity)
        }


        //HERE map implmentation

        //mVenueMapFragment = getMapFragment()
        com.dispachio.utils.Log.mEnabled = true

        // checking dynamically controlled permissions
        if (context?.let { hasPermissions(it, RUNTIME_PERMISSIONS) } == true) {
            startVenueMaps()
        } else {
            ActivityCompat
                .requestPermissions(
                    mActivity,
                    RUNTIME_PERMISSIONS,
                    REQUEST_CODE_ASK_PERMISSIONS
                )
        }

    }

    private val callback = OnMapReadyCallback { googleMap ->
        val startingPoint = freshBatteryData?.pickupLatitude?.let {
            freshBatteryData?.pickupLongitude?.let { it1 ->
                LatLng(
                    it,
                    it1
                )
            }
        }

        val destinationPoint = freshBatteryData?.deliveryLatitude?.let {
            freshBatteryData?.deliveryLongitude?.let { it1 ->
                LatLng(
                    it,
                    it1
                )
            }
        }
        googleMap.addMarker(destinationPoint?.let {
            MarkerOptions().position(it).title(freshBatteryData?.deliveryLocationName)
        })

        val path: MutableList<LatLng> = ArrayList()

        //Draw the polyline

        if (path.size > 0) {
            val opts = PolylineOptions().addAll(path).color(Color.BLUE).width(5f)
            googleMap.addPolyline(opts)
        }

        // Getting URL to the Google Directions API
        MapCallBacks(mActivity, object : MapCallBacks.OnMapScreenChanges {
            override fun routeStarted(lineOptions: PolylineOptions) {
                googleMap.addPolyline(lineOptions)
            }

        }).getUrl(startingPoint!!, destinationPoint!!)

        googleMap.uiSettings.isZoomControlsEnabled = true
        googleMap.mapType = GoogleMap.MAP_TYPE_HYBRID
        //   googleMap.mapType = GoogleMap.MAP_TYPE_TERRAIN
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(startingPoint, 6f))

    }

    private fun setView() {
        mDataBinding.data = freshBatteryData
    }

    private fun initObserver() {
        dashboardViewModel.milestonesModel.observe(viewLifecycleOwner, {

            when (it.status) {
                Status.SUCCESS -> {
                    /*mActivity.addFragments(
                        MilestoneFragment.newInstance(freshBatteryData, 4),
                        R.id.dashboardContainer,
                        true
                    )*/
                    notifyData(freshBatteryData,4)
                    (activity as NavigationActivity).onBackPressed()
                }
                Status.ERROR -> {
                    mActivity.toast(it.message.toString())
                }
            }
        })
    }

    protected fun notifyData(freshBatteryData: LoadsByDistanceResponseItem?, i: Int) {
        val intent = Intent(NOTIFY_MILESTONE_FRAGMENT)
        intent.putExtra(ARG_PARAM1,i)
        localBroadcastManager?.sendBroadcast(intent)
    }

}