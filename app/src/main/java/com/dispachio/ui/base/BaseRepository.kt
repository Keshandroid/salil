package com.dispachio.ui.base


import android.content.Context
import com.dispachio.R
import com.dispachio.utils.APIResponseErrorCode
import com.google.gson.JsonParser
import org.json.JSONException
import retrofit2.Call
import retrofit2.Response
import timber.log.Timber


/**
 * Created by 1000292 on 06-10-2020
 *
 * PUNE.
 */

abstract class BaseRepository {

    /**
     * Common response execute function
     * if it is type of 'BaseResponseModel<T>'
     * And you need only result as response
     */
    fun <P> execute(
        context: Context,
        response: Response<BaseResponseModel<P>>,
        success: (payload: P) -> Unit,
        fail: (error: String) -> Unit
    ) {
        try {
            if (response.isSuccessful) {
                response.body()?.let { body_ ->
                    body_.entity?.let { results_ ->
                        when (body_.status ?: -1) {
                            APIResponseErrorCode.ErrorCode_100.errorCodeValue -> {
                                success.invoke(results_)
                            }
                            else -> {
                                fail.invoke(
                                    body_.message
                                        ?: context.getString(R.string.error_something_went_wrong)
                                )
                            }
                        }

                    } ?: kotlin.run {
                        fail.invoke(
                            body_.message ?: context.getString(R.string.error_something_went_wrong)
                        )
                    }
                } ?: kotlin.run {
                    fail.invoke(context.getString(R.string.error_something_went_wrong))
                }
            } else {
                fail.invoke(context.getString(R.string.error_something_went_wrong))
            }
        } catch (e: Exception) {
            Timber.e(e)
            fail.invoke(context.getString(R.string.error_something_went_wrong))
        }

    }

    /**
     * Common response execute function
     * if it is of type of Call<BaseResponseModel<T>>?'
     * And you need only result as response
     * when 100 is success
     */
    fun <P> execute(
        call: Call<BaseResponseModel<P>>,
        success: (payload: P) -> Unit,
        fail: (error: String) -> Unit,
        context: Context
    ) {
        call.enqueue {
            onResponse = { response_ ->
                try {
                    if (response_.isSuccessful) {
                        response_.body()?.let { body_ ->
                            body_.entity?.let { entity ->
                                when (body_.status ?: -1) {
                                    APIResponseErrorCode.ErrorCode_100.errorCodeValue -> {
                                        success.invoke(entity)
                                    }
                                    else -> {
                                        fail.invoke(
                                            body_.message
                                                ?: context.getString(R.string.error_something_went_wrong)
                                        )
                                    }
                                }
                            } ?: kotlin.run {
                                fail.invoke(
                                    body_.message
                                        ?: context.getString(R.string.error_something_went_wrong)
                                )
                            }
                        } ?: kotlin.run {
                            fail.invoke(context.getString(R.string.error_something_went_wrong))
                        }
                    } else {
                        try {
                            val errorJsonString = response_.errorBody()?.string()
                            var message = JsonParser().parse(errorJsonString)
                                .asJsonObject["message"]
                                .asString
                            if (message.isEmpty()) {
                                message = JsonParser().parse(errorJsonString)
                                    .asJsonObject["error"]
                                    .asString
                            }
                            fail.invoke(message)

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                    }
                } catch (e: Exception) {
                    Timber.e(e)
                    fail.invoke(context.getString(R.string.error_something_went_wrong))
                }
            }

            onFailure = {
                Timber.e(it)
                fail.invoke(context.getString(R.string.error_something_went_wrong))
            }
        }
    }

    /**
     * Common response execute function
     * if it is of type of Call<BaseResponseModel<T>>?'
     * And you need only result as response
     * when success i dyanamic
     */
    fun <P> execute(
        call: Call<BaseResponseModel<P>>,
        success: (payload: P) -> Unit,
        fail: (error: String) -> Unit,
        context: Context,
        code: Int
    ) {
        call.enqueue {
            onResponse = { response_ ->
                try {
                    if (response_.isSuccessful) {
                        response_.body()?.let { body_ ->
                            body_.entity?.let { results_ ->
                                when (body_.status ?: -1) {
                                    code -> {
                                        success.invoke(results_)
                                    }
                                    else -> {
                                        fail.invoke(
                                            body_.message
                                                ?: context.getString(R.string.error_something_went_wrong)
                                        )
                                    }
                                }
                            } ?: kotlin.run {
                                fail.invoke(
                                    body_.message
                                        ?: context.getString(R.string.error_something_went_wrong)
                                )
                            }
                        } ?: kotlin.run {
                            fail.invoke(context.getString(R.string.error_something_went_wrong))
                        }
                    } else {
                        fail.invoke(context.getString(R.string.error_something_went_wrong))
                    }
                } catch (e: Exception) {
                    Timber.e(e)
                    fail.invoke(context.getString(R.string.error_something_went_wrong))
                }
            }

            onFailure = {
                Timber.e(it)
                fail.invoke(context.getString(R.string.error_something_went_wrong))
            }
        }
    }
}