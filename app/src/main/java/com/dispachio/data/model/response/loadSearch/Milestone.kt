package com.dispachio.data.model.response.loadSearch


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Milestone(
    @SerializedName("answer1")
    val answer1: Int?=null,
    @SerializedName("answer2")
    val answer2: Int?=null,
    @SerializedName("answer5")
    val answer5: String?=null,
    @SerializedName("curDate")
    val curDate: String?=null,
    @SerializedName("curTime")
    val curTime: String?=null,
    @SerializedName("firstImageName")
    val firstImageName: String?=null,
    @SerializedName("isCompleted")
    val isCompleted: Boolean?=null,
    @SerializedName("latitude")
    val latitude: Double?=null,
    @SerializedName("loadId")
    val loadId: Int?=null,
    @SerializedName("longitude")
    val longitude: Double?=null,
    @SerializedName("milestoneId")
    val milestoneId: Int?=null,
    @SerializedName("milestoneStatus")
    val milestoneStatus: MilestoneStatus?=null,
    @SerializedName("milestoneStatusId")
    val milestoneStatusId: Int?=null,
    @SerializedName("question1")
    val question1: Int?=null,
    @SerializedName("question2")
    val question2: Int?=null,
    @SerializedName("question5")
    val question5: Int?=null
) : Parcelable