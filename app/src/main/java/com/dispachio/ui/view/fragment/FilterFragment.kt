package com.dispachio.ui.view.fragment

import android.Manifest
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Parcelable
import android.widget.DatePicker
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dispachio.R
import com.dispachio.data.model.response.loadSearch.LoadsByDistanceResponse
import com.dispachio.databinding.FragmentFilterBinding
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.base.addFragments
import com.dispachio.ui.base.replaceFragment
import com.dispachio.ui.base.toast
import com.dispachio.ui.view.activity.NavigationActivity
import com.dispachio.ui.view.fragment.milestone.NOTIFY_MILESTONE_FRAGMENT
import com.dispachio.ui.viewmodel.DashboardViewModel
import com.dispachio.utils.Log
import com.dispachio.utils.setSafeOnClickListener
import com.example.dispachio.common.Status
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.here.android.mpa.common.*
import com.here.android.mpa.common.PositioningManager.*
import java.lang.ref.WeakReference
import java.text.SimpleDateFormat
import java.util.*

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class FilterFragment : BaseFragment<FragmentFilterBinding>(R.layout.fragment_filter) {

    private val dashboardViewModel: DashboardViewModel by activityViewModels()
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var lastKnownLocation: Location? = null

    //HERE map
    private val TAG = "FilterFragment"

    private var mPositioningManager: PositioningManager? = null
    private var mGoogleLocation: LocationDataSourceGoogleServices? = null
    private var mLocationMethod: LocationMethod? = null
    private var paused: Boolean = false
    var lattitude: Double? = null
    var longitude: Double? = null
    private var mLastReceivedPosition: GeoPosition? = null


    //HERE map
    private val positionListener: OnPositionChangedListener = object : OnPositionChangedListener {
        override fun onPositionUpdated(method: LocationMethod, position: GeoPosition?, isMapMatched: Boolean) {
            if (!paused) {
                mLastReceivedPosition = position
                Log.d("CurrentLocation",""+ position!!.coordinate)
            }
        }

        override fun onPositionFixChanged(method: LocationMethod, status: LocationStatus) {
        }
    }

    companion object {

        private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1

        @JvmStatic
        fun newInstance() =
            FilterFragment().apply {
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun initView() {
        getLocationPermission()
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mActivity)

        //HERE map
        startPositioningUpdates()
//        getInstance().addListener(WeakReference<OnPositionChangedListener>(positionListener))

        var lastKnownLocation = getDeviceLocation()

        mDataBinding.dopEdt.setSafeOnClickListener {
            val c = Calendar.getInstance()
            val date =
                DatePickerDialog.OnDateSetListener { view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                    c.set(Calendar.YEAR, year)
                    c.set(Calendar.MONTH, monthOfYear)
                    c.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    val myFormat = "yyyy-MM-dd" //In which you need put here

                    val sdf = SimpleDateFormat(myFormat, Locale.US)

                    mDataBinding.dopEdt.setText(sdf.format(c.time))
                }


            val datePickerDialog = context?.let { it1 ->
                DatePickerDialog(
                    it1, R.style.DialogTheme, date, c
                        .get(Calendar.YEAR), c.get(Calendar.MONTH),
                    c.get(Calendar.DAY_OF_MONTH)
                )
            }

         /*   c.add(Calendar.YEAR, -18)
            datePickerDialog?.datePicker?.maxDate = c.timeInMillis*/


            c.add(Calendar.YEAR, 0)
            c.add(Calendar.DAY_OF_MONTH, 0)
            c.add(Calendar.MONTH, 0)
            datePickerDialog?.datePicker?.minDate = c.timeInMillis

            datePickerDialog?.show()
        }

        mDataBinding.dodEdt.setSafeOnClickListener {
            val c = Calendar.getInstance()
            val date =
                DatePickerDialog.OnDateSetListener { view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                    c.set(Calendar.YEAR, year)
                    c.set(Calendar.MONTH, monthOfYear)
                    c.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    val myFormat = "yyyy-MM-dd" //In which you need put here

                    val sdf = SimpleDateFormat(myFormat, Locale.US)

                    mDataBinding.dodEdt.setText(sdf.format(c.time))
                }


            val datePickerDialog = context?.let { it1 ->
                DatePickerDialog(
                    it1, R.style.DialogTheme, date, c
                        .get(Calendar.YEAR), c.get(Calendar.MONTH),
                    c.get(Calendar.DAY_OF_MONTH)
                )
            }

        /*    c.add(Calendar.YEAR, -18)
            datePickerDialog?.datePicker?.maxDate = c.timeInMillis*/


            c.add(Calendar.YEAR, 0)
            c.add(Calendar.DAY_OF_MONTH, 0)
            c.add(Calendar.MONTH, 0)
            datePickerDialog?.datePicker?.minDate = c.timeInMillis

            datePickerDialog?.show()
        }
        mDataBinding.applyBtn.setSafeOnClickListener {

            //Google Location code
            /*var lattitude: Double? = null
            var longitude: Double? = null
            if (lastKnownLocation?.latitude != null
            ) {
                lattitude = lastKnownLocation.latitude
                longitude = lastKnownLocation.longitude
            }*/


            //HERE map last location
            if(mLastReceivedPosition!=null){
                val receivedCoordinate = mLastReceivedPosition!!.coordinate
                lattitude = receivedCoordinate.latitude
                longitude = receivedCoordinate.longitude
            }

            //    mDataBinding.autoCompleteEditText.setText("${lattitude.toString()} , ${longitude.toString()}")
            val distance = when (mDataBinding.loadByDistanceSp.selectedItemPosition) {
                0 -> {
                    5
                }
                1 -> {
                    10
                }
                2 -> {
                    15
                }
                3 -> {
                    20
                }
                else -> null
            }

            val containerID = when (mDataBinding.terminalType.selectedItemPosition) {
                0 -> {
                    1
                }
                1 -> {
                    2
                }
                else -> null
            }

            val loadTypeId = when (mDataBinding.typeOfLoadEdt.selectedItemPosition) {
                0 -> {
                    1
                }
                1 -> {
                    2
                }
                else -> null
            }
            val deliveryTypeId = when (mDataBinding.deliveryTypeSp.selectedItemPosition) {
                0 -> {
                    1
                }
                1 -> {
                    2
                }
                else -> null
            }

            val minRate = when (mDataBinding.minEdt.selectedItemPosition) {
                0 -> {
                    1
                }
                1 -> {
                    2
                }
                else -> null
            }

            val maxRate = when (mDataBinding.maxEdt.selectedItemPosition) {
                0 -> {
                    1
                }
                1 -> {
                    2
                }
                else -> null
            }
            dashboardViewModel.applyFilter(
                lattitude,
                longitude,
                distance,
                mDataBinding.dopEdt.text?.toString(),
                mDataBinding.dodEdt.text?.toString(),
                containerID,
                loadTypeId,
                deliveryTypeId,
                minRate,
                maxRate
            )

        }

        initObserver()
    }

    private fun startPositioningUpdates() {

        Log.mEnabled = true
        paused = false;

        val mapEngine = MapEngine.getInstance()
        val appContext = context?.let { ApplicationContext(it) }
        appContext?.let {
            mapEngine.init(it) { error ->
                if (error == OnEngineInitListener.Error.NONE) {
                    // Post initialization code goes here

                    Log.v(TAG, "Start of Positioning Updates")

                    getInstance().addListener(WeakReference<OnPositionChangedListener>(positionListener))


                    mPositioningManager = PositioningManager.getInstance()
                    if (mPositioningManager == null) {
                        Log.w(TAG, "startPositionUpdates: PositioningManager is null")
                    }else{
                        //Google location datasource
                        mGoogleLocation = LocationDataSourceGoogleServices.getInstance()
                        if (mGoogleLocation != null) {
                            mPositioningManager!!.dataSource = mGoogleLocation
                            //mPositioningManager?.addListener(WeakReference(this))
                            mPositioningManager?.addListener(
                                WeakReference<PositioningManager.OnPositionChangedListener>(
                                    positionListener
                                )
                            )
                            mLocationMethod = LocationMethod.GPS_NETWORK
                            if (mPositioningManager!!.start(mLocationMethod)) {
                                Log.e(
                                    TAG,
                                    "startPositionUpdates: Position updates started successfully."
                                )
                            }
                        }
                    }
                } else {
                    // handle factory initialization failure
                    Log.w(TAG, "startPositionUpdates: Error occurs")

                }
            }
        }


    }

    private fun initObserver() {

        dashboardViewModel.applyFilterModel.observe(viewLifecycleOwner, {
            if(getViewLifecycleOwner().getLifecycle().getCurrentState()== Lifecycle.State.RESUMED){
                when (it.status) {
                    Status.SUCCESS -> {
                        /*mActivity.replaceFragment(
                            DashboardFragment.newInstance(it.data),
                            R.id.containerView
                        )*/
                        (activity as NavigationActivity).onBackPressed()
                    }
                    Status.ERROR -> {
                        mActivity.toast(it.message.toString())
                    }
                }
            }





            /*notifyData(it.data)
            (activity as NavigationActivity).onBackPressed()*/
        }
        )
    }



    private var locationPermissionGranted = false

    private fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(
                mActivity.applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            locationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(
                mActivity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    private fun getDeviceLocation(): Location? {

        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (locationPermissionGranted) {
                val locationResult = fusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(mActivity) { task ->
                    if (task.isSuccessful) {
                        // Set the map's camera position to the current location of the device.
                        lastKnownLocation = task.result
                    }
                }
            }
        } catch (e: SecurityException) {
            //Log.e("Exception: %s", e.message, e)
        }
        return lastKnownLocation
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        if (mPositioningManager != null) {
            mPositioningManager!!.stop();
        }

        super.onPause()
        paused = true;

    }

    override fun onDestroy() {
        if (mPositioningManager != null) {
            // Cleanup
            mPositioningManager!!.removeListener(
                positionListener);
        }
        super.onDestroy()
    }

}