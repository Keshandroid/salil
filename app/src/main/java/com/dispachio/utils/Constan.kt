package com.dispachio.utils

const val PICK_CAMERA_IMAGE = 1002
const val PICK_GALLERY_IMAGE = 1001
const val OPERATION_CODE = "operation_code"
const val SELECTED_IMAGE = "selected_url"
const val FILE_TYPE=".jpg"
const val MEDIA_TYPE="image/jpeg"
const val FORM_DATA_NAME="PHOTO"
const val PREF_CUSTOMER_ID = "CUSTOMER_ID"
const val PREF_TOKEN = "TOKEN"
const val PREF_USER_NAME = "USER_NAME"
const val PREFS_USER_ID = "PREFS_USER_ID"
const val PREFS_PWD = "PREFS_PWD"
const val PREFS_IS_ACTIVE = "PREFS_IS_ACTIVE"
const val PREFS_EMAIL_ID = "PREFS_EMAIL_ID"
const val PREFS_PROFILE_COMPLETED = "PROFILE_COMPLETED"
const val PREFS_PICTURE_PATH = "PICTURE_PATH"
const val PREFS_PICTURE_PATH1 = "PICTURE_PATH"
const val PREFS_PICTURE_PATH2 = "PICTURE_PATH"
const val PREFS_OTP = "OTP"
const val PREFS_CONTACT_NO = "CONTACT_NO"

enum class APIResponseErrorCode(val errorCodeValue: Int) {
    ErrorCode_100(1),
    ErrorCode_101(101),
    ErrorCode_104(104),
    ErrorCode_200(200)
}


enum class AWS_URL(val url: String) {
    AWS_ACCESS_KEY("AKIA4ABLM7YRTIIAYBUJ"),
    AWS_SECRET_KEY("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"),
    IMAGE_PATH_TO_STORE("document-picture"),
    BUCKET_NAME("dispachio")
}

enum class CompanyType(val value: String) {
    CORPORATION("CORPORATION"),
    LLC("LLC"),
    DBA("DBA"),
    CONTRACTOR("CONTRACTOR"),
    SOLE("SOLE");

    override fun toString() : String {
        return value
    }
}
