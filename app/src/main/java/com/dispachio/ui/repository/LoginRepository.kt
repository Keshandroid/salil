package com.dispachio.ui.repository

import android.content.Context
import com.dispachio.data.ApiCalls
import com.dispachio.data.model.request.LoginRequest
import com.dispachio.data.model.request.RegistrationRequestModel
import com.dispachio.data.model.response.RegisterResponceModel
import com.dispachio.data.model.response.login.LoginResponse
import com.dispachio.ui.base.BaseRepository
import com.dispachio.utils.SharedPreference
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class LoginRepository @Inject constructor(
    private val loginRegisterApi: ApiCalls,
    private val sharedPreference: SharedPreference,
    @ApplicationContext private val context: Context
) : BaseRepository() {
    fun setRegistrationDetail(
        registrationRequestModel: RegistrationRequestModel,
        success: (registrationResponceModel: RegisterResponceModel) -> Unit,
        fail: (error: String) -> Unit
    ) {
        loginRegisterApi.firstRgsCall(registrationRequestModel).apply {
            execute(this, success, fail, context)
        }
    }

    fun completeRegisCall(
        registrationRequestModel: RegistrationRequestModel,
        success: (registrationResponceModel: RegisterResponceModel) -> Unit,
        fail: (error: String) -> Unit
    ) {
        loginRegisterApi.completeRegisCall(registrationRequestModel).apply {
            execute(this, success, fail, context)
        }
    }

    fun updateCarrierProfileData(
        registrationRequestModel: RegistrationRequestModel,
        success: (registrationResponceModel: RegisterResponceModel) -> Unit,
        fail: (error: String) -> Unit
    ) {
        loginRegisterApi.updateCarrierProfileData(registrationRequestModel).apply {
            execute(this, success, fail, context)
        }
    }

    fun loginCAll(
        request: LoginRequest,
        success: (response: LoginResponse) -> Unit,
        fail: (error: String) -> Unit
    ) {
        loginRegisterApi.loginCall(request).apply {
            execute(this, success, fail, context)
        }
    }

    fun resetCall(
        request: LoginRequest,
        success: (response: LoginResponse) -> Unit,
        fail: (error: String) -> Unit
    ) {
        loginRegisterApi.forgotPasswordCall(request).apply {
            execute(this, success, fail, context)
        }
    }

    fun saveUserData(
        userId: String,
        password: String?,
        active: Boolean,
        email: String?,
        name: String?,
        profileCompleted: Boolean,
        contactNumber: String?
    ) {
        sharedPreference.saveUserID(userId)
        sharedPreference.savePassword(password)
        sharedPreference.saveIsActive(active)
        sharedPreference.saveEmailId(email)
        sharedPreference.setUserName(name)
        sharedPreference.isProfileCompleted(profileCompleted)
        sharedPreference.saveContactNumber(contactNumber)
    }


    fun isProfileCompleted(profileCompleted:Boolean) {
         sharedPreference.isProfileCompleted(profileCompleted)
    }

    fun getUserId():String? {
        return sharedPreference.getUserID()
    }

    fun otpChecked(): String? {
        return sharedPreference.otpChecked()
    }

    fun otpSent(otp: String?) {
        sharedPreference.otpSent(otp)
    }

    fun savePicturePath(picturePath: String?) {
        sharedPreference.savePicturePath(picturePath)
    }

    fun getPicturePath(): String? {
        return sharedPreference.getPicturePath()
    }

    fun savePicturePath1(picturePath: String?) {
        sharedPreference.savePicturePath1(picturePath)
    }

    fun getPicturePath1(): String? {
        return sharedPreference.getPicturePath1()
    }

    fun savePicturePath2(picturePath: String?) {
        sharedPreference.savePicturePath2(picturePath)
    }

    fun getPicturePath2(): String? {
        return sharedPreference.getPicturePath2()
    }

}