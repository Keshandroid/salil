package com.dispachio.data.model.response.loadSearch


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MileStoneResponse(
    @SerializedName("accessorialSetupIdList")
    val accessorialSetupIdList: String,
    @SerializedName("accessorialSetupList")
    val accessorialSetupList: String,
    @SerializedName("bookingNo")
    val bookingNo: String,
    @SerializedName("carrierPrice")
    val carrierPrice: Double,
    @SerializedName("chassisType")
    val chassisType: ChassisType,
    @SerializedName("chassisTypeId")
    val chassisTypeId: Int,
    @SerializedName("commissionPercentage")
    val commissionPercentage: String,
    @SerializedName("containerId")
    val containerId: Int,
    @SerializedName("containerNumber")
    val containerNumber: String,
    @SerializedName("deliveryDate")
    val deliveryDate: String,
    @SerializedName("deliveryLatitude")
    val deliveryLatitude: Double,
    @SerializedName("deliveryLocationAddress")
    val deliveryLocationAddress: String,
    @SerializedName("deliveryLocationName")
    val deliveryLocationName: String,
    @SerializedName("deliveryLongitude")
    val deliveryLongitude: Double,
    @SerializedName("deliveryTime")
    val deliveryTime: String,
    @SerializedName("deliveryType")
    val deliveryType: DeliveryType,
    @SerializedName("deliveryTypeId")
    val deliveryTypeId: Int,
    @SerializedName("height")
    val height: Height,
    @SerializedName("heightId")
    val heightId: Int,
    @SerializedName("isNotifyAccessorials")
    val isNotifyAccessorials: Boolean,
    @SerializedName("isNotifyMilestones")
    val isNotifyMilestones: Boolean,
    @SerializedName("isProfileCompleted")
    val isProfileCompleted: String,
    @SerializedName("length")
    val length: Length,
    @SerializedName("lengthId")
    val lengthId: Int,
    @SerializedName("loadContainer")
    val loadContainer: LoadContainer,
    @SerializedName("loadCreated")
    val loadCreated: String,
    @SerializedName("loadId")
    val loadId: Int,
    @SerializedName("loadStatus")
    val loadStatus: LoadStatus,
    @SerializedName("loadStatusId")
    val loadStatusId: Int,
    @SerializedName("loadType")
    val loadType: LoadType,
    @SerializedName("loadTypeId")
    val loadTypeId: Int,
    @SerializedName("milestones")
    val milestones: List<Milestone>?=null,
    @SerializedName("notifyEmail")
    val notifyEmail: String,
    @SerializedName("pickupDate")
    val pickupDate: String,
    @SerializedName("pickupLatitude")
    val pickupLatitude: Double,
    @SerializedName("pickupLocationAddress")
    val pickupLocationAddress: String,
    @SerializedName("pickupLocationName")
    val pickupLocationName: String,
    @SerializedName("pickupLongitude")
    val pickupLongitude: Double,
    @SerializedName("pickupTime")
    val pickupTime: String,
    @SerializedName("price")
    val price: Double,
    @SerializedName("truckingUserId")
    val truckingUserId: Int
) : Parcelable