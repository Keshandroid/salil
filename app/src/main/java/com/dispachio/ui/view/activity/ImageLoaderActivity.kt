package com.dispachio.ui.view.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import com.dispachio.R
import com.dispachio.databinding.ActivityDocumentOperationBinding
import com.dispachio.ui.base.*
import com.dispachio.ui.view.fragment.CropFragment
import com.dispachio.utils.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import timber.log.Timber

class ImageLoaderActivity :
    BaseActivity<ActivityDocumentOperationBinding>(R.layout.activity_document_operation),
    DocumentListener {

    private var REQUEST_CODE: Int? = null
    private var mToolbarTitle: String? = null
    private var mFileName: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    override fun initView() {
        /* mDataBinding.includeToolbar.toolbar.background =
             ContextCompat.getDrawable(this, R.color.mfp_dark_three)*/
        REQUEST_CODE = intent.getIntExtra(OPERATION_CODE, 0)
        /*setUpToolBar(
            mDataBinding.includeToolbar.toolbar,
            (intent?.getStringExtra(TOOLBAR_TITLE) ?: "").also {
                mToolbarTitle = it
            },
            mDataBinding.includeToolbar.toolbarTitle
        )*/
        pickImage()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                PICK_GALLERY_IMAGE -> {
                    data?.data?.let {
                        openCropFragment(it, PICK_GALLERY_IMAGE)
                    } ?: kotlin.run {
                        Timber.e("Uri null")
                    }
                }
                PICK_CAMERA_IMAGE -> {
                    try {
                        openCropFragment(
                            Utility.getCacheImagePath(this@ImageLoaderActivity, mFileName!!),
                            PICK_CAMERA_IMAGE
                        )
                    } catch (e: Exception) {
                        Timber.e("Uri null")
                    }
                }
                else -> {
                    this@ImageLoaderActivity.finish()
                }
            }
        } else {
            this@ImageLoaderActivity.finish()
        }
    }

    private fun openCropFragment(uri: Uri?, code: Int) {
        CropFragment.newInstance().apply {
            arguments= Bundle().apply {
                putParcelable(SELECTED_IMAGE,uri)
                putInt(OPERATION_CODE,code)
            }
        }.also {
            addFragments(it,R.id.dashboardContainer,true)
        }
    }

    override fun retake(requestCode: Int) {
        REQUEST_CODE = requestCode
        pickImage()
    }


    override fun done(uri: Uri?) {
        Intent().apply {
            data = uri
        }.also {
            setResult(Activity.RESULT_OK, it)
            this@ImageLoaderActivity.finish()
        }

    }

    private fun pickImage() {
        Dexter.withActivity(this).withPermissions(
            listOf(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        ).withListener(object : MultiplePermissionsListener {
            override fun onPermissionsChecked(report: MultiplePermissionsReport?) = report?.let {
                if (it.areAllPermissionsGranted()) {
                    when (REQUEST_CODE) {
                        PICK_GALLERY_IMAGE -> {
                            startActivityForResult(Intent.createChooser(Intent().apply {
                                type = "image/*"
                                action = Intent.ACTION_GET_CONTENT
                            }, "Select Picture"), PICK_GALLERY_IMAGE)
                        }
                        PICK_CAMERA_IMAGE -> {
                            mFileName = "${System.currentTimeMillis()}".plus(FILE_TYPE)
                            startActivityForResult(Intent(MediaStore.ACTION_IMAGE_CAPTURE).apply {
                                putExtra(
                                    MediaStore.EXTRA_OUTPUT,
                                    Utility.getCacheImagePath(this@ImageLoaderActivity, mFileName!!)
                                )
                                putExtra(MediaStore.EXTRA_SIZE_LIMIT, 10 * 1024 * 1024)
                            }, PICK_CAMERA_IMAGE)
                        }
                    }
                }

                if (it.isAnyPermissionPermanentlyDenied) {
                    openPermissionSettings()
                }
            } ?: kotlin.run {
                toast("Error occurred! ")
            }

            override fun onPermissionRationaleShouldBeShown(
                permissions: MutableList<PermissionRequest>?,
                token: PermissionToken?
            ) {
                token?.continuePermissionRequest()
            }
        })
            .withErrorListener { toast("Error occurred! ") }
            .onSameThread()
            .check()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            popBackStack()
        } else {
            this@ImageLoaderActivity.finish()
        }
    }


}