package com.dispachio.ui.di

import com.dispachio.data.DashboardApi
import com.dispachio.ui.base.BaseActivityModule
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import retrofit2.Retrofit

@Module(includes = [BaseActivityModule::class])
@InstallIn(ActivityComponent::class)
object DashboardFragmentModuleDi {
    @Provides
    fun provideLoginRegisterApi(retrofit: Retrofit): DashboardApi =
        retrofit.create(DashboardApi::class.java)
}