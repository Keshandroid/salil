package com.dispachio.ui.view.activity

import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import com.dispachio.R
import com.dispachio.databinding.ActivityBrowsePictureBinding
import com.dispachio.ui.base.BaseActivity
import com.dispachio.ui.base.toast
import java.lang.Exception


class BrowsePictureActivity :
    BaseActivity<ActivityBrowsePictureBinding>(R.layout.activity_browse_picture) {
    private val SELECT_PICTURE = 1

    private var selectedImagePath: String? = null


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                val selectedImageUri: Uri? = data?.data
                selectedImagePath = getPath(selectedImageUri)
            }
        }
    }

    /**
     * helper to retrieve the path of an image URI
     */
    fun getPath(uri: Uri?): String? {
        // just some safety built in
        if (uri == null) {
            // TODO perform some logging or show user feedback
            return null
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor: Cursor? = managedQuery(uri, projection, null, null, null)
        if (cursor != null) {
            val column_index: Int = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            val path: String = cursor.getString(column_index)
            cursor.close()
            return path
        }
        // this is our fallback here
        return uri.path
    }

    override fun initView() {
        try {
            mDataBinding.button.setOnClickListener {
                // in onCreate or any event where your want the user to
                // select a file
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(
                    Intent.createChooser(
                        intent,
                        "Select Picture"
                    ), SELECT_PICTURE
                )
            }
        }catch (e: Exception)
        {
            toast(e.printStackTrace().toString())
        }
    }

}