package com.dispachio.data.model.response.loadSearch

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class LoadsByDistanceResponse : ArrayList<LoadsByDistanceResponseItem>(), Parcelable