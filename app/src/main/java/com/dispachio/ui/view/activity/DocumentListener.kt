package com.dispachio.ui.view.activity

import android.net.Uri

interface DocumentListener {
    fun retake(requestCode:Int)
    fun done(uri: Uri?)
}
