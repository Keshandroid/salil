package com.dispachio.utils


import android.app.Activity
import android.app.Dialog
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.telephony.SmsManager
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.URLSpan
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.exifinterface.media.ExifInterface
import com.dispachio.R
import okhttp3.MediaType
import okhttp3.RequestBody
import timber.log.Timber
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.random.Random


/**
 * Created by 1000292 on 09-10-2020
 *
 * PUNE.
 */
object Utility {

    /**
     * get bitmap from uri
     */
    fun getBitmap(context: Context, imageUri: Uri): Bitmap? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            ImageDecoder.decodeBitmap(
                ImageDecoder.createSource(
                    context.contentResolver,
                    imageUri
                )
            )
        } else {
            context
                .contentResolver
                .openInputStream(imageUri)?.use { inputStream ->
                    BitmapFactory.decodeStream(inputStream)
                }
        }
    }

    /**
     * useful to create cache directory
     */
    fun getCacheImagePath(context: Context, fileName: String): Uri? {
        val path = File(context.externalCacheDir, "camera")
        if (!path.exists()) path.mkdirs()
        val image = File(path, fileName)
        return FileProvider.getUriForFile(
            context,
            "${context.packageName}.provider",
            image
        )
    }


    /**
     * store bitmap in external cache and get Uri
     */
    fun getCacheImagePath(context: Context, fileName: String, bitmap: Bitmap): Uri? {
        val path = File(context.externalCacheDir, "camera")
        if (!path.exists()) path.mkdirs()
        val image = File(path, fileName)
        try {
            FileOutputStream(image).apply {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, this)
                flush()
                close()
            }
        } catch (e: FileNotFoundException) {
            Timber.e(e)
        } catch (e: IOException) {
            Timber.e(e)
        } catch (e: Exception) {
            Timber.e(e)
        }
        Timber.d("Actual size ${(image.length() / (1024))} KB")
        return FileProvider.getUriForFile(
            context,
            "${context.packageName}.provider",
            image
        ).also {
            File(getFileProviderFilePath(context, it)).apply {
                "After crop ${(this.length() / (1024))} KB".let { fileSize ->
                    Timber.d(fileSize)
                }
            }
        }
    }

    /**
     * Calling this will delete the images from cache directory
     * useful to clear some memory
     */
    fun clearCache(context: Context) {
        val path = File(context.externalCacheDir, "camera")
        if (path.exists() && path.isDirectory) {
            for (child in path.listFiles()!!) {
                child.delete()
            }
        }
    }

    /**
     * Get file path from uri
     */
    fun getFileProviderFilePath(context: Context, uri: Uri): String {
        return (File(
            File(context.externalCacheDir, "camera"),
            queryName(context.contentResolver, uri) ?: ""
        ).path).also {
            Timber.d(it)
        }
    }

    /**
     * Get file  from uri
     */
    fun getFileProviderFile(context: Context, uri: Uri): File? {
        return (File(
            File(context.externalCacheDir, "camera"),
            queryName(context.contentResolver, uri) ?: ""
        )).also {
            Timber.d(it.path)
        }
    }

    /**
     * Get file name from uri
     */
    fun queryName(resolver: ContentResolver, uri: Uri): String? {
        val returnCursor: Cursor = resolver.query(uri, null, null, null, null)!!
        val nameIndex: Int = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        val name: String = returnCursor.getString(nameIndex)
        returnCursor.close()
        return name
    }

    /**
     * Get image rotation angle
     */
    fun exifRotation(filePath: String): Int {
        return try {
            when (ExifInterface(filePath).getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL
            )) {
                ExifInterface.ORIENTATION_ROTATE_270 -> 270
                ExifInterface.ORIENTATION_ROTATE_180 -> 180
                ExifInterface.ORIENTATION_ROTATE_90 -> 90
                else -> 0
            }
        } catch (e: Exception) {
            Timber.e(e)
            0
        }.also {
            Timber.d(it.toString())
        }
    }


    fun validate(fields: ArrayList<String>): Boolean {
        for (i in fields.indices) {
            val currentField = fields[i]
            if (currentField.length <= 0) {
                return false
            }
        }
        return true
    }

    /**
     * useful to get date suffix
     */
    fun getDateSuffix(day: Int): String {
        return if (day in 4..20) "th"
        else when (day % 10) {
            1 -> "st"
            2 -> "nd"
            3 -> "rd"
            else -> "th"
        }
    }


    /**
     * useful to get text drawable
     */
    fun getSymbol(context: Context, symbol: String, textSize: Float, color: Int): Drawable {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.textSize = textSize
        paint.color = color
        paint.textAlign = Paint.Align.LEFT
        val baseline = -paint.ascent()
        val width = (paint.measureText(symbol) + 0.5f)
        val height = (baseline + paint.descent() + 0.5f)
        val image = Bitmap.createBitmap(width.toInt(), height.toInt(), Bitmap.Config.ARGB_8888)
        val canvas = Canvas(image)
        canvas.drawText(symbol, 0F, baseline, paint)
        return BitmapDrawable(context.resources, image)
    }


    /**
     * Create Plain requestBody
     */
    fun String.createPlainRequestBody(): RequestBody =
        RequestBody.create(MediaType.parse("text/plain"), this)


    /**
     * Use for common progress bar
     * @param context => not a application context
     * @return Dialog => dialog obj
     */
    fun showCommonProgressDialog(context: Context): Dialog {
        val views: View = LayoutInflater.from(context).inflate(R.layout.item_progress_bar, null)
        return Dialog(context).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawableResource(android.R.color.transparent)
            setContentView(views)
            setCancelable(false)
        }
    }

    fun getmin(vararg input: Double): Double {
        var smallest = Double.MAX_VALUE
        for (itm in input) {
            if (itm > 0.0 && itm < smallest) {
                smallest = itm
            }
        }
        return smallest
    }


    fun getTittleCaseString(text: String): String {
        return text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase()
    }

    fun getGenderCodeFromValue(gender: String, context: Context): String {
        var gender = ""
        when (gender) {
            "Male" -> gender = "M"
            "Female" -> gender = "F"
            "Others" -> gender = "O"
        }
        return gender
    }

    fun getGenderValueFromCode(gender: String, context: Context): String {
        var gender = ""
        when (gender) {
            "M" -> gender = "Male"
            "F" -> gender = "Female"
            "O" -> gender = "Others"
        }
        return gender
    }

    fun getMaritalStatusCodeFromValue(marital: String, context: Context): String {
        var marital = ""
        when (marital) {
            "Male" -> marital = "M"
            "Female" -> marital = "F"
            "Others" -> marital = "O"
        }
        return marital
    }

    fun getMaritalStatusValueFromCode(marital: String, context: Context): String {
        var marital = ""
        when (marital) {
            "M" -> marital = "Male"
            "F" -> marital = "Female"
            "O" -> marital = "Others"
        }
        return marital
    }

    fun getOccupationCodeFromValue(occupation: String, context: Context): String {
        var occupation = ""
        when (occupation) {
            "Business" -> occupation = "01"
            "Forex Dealer" -> occupation = "43"
            "Private Sector Service" -> occupation = "41"
            "Public Sector Service" -> occupation = "42"
            "Government Service" -> occupation = "44"
            "Other Service" -> occupation = "02"
            "Doctor" -> occupation = "09"
            "Professional" -> occupation = "03"
            "Agriculturist" -> occupation = "04"
            "Retired" -> occupation = "05"
            "Housewife" -> occupation = "06"
            "Student" -> occupation = "07"
            "Others" -> occupation = "08"
            "Unknown" -> occupation = "99"
            "Not Applicable" -> occupation = "99"
        }
        return occupation
    }

    fun getOccupationValueFromCode(occupation: String, context: Context): String {
        var occupation = ""
        when (occupation) {
            "01" -> occupation = "Business"
            "43" -> occupation = "Forex Dealer"
            "41" -> occupation = "Private Sector Service"
            "42" -> occupation = "Public Sector Service"
            "44" -> occupation = "Government Service"
            "02" -> occupation = "Other Service"
            "09" -> occupation = "Doctor"
            "03" -> occupation = "Professional"
            "04" -> occupation = "Agriculturist"
            "05" -> occupation = "Retired"
            "06" -> occupation = "Housewife"
            "07" -> occupation = "Student"
            "08" -> occupation = "Others"
            "99" -> occupation = "Unknown"
            "99" -> occupation = "Not Applicable"
        }
        return occupation
    }

    fun getCountryCodeFromValue(country: String, context: Context): String {
        var country = ""
        when (country) {
            "India" -> country = "IN"
            "United Arab Emirates" -> country = "AE"
            "United States of America" -> country = "US"
            "Singapore" -> country = "SG"
            "United Kingdom" -> country = "GB"
            "Canada" -> country = "CA"
            "Nepal" -> country = "NP"
            "Myanmar" -> country = "MM"
            "Australia" -> country = "AU"
            "New Zealand" -> country = "NZ"
            "GermString" -> country = "DE"
            "Andorra" -> country = "AD"
            "Afghanistan" -> country = "AF"
            "Anguilla" -> country = "AI"
            "Antigua and Barbuda" -> country = "AG"
            "Albania" -> country = "AL"
            "Algeria" -> country = "AZ"
            "Angola" -> country = "AO"
            "Antarctica" -> country = "AQ"
            "Aland Islands" -> country = "AX"
            "Argentina" -> country = "AR"
            "Armenia" -> country = "AM"
            "Aruba" -> country = "AW"
            "Austria" -> country = "AI"
            "Azerbaijan" -> country = "AZ"
            "Bahamas" -> country = "BS"
            "Bahrain" -> country = "BH"
            "Bangladesh" -> country = "BD"
            "Barbados" -> country = "BB"
            "Belarus" -> country = "BY"
            "Belgium" -> country = "BE"
            "Belize" -> country = "BZ"
            "Benin" -> country = "BJ"
            "Saint Barthélemy" -> country = "BL"
            "Bermuda" -> country = "BM"
            "Bhutan" -> country = "BT"
            "Bolivia" -> country = "BO"
            "Bosnia and Herzegovina" -> country = "BA"
            "Botswana" -> country = "BW"
            "Brazil" -> country = "BR"
            "Brunei" -> country = "BN"
            "Bulgaria" -> country = "BG"
            "Burkina Faso" -> country = "BF"
            "Burundi" -> country = "BI"
            "Bonaire, Sint Eustatius and Saba" -> country = "BQ"
            "Bouvet Island" -> country = "BV"
            "Cocos (Keeling) Islands" -> country = "CC"
            "Cameroon" -> country = "CM"
            "Cape Verde" -> country = "CV"
            "Cayman Islands" -> country = "KY"
            "Central African Republic" -> country = "CF"
            "Chad" -> country = "TD"
            "Chile" -> country = "CL"
            "China" -> country = "CN"
            "Colombia" -> country = "CO"
            "Cambodia" -> country = "KH"
            "Comoros" -> country = "KM"
            "Congo" -> country = "CG"
            "Cook Islands" -> country = "CK"
            "Costa Rica" -> country = "CR"
            "Côte d'Ivoire" -> country = "CI"
            "Croatia" -> country = "HR"
            "Cuba" -> country = "CU"
            "Curacao" -> country = "CW"
            "Christmas Island" -> country = "CX"
            "Cyprus" -> country = "CY"
            "Czech Republic" -> country = "CZ"
            "Denmark" -> country = "DK"
            "Djibouti" -> country = "DJ"
            "Dominica" -> country = "DM"
            "Dominican Republic" -> country = "DO"
            "East Timor" -> country = "TL"
            "Ecuador" -> country = "EC"
            "Egypt" -> country = "EG"
            "Western Sahara" -> country = "EH"
            "Eritrea" -> country = "ER"
            "El Salvador" -> country = "SV"
            "Equatorial Guinea" -> country = "GQ"
            "Estonia" -> country = "EE"
            "Ethiopia" -> country = "ET"
            "Falkland Islands" -> country = "FK"
            "Fiji" -> country = "FJ"
            "Finland" -> country = "FI"
            "Micronesia" -> country = "FM"
            "Faroe Islands" -> country = "FO"
            "France" -> country = "FR"
            "French Guiana" -> country = "GF"
            "French Polynesia" -> country = "PF"
            "Gabon" -> country = "GA"
            "Gambia" -> country = "GM"
            "Georgia" -> country = "GE"
            "Ghana" -> country = "GH"
            "Gibraltar" -> country = "GI"
            "Greece" -> country = "GR"
            "Greenland" -> country = "GL"
            "Grenada" -> country = "GD"
            "Guadeloupe" -> country = "GP"
            "South Georgia and the South Sandwich Islands" -> country = "GS"
            "Guam" -> country = "GU"
            "Guatemala" -> country = "GT"
            "Guernsey" -> country = "GG"
            "Guinea" -> country = "GN"
            "Guinea-Bissau" -> country = "GW"
            "Guyana" -> country = "GY"
            "Haiti" -> country = "HT"
            "Honduras" -> country = "HN"
            "Hong Kong" -> country = "HK"
            "Heard Island and McDonald Islands" -> country = "HM"
            "Hungary" -> country = "HU"
            "Iceland" -> country = "IS"
            "Indonesia" -> country = "ID"
            "Iran" -> country = "IR"
            "Iraq" -> country = "IQ"
            "Ireland" -> country = "IE"
            "Isle of Man" -> country = "IM"
            "British Indian Ocean Territory" -> country = "IO"
            "Israel" -> country = "IL"
            "Italy" -> country = "IT"
            "Jamaica" -> country = "JM"
            "Japan" -> country = "JP"
            "Jersey" -> country = "JE"
            "Jordan" -> country = "JO"
            "Kazakhstan" -> country = "KZ"
            "Kenya" -> country = "KE"
            "Kiribati" -> country = "KI"
            "Kuwait" -> country = "KW"
            "Kyrgyzstan" -> country = "KG"
            "Laos" -> country = "LA"
            "Liechtenstein" -> country = "LI"
            "Latvia" -> country = "LV"
            "Lebanon" -> country = "LB"
            "Lesotho" -> country = "LS"
            "Liberia" -> country = "LR"
            "Libya" -> country = "LY"
            "Lithuania" -> country = "LT"
            "Luxembourg" -> country = "LU"
            "Monaco" -> country = "MC"
            "Montenegro" -> country = "ME"
            "Saint Martin" -> country = "MF"
            "Macedonia" -> country = "MK"
            "Madagascar" -> country = "MG"
            "Marshall Islands" -> country = "MH"
            "Macao" -> country = "MO"
            "Malawi" -> country = "MW"
            "Malaysia" -> country = "MY"
            "Maldives" -> country = "MV"
            "Mali" -> country = "ML"
            "Malta" -> country = "MT"
            "Mauritania" -> country = "MR"
            "Mauritius" -> country = "MU"
            "Mexico" -> country = "MX"
            "Moldova" -> country = "MD"
            "Mongolia" -> country = "MN"
            "Northern Mariana Islands" -> country = "MP"
            "Martinique" -> country = "MQ"
            "Montserrat" -> country = "MS"
            "Morocco" -> country = "MA"
            "Mozambique" -> country = "MZ"
            "Namibia" -> country = "NA"
            "Netherlands" -> country = "NL"
            "Netherlands Antilles" -> country = "AN"
            "New Caledonia" -> country = "NC"
            "Nicaragua" -> country = "NI"
            "Niger" -> country = "NE"
            "Norfolk Island" -> country = "NF"
            "Nigeria" -> country = "NG"
            "North Korea" -> country = "KP"
            "Norway" -> country = "NO"
            "Nauru" -> country = "NR"
            "Niue" -> country = "NU"
            "Oman" -> country = "OM"
            "Pakistan" -> country = "PK"
            "Panama" -> country = "PA"
            "Papua New Guinea" -> country = "PG"
            "Paraguay" -> country = "PY"
            "Peru" -> country = "PE"
            "Philippines" -> country = "PH"
            "Poland" -> country = "PL"
            "Saint Pierre and Miquelon" -> country = "PM"
            "Pitcairn" -> country = "PN"
            "Puerto Rico" -> country = "PR"
            "Occupied Palestinian Territories" -> country = "PS"
            "Portugal" -> country = "PT"
            "Palau" -> country = "PW"
            "Qatar" -> country = "QA"
            "Réunion Island" -> country = "RE"
            "Romania" -> country = "RO"
            "Serbia" -> country = "RS"
            "Russia" -> country = "RU"
            "Rwanda" -> country = "RW"
            "São Tomé and Príncipe" -> country = "ST"
            "Saudi Arabia" -> country = "SA"
            "Senegal" -> country = "SN"
            "Seychelles" -> country = "SC"
            "Svalbard and Jan Mayen" -> country = "SJ"
            "Slovakia" -> country = "SK"
            "Sierra Leone" -> country = "SL"
            "San Marino" -> country = "SM"
            "Slovenia" -> country = "SI"
            "Solomon Islands" -> country = "SB"
            "Somalia" -> country = "SO"
            "South Africa" -> country = "ZA"
            "South Korea" -> country = "KR"
            "Spain" -> country = "ES"
            "Sri Lanka" -> country = "LK"
            "Saint Helena, Ascension and Tristan da Cunha" -> country = "SH"
            "Saint Kitts And Nevis" -> country = "KN"
            "Saint Vincent and the Grenadines" -> country = "VC"
            "Saint Lucia" -> country = "LC"
            "Sudan" -> country = "SD"
            "Suriname" -> country = "SR"
            "South Sudan" -> country = "SS"
            "Sint Maarten" -> country = "SX"
            "Swaziland" -> country = "SZ"
            "Sweden" -> country = "SE"
            "Switzerland" -> country = "CH"
            "Syria" -> country = "SY"
            "Turks and Caicos Islands" -> country = "TC"
            "French Southern and Antarctic Lands" -> country = "TF"
            "Taiwan" -> country = "TW"
            "Tajikistan" -> country = "TJ"
            "Tokelau" -> country = "TK"
            "Tanzania" -> country = "TZ"
            "Thailand" -> country = "TH"
            "Togo" -> country = "TG"
            "Tonga" -> country = "TO"
            "Trinidad And Tobago" -> country = "TT"
            "Tunisia" -> country = "TN"
            "Turkey" -> country = "TR"
            "Turkmenistan" -> country = "TM"
            "Tuvalu" -> country = "TV"
            "Ukraine" -> country = "UA"
            "Uganda" -> country = "UG"
            "United States Minor Outlying Islands" -> country = "UM"
            "Uruguay" -> country = "UY"
            "Uzbekistan" -> country = "UZ"
            "Vatican City State" -> country = "VA"
            "British Virgin Islands" -> country = "VG"
            "United States Virgin Islands" -> country = "VI"
            "Vanuatu" -> country = "VU"
            "Venezuela" -> country = "VE"
            "Vietnam" -> country = "VN"
            "Wallis and Futuna" -> country = "WF"
            "Western Samoa" -> country = "WS"
            "American Samoa" -> country = "AS"
            "Yemen" -> country = "YE"
            "Mayotte" -> country = "YT"
            "Zaire" -> country = "CD"
            "Zambia" -> country = "ZM"
            "Zimbabwe" -> country = "ZW"
            "Not categorized" -> country = "XX"
            "Others" -> country = "ZZ"
        }
        return country
    }

    fun getCountryValueFromCode(country: String, context: Context): String {
        var country = ""
        when (country) {
            "IN" -> country = "India"
            "AE" -> country = "United Arab Emirates"
            "US" -> country = "United States of America"
            "SG" -> country = "Singapore"
            "GB" -> country = "United Kingdom"
            "CA" -> country = "Canada"
            "NP" -> country = "Nepal"
            "MM" -> country = "Myanmar"
            "AU" -> country = "Australia"
            "NZ" -> country = "New Zealand"
            "DE" -> country = "GermString"
            "AD" -> country = "Andorra"
            "AF" -> country = "Afghanistan"
            "AI" -> country = "Anguilla"
            "AG" -> country = "Antigua and Barbuda"
            "AL" -> country = "Albania"
            "AZ" -> country = "Algeria"
            "AO" -> country = "Angola"
            "AQ" -> country = "Antarctica"
            "AX" -> country = "Aland Islands"
            "AR" -> country = "Argentina"
            "AM" -> country = "Armenia"
            "AW" -> country = "Aruba"
            "AI" -> country = "Austria"
            "AZ" -> country = "Azerbaijan"
            "BS" -> country = "Bahamas"
            "BH" -> country = "Bahrain"
            "BD" -> country = "Bangladesh"
            "BB" -> country = "Barbados"
            "BY" -> country = "Belarus"
            "BE" -> country = "Belgium"
            "BZ" -> country = "Belize"
            "BJ" -> country = "Benin"
            "BL" -> country = "Saint Barthélemy"
            "BM" -> country = "Bermuda"
            "BT" -> country = "Bhutan"
            "BO" -> country = "Bolivia"
            "BA" -> country = "Bosnia and Herzegovina"
            "BW" -> country = "Botswana"
            "BR" -> country = "Brazil"
            "BN" -> country = "Brunei"
            "BG" -> country = "Bulgaria"
            "BF" -> country = "Burkina Faso"
            "BI" -> country = "Burundi"
            "BQ" -> country = "Bonaire, Sint Eustatius and Saba"
            "BV" -> country = "Bouvet Island"
            "CC" -> country = "Cocos (Keeling) Islands"
            "CM" -> country = "Cameroon"
            "CV" -> country = "Cape Verde"
            "KY" -> country = "Cayman Islands"
            "CF" -> country = "Central African Republic"
            "TD" -> country = "Chad"
        }
        return country
    }

    fun TextView.handleUrlClicks(onClicked: ((String) -> Unit)? = null) {
        //create span builder and replaces current text with it
        text = SpannableStringBuilder.valueOf(text).apply {
            //search for all URL spans and replace all spans with our own clickable spans
            getSpans(0, length, URLSpan::class.java).forEach {
                //add new clickable span at the same position
                setSpan(
                    object : ClickableSpan() {
                        override fun onClick(widget: View) {
                            onClicked?.invoke(it.url)
                        }
                    },
                    getSpanStart(it),
                    getSpanEnd(it),
                    Spanned.SPAN_INCLUSIVE_EXCLUSIVE
                )
                //remove old URLSpan
                removeSpan(it)
            }
        }
        //make sure movement method is set
        movementMethod = LinkMovementMethod.getInstance()
    }

    fun openLinkToBrowser(context: Context, url: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        context.startActivity(browserIntent)
    }

    fun sendProfileImage(context: Context, picturePath: String?, s: String) {
        /* network = NetworkHelper(context)
         if (network.isNetworkConnected()) {*/
        val path = File(picturePath)
        if (path.exists()) {
            if (path != null) {
                val file = File(java.lang.String.valueOf(path))
                FileUpload.uploadtos3(
                    context,
                    file,
                    true,
                    s + "." + context.resources.getString(R.string.jpg)
                )
            }
        } else {
            Toast.makeText(
                context,
                context.resources.getString(R.string.path_not_exist),
                Toast.LENGTH_SHORT
            ).show()
        }

    }

    private val GET_FROM_GALLERY = 3

    fun openGallery(context: Context) {


        try {
            val builder = StrictMode.VmPolicy.Builder()
            StrictMode.setVmPolicy(builder.build())
            val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
            (context as Activity).startActivityForResult(i, GET_FROM_GALLERY)

        } catch (e: Exception) {
            Timber.e(e.printStackTrace().toString())
        }
    }

    fun openCamera(context: Context) {
        val packageManager: PackageManager = context.packageManager
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            val mainDirectory = File(context.getExternalFilesDir(null).toString(), "/Dispachio")
            if (!mainDirectory.exists()) mainDirectory.mkdirs()
            val calendar: Calendar = Calendar.getInstance()
            val builder = StrictMode.VmPolicy.Builder()
            StrictMode.setVmPolicy(builder.build())
            val uriFilePath = Uri.fromFile(File(mainDirectory, "IMG_" + calendar.timeInMillis))
            var intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uriFilePath)
            (context as Activity).startActivityForResult(intent, 1)
        }
    }

    fun generateOTP(): String {
        val random = Random
        return String.format("%04d", random.nextInt(10000))
    }

    fun sendLongSMS(number: String): String {
        val otp = generateOTP()
        val message = otp
        val smsManager = SmsManager.getDefault()
        val parts = smsManager.divideMessage(message)
        smsManager.sendMultipartTextMessage(number, null, parts, null, null)
        return otp
    }
}

