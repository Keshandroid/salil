package com.dispachio.data.model.response.loadSearch


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ChassisType(
    @SerializedName("chassisTypeId")
    val chassisTypeId: Int,
    @SerializedName("chassisTypeValue")
    val chassisTypeValue: String
) : Parcelable