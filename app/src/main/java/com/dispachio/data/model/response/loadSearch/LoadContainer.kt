package com.dispachio.data.model.response.loadSearch


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoadContainer(
    @SerializedName("containerId")
    val containerId: Int,
    @SerializedName("containerValue")
    val containerValue: String
) : Parcelable