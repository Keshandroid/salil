package com.dispachio.ui.repository

import android.content.Context
import com.dispachio.data.DashboardApi
import com.dispachio.data.model.request.ApplyForLoadRequest
import com.dispachio.data.model.request.MileStoneRequest
import com.dispachio.data.model.request.PODRequest
import com.dispachio.data.model.response.ApplyForLoadResponse
import com.dispachio.data.model.response.LedgerListResponse
import com.dispachio.data.model.response.PODResponse
import com.dispachio.data.model.response.loadSearch.LoadListResponse
import com.dispachio.data.model.response.loadSearch.LoadsByDistanceResponse
import com.dispachio.data.model.response.loadSearch.LoadsByIdResponse
import com.dispachio.data.model.response.loadSearch.MileStoneResponse
import com.dispachio.ui.base.BaseRepository
import com.dispachio.utils.Log
import com.dispachio.utils.SharedPreference
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class DashboardRepository @Inject constructor(
    private val apiCall: DashboardApi,
    private val sharedPreference: SharedPreference,
    @ApplicationContext private val context: Context
) : BaseRepository() {


    fun loadByIDCall(
        success: (response: LoadsByIdResponse) -> Unit,
        fail: (error: String) -> Unit
    ) {
        //TODO change as dynamic data
        var userID = sharedPreference.getUserID()?.toInt()
        apiCall.getLoadByIdCall(2).apply {
            execute(this, success, fail, context)
        }
    }


    fun getLoadListIdCall(
        request: Int,
        success: (response: LoadListResponse) -> Unit,
        fail: (error: String) -> Unit
    ) {
        apiCall.getLoadListIdCall(request).apply {
            execute(this, success, fail, context)
        }
    }

    fun milestonesCall(
        request: MileStoneRequest,
        success: (response: MileStoneResponse) -> Unit,
        fail: (error: String) -> Unit
    ) {
        apiCall.milestonesCall(request).apply {
            execute(this, success, fail, context)
        }
    }

    fun applyForLoads(
        request: ApplyForLoadRequest,
        success: (response: ApplyForLoadResponse) -> Unit,
        fail: (error: String) -> Unit
    ) {



        apiCall.applyForLoads(request).apply {
            execute(this, success, fail, context)
        }
    }

    fun createPOD(
        request: PODRequest,
        success: (response: PODResponse) -> Unit,
        fail: (error: String) -> Unit
    ) {
        apiCall.createPOD(request).apply {
            execute(this, success, fail, context)
        }
    }

    fun getAllLedgerList(
        userID: Int,
        success: (response: LedgerListResponse) -> Unit,
        fail: (error: String) -> Unit
    ) {
        apiCall.getAllLedgerList(userID).apply {
            execute(this, success, fail, context)
        }
    }

    fun getLoadsByDistanceCall(
        latitude: Double,
        longitude: Double,
        distance: Int,
        success: (response: LoadsByDistanceResponse) -> Unit,
        fail: (error: String) -> Unit
    ) {
        apiCall.getLoadsByDistanceCall(latitude, longitude, distance).apply {
            execute(this, success, fail, context)
        }
    }

    fun getApprovalLoads(
        userId: Int,
        loadStatusId: Int,
        success: (response: LoadsByDistanceResponse) -> Unit,
        fail: (error: String) -> Unit
    ) {
        apiCall.getApprovalLoads(userId, loadStatusId).apply {
            execute(this, success, fail, context)
        }
    }

    fun applyForFilter(
        latitude: Double?,
        longitude: Double?,
        distance: Int?,
        pickupDate: String?,
        deliveryDate: String?,
        containerId: Int?,
        loadTypeId: Int?,
        deliveryTypeId: Int?,
        minRate: Int?,
        maxRate: Int?,
        success: (response: LoadsByDistanceResponse) -> Unit,
        fail: (error: String) -> Unit
    ) {
        apiCall.applyFilter(
            latitude,
            longitude,
            distance,
            pickupDate,
            deliveryDate,
            containerId,
            loadTypeId,
            deliveryTypeId,
            minRate,
            maxRate
        ).apply {
            execute(this, success, fail, context)
        }
    }


    fun getName(): String? {
        return sharedPreference.getUserName()
    }

    fun isRegistrationCompleted(): Boolean {
        return sharedPreference.isRegistrationCompleted()
    }

    fun savePicturePath(picturePath: String?) {
        sharedPreference.savePicturePath(picturePath)
    }

    fun getPicturePath(): String? {
        return sharedPreference.getPicturePath()
    }

    fun getUserId(): String? {
        return sharedPreference.getUserID()
    }

    fun savePicturePath1(picturePath: String?) {
        sharedPreference.savePicturePath1(picturePath)
    }

    fun getPicturePath1(): String? {
        return sharedPreference.getPicturePath1()
    }

    fun savePicturePath2(picturePath: String?) {
        sharedPreference.savePicturePath2(picturePath)
    }

    fun getPicturePath2(): String? {
        return sharedPreference.getPicturePath2()
    }

    fun getEmailId(): String {
        return sharedPreference.getEmailID()

    }

    fun getPassword(): String {
        return sharedPreference.getPassword()
    }

    fun getContactNumber(): String {
        return sharedPreference.getContactNumber()
    }


}