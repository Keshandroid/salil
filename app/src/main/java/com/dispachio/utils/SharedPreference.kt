package com.dispachio.utils

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPreference @Inject constructor(@ApplicationContext private val context: Context) {
    private val PREFS_NAME = "bfdl_mfp"
    val pref = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun deleteAllSharedPreference() {
        pref.edit().clear().apply()
    }

    fun getCustomerID(): String? {
        return pref.getString(PREF_CUSTOMER_ID, "")
    }

    fun setCustomerID(customerId: String) {
        pref.edit().putString(PREF_CUSTOMER_ID, customerId).apply()
    }

    fun getToken(): String? {
        return pref.getString(PREF_TOKEN, "")
    }

    fun setToken(token: String) {
        pref.edit().putString(PREF_TOKEN, "token $token").apply()
    }

    fun getUserName(): String? {
        return pref.getString(PREF_USER_NAME, "")
    }

    fun setUserName(userName: String?) {
        pref.edit().putString(PREF_USER_NAME, userName).apply()
    }

    fun saveUserID(userId: String) {
        pref.edit().putString(PREFS_USER_ID, userId).apply()
    }

    fun getUserID(): String? {
        return pref.getString(PREFS_USER_ID, "0")
    }

    fun savePassword(pwd: String?) {
        pref.edit().putString(PREFS_PWD, pwd).apply()
    }

    fun saveIsActive(isActive: Boolean) {
        pref.edit().putBoolean(PREFS_IS_ACTIVE, isActive).apply()
    }

    fun saveEmailId(email: String?) {
        pref.edit().putString(PREFS_EMAIL_ID, email).apply()
    }

    fun getEmailID(): String {
        return pref.getString(PREFS_EMAIL_ID, "").toString()
    }

    fun isProfileCompleted(profileCompleted: Boolean) {
        pref.edit().putBoolean(PREFS_PROFILE_COMPLETED, profileCompleted).apply()
    }

    fun isRegistrationCompleted(): Boolean {
        return pref.getBoolean(PREFS_PROFILE_COMPLETED, false)
    }

    fun savePicturePath(picturePath: String?) {
        pref.edit().putString(PREFS_PICTURE_PATH, picturePath).apply()
    }

    fun getPicturePath(): String? {
        return pref.getString(PREFS_PICTURE_PATH, null)
    }

    fun savePicturePath1(picturePath: String?) {
        pref.edit().putString(PREFS_PICTURE_PATH1, picturePath).apply()
    }

    fun getPicturePath1(): String? {
        return pref.getString(PREFS_PICTURE_PATH1, null)
    }

    fun savePicturePath2(picturePath: String?) {
        pref.edit().putString(PREFS_PICTURE_PATH2, picturePath).apply()
    }

    fun getPicturePath2(): String? {
        return pref.getString(PREFS_PICTURE_PATH2, null)
    }

    fun otpSent(otp: String?) {
        pref.edit().putString(PREFS_OTP, otp).apply()
    }

    fun otpChecked(): String? {
        return pref.getString(PREFS_OTP, null)
    }

    fun getPassword(): String {
        return pref.getString(PREFS_PWD, "").toString()
    }

    fun getContactNumber(): String {
        return pref.getString(PREFS_CONTACT_NO, "").toString()
    }

    fun saveContactNumber(contactNumber: String?) {
        pref.edit().putString(PREFS_CONTACT_NO, contactNumber).apply()
    }
}