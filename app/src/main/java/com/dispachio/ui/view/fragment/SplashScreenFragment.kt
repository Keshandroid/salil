package com.dispachio.ui.view.fragment


import android.content.Intent
import android.os.Handler
import androidx.fragment.app.activityViewModels
import com.dispachio.R
import com.dispachio.databinding.FragmentSplashScreenBinding
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.base.addFragments
import com.dispachio.ui.view.activity.NavigationActivity
import com.dispachio.ui.viewmodel.LoginRegisterViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class SplashScreenFragment :
    BaseFragment<FragmentSplashScreenBinding>(R.layout.fragment_splash_screen) {

    private val loginRegisterViewModel: LoginRegisterViewModel by activityViewModels()

    override fun initView() {
        Handler().postDelayed({
            var userId = loginRegisterViewModel.getUserID()
            if (!userId.isNullOrEmpty() && userId.toInt() != 0) {
                startActivity(Intent(context, NavigationActivity::class.java))
            } else {
                mActivity.addFragments(
                    LoginScreenFragment.newInstance(),
                    R.id.logingContainer,
                    false
                )

            }
        }, 3000)
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            SplashScreenFragment()
                .apply {

                }
    }
}