package com.dispachio.data.model.response.loadSearch


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DeliveryType(
    @SerializedName("deliveryTypeId")
    val deliveryTypeId: Int,
    @SerializedName("typeValue")
    val typeValue: String
) : Parcelable