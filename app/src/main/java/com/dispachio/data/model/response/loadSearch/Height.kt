package com.dispachio.data.model.response.loadSearch


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Height(
    @SerializedName("heightId")
    val heightId: Int,
    @SerializedName("value")
    val value: String
) : Parcelable