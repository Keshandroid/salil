package com.dispachio.ui.view.fragment.milestone

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dispachio.R
import com.dispachio.data.model.request.MileStoneRequest
import com.dispachio.data.model.response.loadSearch.LoadsByDistanceResponseItem
import com.dispachio.databinding.DroppedContainerBinding
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.base.addFragments
import com.dispachio.ui.base.toast
import com.dispachio.ui.view.activity.NavigationActivity
import com.dispachio.ui.viewmodel.DashboardViewModel
import com.dispachio.utils.Log
import com.dispachio.utils.Utility
import com.example.dispachio.common.Status
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DcFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DcFragment : BaseFragment<DroppedContainerBinding>(R.layout.dropped_container) {

    private val dashboardViewModel: DashboardViewModel by activityViewModels()
    private var freshBatteryData: LoadsByDistanceResponseItem? = null
    protected var localBroadcastManager: LocalBroadcastManager? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        localBroadcastManager = activity?.let { LocalBroadcastManager.getInstance(it) }

        arguments?.let {
            freshBatteryData = it.getParcelable(ARG_PARAM1)!!
        }
    }


    companion object {

        private const val LOCATION_PERMISSION_REQUEST_CODE = 1

        @JvmStatic
        fun newInstance(param1: LoadsByDistanceResponseItem?) =
            DcFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM1, param1)
                }
            }
    }

    override fun initView() {
        mDataBinding.confirmBtn.setOnClickListener { view: View? ->

            var path = dashboardViewModel.getPicturePath()
            var path1 = dashboardViewModel.getPicturePath1()
            var userid = dashboardViewModel.getUserId()

            var fileName: String? = null
            if (path != null) {
                fileName = userid + "_dc1_" + System.currentTimeMillis()
                Utility.sendProfileImage(mActivity, path, fileName)
            }
            var fileName1: String? = null

            if (path1 != null) {
                fileName1 = userid + "_dc2_" + System.currentTimeMillis()
                Utility.sendProfileImage(mActivity, path1, fileName1)
            }
            var mileStoneRequest = MileStoneRequest()
            mileStoneRequest.milestoneStatusId = 6
            mileStoneRequest.latitude = freshBatteryData?.pickupLatitude
            mileStoneRequest.longitude = freshBatteryData?.pickupLongitude
            mileStoneRequest.curDate = freshBatteryData?.pickupDate
            mileStoneRequest.curTime = freshBatteryData?.pickupTime
            mileStoneRequest.loadId = freshBatteryData?.loadId
            mileStoneRequest.firstImageName = fileName
            mileStoneRequest.secondImageName = fileName1


            dashboardViewModel.milestonesCall(mileStoneRequest)
            initObserver()


        }
        mDataBinding.uploadImgBtn.setOnClickListener { view: View? ->
            Utility.openGallery(mActivity)
        }
        mDataBinding.uploadImgDocBtn.setOnClickListener { view: View? ->
            Utility.openGallery(mActivity)
        }
        setView()
    }

    private fun setView() {
        mDataBinding.data = freshBatteryData
    }

    private fun initObserver() {
        dashboardViewModel.milestonesModel.observe(viewLifecycleOwner, {

            when(it.status){
                Status.SUCCESS->{
                    /*mActivity.addFragments(
                        MilestoneFragment.newInstance(freshBatteryData, 6),
                        R.id.dashboardContainer,
                        true
                    )*/


                    notifyData(freshBatteryData,6)
                    (activity as NavigationActivity).onBackPressed()


                }
                Status.ERROR->{
                    mActivity.toast(it.message.toString())
                }
            }
        })
    }

    protected fun notifyData(freshBatteryData: LoadsByDistanceResponseItem?, i: Int) {
        val intent = Intent(NOTIFY_MILESTONE_FRAGMENT)
        intent.putExtra(ARG_PARAM1,i)
        localBroadcastManager?.sendBroadcast(intent)
    }

}