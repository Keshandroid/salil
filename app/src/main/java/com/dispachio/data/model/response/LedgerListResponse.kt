package com.dispachio.data.model.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class LedgerListResponse : ArrayList<PODResponse>(), Parcelable