package com.dispachio.ui.base


import android.app.Dialog
import android.content.Context
import com.dispachio.utils.Utility
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ActivityContext

/**
 * Created by 1000292 on 24-11-2020
 *
 * PUNE.
 */
@Module
@InstallIn(ActivityComponent::class)
 object BaseActivityModule {

   @Provides
   fun getProgressBar(@ActivityContext context: Context):Dialog= Utility.showCommonProgressDialog(context)

}