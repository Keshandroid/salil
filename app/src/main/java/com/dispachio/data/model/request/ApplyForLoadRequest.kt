package com.dispachio.data.model.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ApplyForLoadRequest(
    @SerializedName("loadId")
    var loadId: Int? = null,
    @SerializedName("carrierUserId")
    var carrierUserId: Int? = null
) : Parcelable
