package com.dispachio.ui.base

import com.google.gson.annotations.SerializedName

/**
 * Created by 1000292 on 27-11-2020
 *
 * PUNE.
 */
open class ParentBaseResponseModel(
    @field:SerializedName("status")
    val status: Int?=null,
    @field:SerializedName("message")
    val message: String?=null
)