package com.dispachio.ui.view.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.activityViewModels
import com.dispachio.R
import com.dispachio.data.model.request.RegistrationRequestModel
import com.dispachio.databinding.FragmentProfileBinding
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.base.toast
import com.dispachio.ui.view.activity.NavigationActivity
import com.dispachio.ui.view.activity.SplashAndLoginActivity
import com.dispachio.ui.viewmodel.DashboardViewModel
import com.dispachio.ui.viewmodel.LoginRegisterViewModel
import com.dispachio.utils.DeleteCache
import com.dispachio.utils.SharedPreference
import com.dispachio.utils.setSafeOnClickListener
import com.example.dispachio.common.Status
import com.google.gson.Gson

class ProfileFragment : BaseFragment<FragmentProfileBinding>(R.layout.fragment_profile) {

    private val dashboardViewModel: DashboardViewModel by activityViewModels()
    private val loginViewModel: LoginRegisterViewModel by activityViewModels()

    override fun initView() {

        initObserver()
        mDataBinding.editTextTextEnterName.setText(dashboardViewModel.getName())
        mDataBinding.editTextTextContactNumber.setText(dashboardViewModel.getContactNumber())
        mDataBinding.editTextTextEmailAddress.setText(dashboardViewModel.getEmailID())
        mDataBinding.editTextPassword.setText(dashboardViewModel.getPassword())

        mDataBinding.include.logout.setSafeOnClickListener {
            mActivity.startActivity(Intent(mActivity, SplashAndLoginActivity::class.java))
            SharedPreference(requireContext()).deleteAllSharedPreference()
            DeleteCache(mActivity).deleteCache()
            mActivity.toast("You've logged out completely.")
        }

        mDataBinding.include.imageView2.setSafeOnClickListener {
            (activity as NavigationActivity).openSideDrawer()
        }

        mDataBinding.submitBtn.setSafeOnClickListener {
            if (!mDataBinding.editTextTextEnterName.text.isNullOrEmpty()
                && !mDataBinding.editTextTextContactNumber.text.isNullOrEmpty()
                && !mDataBinding.editTextPassword.text.isNullOrEmpty()
            ) {
                var updatedDetail = RegistrationRequestModel()
                updatedDetail.name = mDataBinding.editTextTextEnterName.text.toString()
                updatedDetail.userID = loginViewModel.getUserID()?.toInt()
                updatedDetail.contactNumber = mDataBinding.editTextTextContactNumber.text.toString()
                updatedDetail.password = mDataBinding.editTextPassword.text.toString()
                updatedDetail.email = mDataBinding.editTextTextEmailAddress.text.toString()
                Log.e("profile_req", Gson().toJson(updatedDetail))
                loginViewModel.updateCarrierProfileData(updatedDetail)
            } else {
                mActivity.toast(mActivity.resources.getString(R.string.fill_complete_details))
            }
        }

    }

    private fun initObserver() {
        loginViewModel.updateCarrierProfResponse.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    /* userId: String,
        password: String?,
        active: Boolean,
        email: String?,
        name: String?,
        profileCompleted: Boolean,
        contactNumber: String?*/
                    mActivity.toast("Successfully Updated.")
                    it.data?.let { it1 ->
                        loginViewModel.saveLoginData(
                            it1.userId.toString(), it1.password,
                            it1.isActive!!, it1.email, it1.name, it1.isProfileCompleted!!,
                            it1.contactNumber
                        )
                    }
                    mDataBinding.editTextTextContactNumber.setText(dashboardViewModel.getContactNumber())
                    mDataBinding.editTextTextEmailAddress.setText(dashboardViewModel.getEmailID())
                    mDataBinding.editTextPassword.setText(dashboardViewModel.getPassword())
                    mActivity.startActivity(
                        Intent(
                            context,
                            NavigationActivity::class.java
                        )
                    )
                    mActivity.finish()
                }
                Status.ERROR -> {
                    mActivity.toast(it.message.toString())
                }
            }
        })
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            ProfileFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

}