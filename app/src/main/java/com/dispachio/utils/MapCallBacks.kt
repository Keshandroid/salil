package com.dispachio.utils

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.AsyncTask
import com.directions.route.AbstractRouting
import com.directions.route.Routing
import com.dispachio.R
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolylineOptions
import org.json.JSONObject
import timber.log.Timber
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL


class MapCallBacks(
    private val context: Context,
    private val callback: OnMapScreenChanges?
) {

    var url2: String?=null
    fun getUrl(origin: LatLng, dest: LatLng) {
        // Origin of route
        val str_origin =
            "origin=" + origin.latitude.toString() + "," + origin.longitude

        // Destination of route
        val str_dest =
            "destination=" + dest.latitude.toString() + "," + dest.longitude

        // Key
        val key = "key=" + context.resources.getString(R.string.google_maps_key)

        val mode = "mode=driving"
        // Building the parameters to the web service
        val parameters = "$str_origin&$str_dest&$mode&$key"


        // Output format
        val output = "json"

        // Building the url to the web service

        // Building the url to the web service

        val url = "https://maps.googleapis.com/maps/api/directions/$output?$parameters"

        val fetchUrl = callback?.let { FetchUrl(it) }
        fetchUrl?.execute(url)
    }

    fun startRouting(origin: LatLng, dest: LatLng){


            // Origin of route
            val str_origin =
                "origin=" + origin.latitude.toString() + "," + origin.longitude

            // Destination of route
            val str_dest =
                "destination=" + dest.latitude.toString() + "," + dest.longitude

            // Key
            val key = "key=" + context.resources.getString(R.string.google_maps_key)

            val mode = "mode=drivinsetMyLocationEnabledg"
            // Building the parameters to the web service
            val parameters = "$str_origin&$str_dest&$mode&$key"


            // Output format
            val output = "json"

            /*// Building the url to the web service

            val  url2 = "https://www.google.com/maps/dir/?api=1&dir_action=navigate/$output?$parameters"

            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse(
                    url2
                )
            )
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK and Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
            context.startActivity(intent)*/

        val gmmIntentUri =
            Uri.parse("google.navigation:q=$parameters")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        context.startActivity(mapIntent)
        /*    val fetchUrl = callback?.let { FetchUrl(it) }
            fetchUrl?.execute(url2)*/


    }

    class FetchUrl(callback: OnMapScreenChanges) : AsyncTask<String?, Void?, String>() {

        private val onMapScreenChanges = callback
        override fun doInBackground(vararg url: String?): String {
            // For storing data from web service
            var data = ""
            try {
                // Fetching the data from web service
                data = url[0]?.let { downloadUrl(it) }.toString()
                //    Log.d("Background Task data", data)
            } catch (e: Exception) {
                Timber.d("Background Task $e.toString()")
            }
            return data
        }


        override fun onPostExecute(result: String) {
            super.onPostExecute(result)
            val parserTask = ParserTask(onMapScreenChanges)
            // Invokes the thread for parsing the JSON data
            parserTask.execute(result)
        }


        fun downloadUrl(strUrl: String): String {
            var data = ""
            var iStream: InputStream? = null
            var urlConnection: HttpURLConnection? = null
            try {
                val url = URL(strUrl)

                // Creating an http connection to communicate with url
                urlConnection = url.openConnection() as HttpURLConnection

                // Connecting to url
                urlConnection.connect()

                // Reading data from url
                iStream = urlConnection.inputStream
                val br = BufferedReader(InputStreamReader(iStream))
                val sb = StringBuffer()
                var line: String? = ""
                while (br.readLine().also { line = it } != null) {
                    sb.append(line)
                }
                data = sb.toString()
                Timber.d("downloadUrl $data")
                br.close()
            } catch (e: java.lang.Exception) {
                Timber.d("Exception $e.toString()")
            } finally {
                iStream?.close()
                urlConnection?.disconnect()
            }
            return data
        }
    }


    class ParserTask(callback: OnMapScreenChanges) :
        AsyncTask<String?, Int?, List<List<HashMap<String, String>>>?>() {
        private val onMapScreenChanges = callback

        // Parsing the data in non-ui thread
        override fun doInBackground(vararg jsonData: String?): List<List<HashMap<String, String>>>? {
            val jObject: JSONObject
            var routes: List<List<HashMap<String, String>>>? = null
            try {
                jObject = JSONObject(jsonData[0])
                //  Log.d("ParserTask", jsonData[0])
                val parser = DataParser()
                //  Log.d("ParserTask", parser.toString())

                // Starts parsing data
                routes = parser.parse(jObject)
                //  Log.d("ParserTask", "Executing routes")
                //  Log.d("ParserTask", routes.toString())
            } catch (e: java.lang.Exception) {
                //  Log.d("ParserTask", e.toString())
                e.printStackTrace()
            }
            return routes
        }

        // Executes in UI thread, after the parsing process
        override fun onPostExecute(result: List<List<HashMap<String, String>>>?) {
            var points: ArrayList<LatLng?>
            var lineOptions: PolylineOptions? = null

            // Traversing through all the routes
            for (i in result?.indices!!) {
                points = ArrayList()
                lineOptions = PolylineOptions()

                // Fetching i-th route
                val path = result[i]

                // Fetching all the points in i-th route
                for (j in path.indices) {
                    val point = path[j]
                    val lat = point["lat"]!!.toDouble()
                    val lng = point["lng"]!!.toDouble()
                    val position = LatLng(lat, lng)
                    points.add(position)
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points)
                lineOptions.width(10f)
                lineOptions.color(Color.RED)
                Timber.d("onPostExecute onPostExecute lineoptions decoded")

                onMapScreenChanges.routeStarted(lineOptions)

            }

          /*  // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                onMapScreenChanges.routeStarted(lineOptions)
            } else {
                Timber.d("onPostExecute without Polylines drawn")
            }*/
        }
    }

    interface OnMapScreenChanges {
        fun routeStarted(lineOptions: PolylineOptions)
    }
}