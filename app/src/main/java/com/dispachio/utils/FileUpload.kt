package com.dispachio.utils

import android.content.Context
import android.util.Log
import androidx.activity.viewModels
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.services.s3.AmazonS3Client
import com.dispachio.ui.viewmodel.DashboardViewModel
import java.io.File


object FileUpload {
       private var awsAccessKey = "AKIA4ABLM7YRTIIAYBUJ";
       private var awsSecretKey = "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";
       private var bucketName = "dispachio";
       private var imagePathToStore = "document-picture";

    fun uploadtos3(context: Context, file: File, is_image: Boolean, fileName: String) {
        if (file.exists()) {
            val credentials =
                BasicAWSCredentials(awsAccessKey, awsSecretKey)
            if (is_image) {
                upload(context, file, credentials, imagePathToStore, fileName)
            } else {
                // upload(context, file, credentials, BuildConfig.CSV_PATH_TO_STORE, fileName)
            }
        }
    }

    fun upload(
        context: Context,
        file: File,
        credentials: BasicAWSCredentials?,
        pathToStore: String,
        fileName: String
    ) {
        try {
            val s3Client = AmazonS3Client(credentials)
            val transferUtility: TransferUtility = TransferUtility.builder()
                .context(context)
                .awsConfiguration(AWSMobileClient.getInstance().configuration)
                .s3Client(s3Client)
                .defaultBucket(bucketName)
                .build()
            val uploadObserver: TransferObserver =
                transferUtility.upload("$pathToStore/$fileName", file)
            uploadObserver.setTransferListener(object : TransferListener {
                override fun onStateChanged(id: Int, state: TransferState) {
                        if (TransferState.COMPLETED === state) {
                        // Handle a completed upload.
                        //   Toast.makeText(context, R.string.syncdata_message, Toast.LENGTH_LONG).show();
                        /* if (BuildConfig.CSV_PATH_TO_STORE.equals(pathToStore)) {
                             Handler().postDelayed(Runnable {
                                 if (file.exists()) {
                                     file.delete()
                                 }
                                 val index = deleteFileIfPresent(file.parentFile)
                                 if (index == 0) {
                                     file.parentFile.delete()
                                 }
                             }, 2000)
                         } else*/

                        if (AWS_URL.IMAGE_PATH_TO_STORE.url == pathToStore) {

                            /*val pref: SharedPreferences = context.getSharedPreferences(
                                StringUtils.USER_PREFERENCE,
                                MODE_PRIVATE
                            )
                            val editor = pref.edit()
                            editor.putString(StringUtils.PROFILE_PATH, file.absolutePath)
                            editor.apply()*/
                        }
                    }
                }

                override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                    var percentDonef = ( bytesCurrent /  bytesTotal) * 100;
                    var percentDone =  percentDonef;
                }

                override fun onError(id: Int, ex: Exception?) {
                    // Handle errors
                   Log.i("FileUpload", "Error");
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun deleteFileIfPresent(file: File): Int {
        var i = 0
        if (file.isDirectory) {
            for (childFile in file.listFiles()) {
                if (childFile.exists()) {
                    return i++
                }
            }
        }
        return i
    }
}