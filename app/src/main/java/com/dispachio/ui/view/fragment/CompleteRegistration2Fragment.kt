package com.dispachio.ui.view.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.dispachio.R
import com.dispachio.data.model.request.RegistrationRequestModel
import com.dispachio.databinding.CompleteProfilePg2Binding
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.base.addFragments
import com.dispachio.ui.base.toast
import com.dispachio.ui.view.activity.NavigationActivity
import com.dispachio.ui.viewmodel.DashboardViewModel
import com.dispachio.ui.viewmodel.LoginRegisterViewModel
import com.dispachio.utils.NetworkHelper
import com.dispachio.utils.Utility.openGallery
import com.dispachio.utils.Utility.sendProfileImage
import com.dispachio.utils.setSafeOnClickListener
import com.example.dispachio.common.Status
import com.google.gson.Gson


// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"

/**
 * A simple [Fragment] subclass.
 * Use the [CompleteRegistration2Fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
open class CompleteRegistration2Fragment :
    BaseFragment<CompleteProfilePg2Binding>(R.layout.complete_profile_pg2) {
    private var requestModel: RegistrationRequestModel? = null
    private val loginRegisterViewModel: LoginRegisterViewModel by activityViewModels()
    private val dashboardViewModel: DashboardViewModel by activityViewModels()
    private lateinit var network: NetworkHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            requestModel = it.getParcelable(ARG_PARAM1)!!
        }
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @return A new instance of fragment CompleteRegistration2Fragment.
         */
        @JvmStatic
        fun newInstance(param1: RegistrationRequestModel) =
            CompleteRegistration2Fragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM1, param1)
                }
            }
    }

    override fun initView() {
        mDataBinding.coiImg.setSafeOnClickListener {
            openGallery(mActivity)
        }

        mDataBinding.loaImg.setSafeOnClickListener {
            openGallery(mActivity)
        }

        mDataBinding.porImg.setSafeOnClickListener {
            openGallery(mActivity)
        }
        mDataBinding.submitBtn.setSafeOnClickListener {
            if (!mDataBinding.truckLicenceNo.text.isNullOrEmpty() && !mDataBinding.editEnterMake.text.isNullOrEmpty()
                && !mDataBinding.editEnterYear.text.isNullOrEmpty()
            ) {

                var img = dashboardViewModel.getPicturePath()
                var img1 = dashboardViewModel.getPicturePath1()
                var img2 = dashboardViewModel.getPicturePath2()

                var userid = loginRegisterViewModel.getUserID()
                network = NetworkHelper(mActivity)
                if (network.isNetworkConnected()) {
                    img?.let {
                        var fileName = userid + "_profile1_" + System.currentTimeMillis()
                        requestModel?.firstImageName = fileName
                        sendProfileImage(mActivity, it, fileName)
                    }

                    img1?.let { img1 ->
                        var fileName = userid + "_profile2_" + System.currentTimeMillis()
                        requestModel?.secondImageName = fileName
                        sendProfileImage(mActivity, img1, fileName)
                    }


                    var fileName = userid + "_profile3_" + System.currentTimeMillis()
                    requestModel?.thirdImageName = fileName
                    img2?.let { img2 ->
                        sendProfileImage(
                            mActivity, img2,
                            fileName
                        )
                    }

                    requestModel.also {
                        requestModel?.truckLicenceNumber =
                            mDataBinding.enterTruckLicence.text.toString()
                        requestModel?.truckMake = mDataBinding.editEnterMake.text.toString()
                        requestModel?.truckYear = mDataBinding.editEnterYear.text.toString().toInt()
                        requestModel?.userID = loginRegisterViewModel.getUserID()?.toInt()
                    }

                    Log.e("completer_reg_req=====>", Gson().toJson(requestModel))
                    requestModel?.let { it1 -> loginRegisterViewModel.completeRegisCall(it1) }

                    loginRegisterViewModel.completeRegisResponse.observe(viewLifecycleOwner, {
                        when (it.status) {
                            Status.SUCCESS -> {
                                mActivity.toast("Successfully registered.")
                                loginRegisterViewModel.isProfileCompleted(true)
                                mActivity.startActivity(
                                    Intent(
                                        context,
                                        NavigationActivity::class.java
                                    )
                                )
                                mActivity.finish()
                            }

                            Status.ERROR -> {
                                mActivity.toast(it.message.toString())
                            }
                            Status.LOADING -> {
                            }
                        }
                    })
                } else {
                    Toast.makeText(
                        context,
                        this.resources.getString(R.string.no_network),
                        Toast.LENGTH_LONG
                    )
                        .show()
                }


                loginRegisterViewModel.completeRegisResponse.observe(viewLifecycleOwner, {
                    when (it.status) {

                        Status.SUCCESS -> {
                            clearImg()
                            mActivity.toast("Successfully Registered!!")
                            mActivity.addFragments(
                                DashboardFragment.newInstance(null),
                                R.id.dashboardContainer,
                                false
                            )
                        }

                        Status.ERROR -> {
                            clearImg()
                            mActivity.toast(it.message.toString())


                        }

                    }

                })


            } else {
                mActivity.toast(mActivity.resources.getString(R.string.please_complete_profile))
            }
        }
    }

    private fun clearImg() {
        dashboardViewModel.savePicturePath(null)
        dashboardViewModel.savePicturePath1(null)
        dashboardViewModel.savePicturePath2(null)
    }


}