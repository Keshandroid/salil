package com.dispachio.data.model.response.loadSearch


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AccessorialSetup(
    @SerializedName("accessorialName")
    val accessorialName: String?=null,
    @SerializedName("accessorialSetupId")
    val accessorialSetupId: Int?=null,
    @SerializedName("freeTimeHour")
    val freeTimeHour: Int?=null,
    @SerializedName("milestone")
    val milestone: Milestone?=null,
    @SerializedName("milestoneId")
    val milestoneId: Int?=null,
    @SerializedName("rate")
    val rate: Double?=null,
    @SerializedName("through")
    val through: Through?=null,
    @SerializedName("throughId")
    val throughId: Int?=null,
    @SerializedName("truckingUserId")
    val truckingUserId: Int?=null
) : Parcelable