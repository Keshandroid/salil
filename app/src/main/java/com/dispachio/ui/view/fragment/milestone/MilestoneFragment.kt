package com.dispachio.ui.view.fragment.milestone

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.*
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PointF
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dispachio.R
import com.dispachio.data.model.request.PODRequest
import com.dispachio.data.model.response.loadSearch.LoadsByDistanceResponseItem
import com.dispachio.databinding.FragmentMilestoneBinding
import com.dispachio.services.ForegroundService
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.base.replaceFragment
import com.dispachio.ui.base.toast
import com.dispachio.ui.view.activity.NavigationActivity
import com.dispachio.ui.viewmodel.DashboardViewModel
import com.dispachio.utils.Log
import com.dispachio.utils.RadioMapLoadHelper
import com.dispachio.utils.setSafeOnClickListener
import com.example.dispachio.common.Status
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.GsonBuilder
import com.here.android.mpa.common.*
import com.here.android.mpa.common.PositioningManager.LocationMethod
import com.here.android.mpa.common.PositioningManager.OnPositionChangedListener
import com.here.android.mpa.guidance.NavigationManager
import com.here.android.mpa.guidance.NavigationManager.*
import com.here.android.mpa.guidance.VoiceCatalog
import com.here.android.mpa.guidance.VoiceCatalog.OnDownloadDoneListener
import com.here.android.mpa.guidance.VoiceGuidanceOptions
import com.here.android.mpa.mapping.*
import com.here.android.mpa.mapping.Map
import com.here.android.mpa.mapping.Map.OnTransformListener
import com.here.android.mpa.routing.*
import com.here.android.mpa.venues3d.*
import com.here.android.mpa.venues3d.CombinedRoute.VenueRoutingError
import com.here.android.mpa.venues3d.RoutingController.RoutingControllerListener
import com.here.android.mpa.venues3d.VenueMapFragment.VenueListener
import com.here.android.mpa.venues3d.VenueService.*
import com.here.android.positioning.StatusListener
import com.here.android.positioning.StatusListener.ServiceError
import com.here.android.positioning.radiomap.RadioMapLoader
import com.here.msdkui.guidance.GuidanceManeuverData
import com.here.msdkui.guidance.GuidanceManeuverListener
import com.here.msdkui.guidance.GuidanceManeuverPresenter
import com.here.msdkui.guidance.GuidanceManeuverView
import kotlinx.android.synthetic.main.fragment_milestone.*
import milestone.AAPFragment
import java.lang.ref.WeakReference
import java.security.AccessControlException
import java.util.*
import kotlin.collections.List
import kotlin.collections.MutableList
import kotlin.collections.indices
import kotlin.collections.set


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

var NOTIFY_MILESTONE_FRAGMENT = "NOTIFY_MILESTONE_FRAGMENT"


/**
 * A simple [Fragment] subclass.
 * Use the [MilestoneFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
open class MilestoneFragment : BaseFragment<FragmentMilestoneBinding>(R.layout.fragment_milestone),
    View.OnClickListener, VenueListener,
    MapGesture.OnGestureListener,
    OnPositionChangedListener,
    OnTransformListener,
    RoutingControllerListener {
    private val TAG = "VenuesAndLogging.BasicVenueActivity"
    private var m_geoBoundingBox: GeoBoundingBox? = null

    private val dashboardViewModel: DashboardViewModel by activityViewModels()
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var freshBatteryData: LoadsByDistanceResponseItem? = null
    private var size: Int? = null
    private var mVenueService: VenueService? = null
    private var mRoutingController: RoutingController? = null
    private val mPrivateVenues = false
    private var m_navigationManager: NavigationManager? = null
    private var mPositioningManager: PositioningManager? = null
    //private var mHereLocation: LocationDataSourceHERE? = null

    private var mGoogleLocation: LocationDataSourceGoogleServices? = null

    private var mLastMapCenter: GeoCoordinate? = null
    private val mIndoorPositioning = false
    private var mLocationMethod: LocationMethod? = null
    private var mRouteShown: Boolean = false
    private val mSelectedVenue: Venue? = null
    private var mTransforming = false
    private var mLastReceivedPosition: GeoPosition? = null
    private var mPendingUpdate: Runnable? = null
    private var mUserControl = false
    private var m_route: Route? = null
    private var mNavigate = false

    private lateinit var mMap: Map
    private var guidanceManeuverPresenter: GuidanceManeuverPresenter? = null


    protected var localBroadcastManager: LocalBroadcastManager? = null
    private var m_foregroundServiceStarted = false



    private val RUNTIME_PERMISSIONS = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.INTERNET,
        Manifest.permission.ACCESS_WIFI_STATE,
        Manifest.permission.ACCESS_NETWORK_STATE
    )
    private val REQUEST_CODE_ASK_PERMISSIONS = 1

    private var mRadioMapLoader: RadioMapLoadHelper? = null

        val mVenueLoadListener =
            VenueLoadListener { venue, venueInfo, venueLoadStatus ->
                if (venueLoadStatus != VenueLoadStatus.FAILED) {
                    Log.v(
                        TAG,
                        "onVenueLoadCompleted: loading radio maps for " + venue.id
                    )
                    mRadioMapLoader!!.load(venue)
                }
            }

    private val m_positionListener: NavigationManager.PositionListener =
        object : NavigationManager.PositionListener() {
            override fun onPositionUpdated(geoPosition: GeoPosition) {
                /* Current position information can be retrieved in this callback */

                Log.d(TAG, " <><><><> " + geoPosition.coordinate)

            }
        }

    var m_navigationManagerEventListener: NavigationManagerEventListener =
        object : NavigationManagerEventListener() {
            override fun onRunningStateChanged() {
                //Toast.makeText(mActivity, "Running state changed", Toast.LENGTH_SHORT).show()
            }

            override fun onNavigationModeChanged() {
                //Toast.makeText(mActivity, "Navigation mode changed", Toast.LENGTH_SHORT) .show()
            }

            override fun onEnded(navigationMode: NavigationManager.NavigationMode) {
                //Toast.makeText(mActivity, "$navigationMode was ended", Toast.LENGTH_SHORT).show()
                stopForegroundService()
            }

            override fun onMapUpdateModeChanged(mapUpdateMode: MapUpdateMode) {
                //Toast.makeText(mActivity, "Map update mode is changed to $mapUpdateMode", Toast.LENGTH_SHORT).show()
            }

            override fun onRouteUpdated(route: Route) {
                //Toast.makeText(mActivity, "Route updated", Toast.LENGTH_SHORT).show()
            }

            override fun onCountryInfo(s: String, s1: String) {
                //Toast.makeText(mActivity, "Country info updated from $s to $s1", Toast.LENGTH_SHORT).show()
            }
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        localBroadcastManager = activity?.let { LocalBroadcastManager.getInstance(it) }

        // get broadcast data from next screen
        localBroadcastManager!!.registerReceiver(
            mNotifyReceiver, IntentFilter(
                NOTIFY_MILESTONE_FRAGMENT
            )
        )


        arguments?.let {
            freshBatteryData = it.getParcelable(ARG_PARAM1)!!
            size = it.getInt(ARG_PARAM2)
        }

    }

    override fun onPause() {
        super.onPause()

        Log.d("MilestoneFragment", "onPause")
        /*if (mRoutingController != null) {
            mRoutingController?.hideRoute()
        }*/
    }



    override fun onDestroy() {
        super.onDestroy()
        Log.d("MilestoneFragment", "onDestroy")

        localBroadcastManager?.unregisterReceiver(mNotifyReceiver)

    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d("MilestoneFragment", "onDestroyView")


        val f = mActivity.fragmentManager.findFragmentById(R.id.milestone_map) as VenueMapFragment?
        if (f != null) mActivity.fragmentManager.beginTransaction().remove(f).commit()

    }

    private val mNotifyReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            // Get extra data included in the Intent
            size = intent.getIntExtra(ARG_PARAM1, 0)
            Log.d("MilestoneFrag", "Broadcast Value : " + size)
            initView()

        }
    }



    companion object {

        private const val LOCATION_PERMISSION_REQUEST_CODE = 1

        @JvmStatic
        fun newInstance(param1: LoadsByDistanceResponseItem?, size: Int?) =
            MilestoneFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM1, param1)
                    size?.let { putInt(ARG_PARAM2, it) }
                }
            }
    }



    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(
                mActivity,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                mActivity,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
          //  mMap.isMyLocationEnabled = true

            return
        }

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mActivity)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


//        initView()
    }


    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    //private lateinit var mVenueMapFragment: VenueMapFragment
    private var mVenueMapFragment: VenueMapFragment? = null

    override fun initView() {
        /*val mapFragment =
            childFragmentManager.findFragmentById(R.id.milestone_map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)*/

        mVenueMapFragment = getMapFragment()
        mDataBinding.data = freshBatteryData



        Log.mEnabled = true

        // checking dynamically controlled permissions
        if (context?.let { hasPermissions(it, RUNTIME_PERMISSIONS) } == true) {
            //startVenueMaps()
            startNewVenueMap()

            //initNaviControlButton()
        } else {
            ActivityCompat
                .requestPermissions(
                    mActivity,
                    RUNTIME_PERMISSIONS,
                    REQUEST_CODE_ASK_PERMISSIONS
                )
        }

        Log.d(
            "MilestoneFrag",
            " =freshBatteryData= " + GsonBuilder().setPrettyPrinting().create().toJson(
                freshBatteryData?.milestones
            )
        )


        if (size != null && size != 0) {
            Log.d("MilestoneFrag", " =999= " + size)
            setMilestones(size!!)
        } else {
            Log.d("MilestoneFrag", " =000= " + freshBatteryData?.milestones?.size)
            freshBatteryData?.milestones?.size?.let { setMilestones(it) }
        }


        setUpMap()
        mDataBinding.startBtn.setSafeOnClickListener {

        }

        initObserver()

        mDataBinding.aapBtn.setOnClickListener(this)
        mDataBinding.rpcBtn.setOnClickListener(this)
        mDataBinding.htlBtn.setOnClickListener(this)
        mDataBinding.outgatedBtn.setOnClickListener(this)
        mDataBinding.atdlBtn.setOnClickListener(this)
        mDataBinding.dcBtn.setOnClickListener(this)
        mDataBinding.podBtn.setOnClickListener(this)
        mDataBinding.myLocation.setOnClickListener(this)
        mDataBinding.naviCtrlButton.setOnClickListener(this)
        mDataBinding.stopCtrlButton.setOnClickListener(this)

        bottomSheetBehavior = BottomSheetBehavior.from(mDataBinding.bottomSheet)

        bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // handle onSlide
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                /*when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> Toast.makeText(
                        context,
                        "STATE_COLLAPSED",
                        Toast.LENGTH_SHORT
                    ).show()
                    BottomSheetBehavior.STATE_EXPANDED -> Toast.makeText(
                        context,
                        "STATE_EXPANDED",
                        Toast.LENGTH_SHORT
                    ).show()
                    BottomSheetBehavior.STATE_DRAGGING -> Toast.makeText(
                        context,
                        "STATE_DRAGGING",
                        Toast.LENGTH_SHORT
                    ).show()
                    BottomSheetBehavior.STATE_SETTLING -> Toast.makeText(
                        context,
                        "STATE_SETTLING",
                        Toast.LENGTH_SHORT
                    ).show()
                    BottomSheetBehavior.STATE_HIDDEN -> Toast.makeText(
                        context,
                        "STATE_HIDDEN",
                        Toast.LENGTH_SHORT
                    ).show()
                    else -> Toast.makeText(context, "OTHER_STATE", Toast.LENGTH_SHORT).show()
                }*/
            }
        })

        mDataBinding.btnBottomSheetPersistent.setOnClickListener {
            if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED)
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            else
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }



    }

     /**
     * Only when the app's target SDK is 23 or higher, it requests each dangerous permissions it
     * needs when the app is running.
     */
    private fun hasPermissions(context: Context, permissions: Array<String>) : Boolean {
         if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
             for ( permission: String in permissions){
                 if (ActivityCompat.checkSelfPermission(context, permission)
                     != PackageManager.PERMISSION_GRANTED) {
                     return false;
                 }
             }
         }
        return true
    }

    private fun getMapFragment(): VenueMapFragment? {
        return mActivity.fragmentManager.findFragmentById(R.id.milestone_map) as VenueMapFragment?
    }





    private val instructListener: NewInstructionEventListener = object : NewInstructionEventListener() {
        override fun onNewInstructionEvent() {
            // Interpret and present the Maneuver object as it contains
            // turn by turn navigation instructions for the user.
            m_navigationManager?.getNextManeuver()

        }
    }

    private fun startNewVenueMap(){

        mVenueMapFragment =  getMapFragment()
        mVenueMapFragment?.init(OnEngineInitListener { error ->
            if (error == OnEngineInitListener.Error.NONE) {
                Log.v(

                    TAG,
                    "InitializeVenueMaps: OnEngineInitializationCompleted"
                )
                mVenueService = mVenueMapFragment?.venueService
                mRoutingController = mVenueMapFragment?.routingController
                if (mRoutingController != null) {
                    mRoutingController?.addListener(this)
                }
                // Setting venue service content based on menu option
                if (!mPrivateVenues) {
                    setVenueServiceContent(false, false) // Public only
                } else {
                    setVenueServiceContent(true, true) // Private + public
                }
                m_navigationManager = NavigationManager.getInstance()


            } else {
                AlertDialog.Builder(context).setMessage(
                    """
                            Error : ${error.name}
                            
                            ${error.details}
                            """.trimIndent()
                )
                    .setTitle(R.string.engine_init_error)
                    .setNegativeButton(android.R.string.cancel,
                        DialogInterface.OnClickListener { dialog, which -> mActivity.finish() })
                    .create().show()
            }
        }, VenueServiceListener { result ->
            Log.v(
                TAG,
                "VenueServiceListener: OnInitializationCompleted with result: %s",
                result
            )
            when (result) {
                InitStatus.IN_PROGRESS -> {
                    Log.v(
                        TAG,
                        "Initialization of venue service is in progress..."
                    )
                    Toast.makeText(
                        context,
                        "Initialization of venue service is in progress...",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                InitStatus.OFFLINE_SUCCESS, InitStatus.ONLINE_SUCCESS, InitStatus.ONLINE_FAILED -> {
                    // Adding venue listener to map fragment
                    mVenueMapFragment!!.addListener(this)
                    // Set animations on for floor change and venue entering
                    mVenueMapFragment!!.setFloorChangingAnimation(true)
                    mVenueMapFragment!!.setVenueEnteringAnimation(true)
                    // Ask notification when venue visible; this notification is
                    // part of VenueMapFragment.VenueListener
                    mVenueMapFragment!!.setVenuesInViewportCallback(true)

                    // Add Gesture Listener for map fragment
                    mVenueMapFragment?.mapGesture?.addOnGestureListener(this, 0, false)

                    // retrieve a reference of the map from the map fragment
                    mMap = mVenueMapFragment?.map!!
                    mMap.addTransformListener(this)
                    mMap.zoomLevel = (mMap.maxZoomLevel - 3).toDouble()


                    // Start of Position Updates
                    try {
                        startPositionUpdates()
                        mVenueMapFragment!!.positionIndicator.isVisible = true
                    } catch (ex: Exception) {
                        Log.w(
                            TAG,
                            "startPositionUpdates: Could not register for location updates: %s",
                            Log.getStackTraceString(ex)
                        )
                    }
                    if (mLastMapCenter == null) {
                        mMap.setCenter(
                            GeoCoordinate(
                                61.497961,
                                23.763606,
                                0.0
                            ), Map.Animation.NONE
                        )
                    } else {
                        mMap.setCenter(
                            mLastMapCenter!!,
                            Map.Animation.NONE
                        )
                    }

                    //display indicator

                    // Set positioning indicator visible
                    val positionIndicator: PositionIndicator = mMap.getPositionIndicator()
                    positionIndicator.isVisible = true

                }
            }
        })

    }


    private fun startVenueMaps(){

        mVenueMapFragment =  getMapFragment()
        mVenueMapFragment?.init({ error ->
            if (error == OnEngineInitListener.Error.NONE) {
                Log.v(

                    TAG,
                    "InitializeVenueMaps: OnEngineInitializationCompleted"
                )
                mVenueService = mVenueMapFragment?.venueService
                mRoutingController = mVenueMapFragment?.routingController
                if (mRoutingController != null) {
                    mRoutingController?.addListener(this)
                }
                // Setting venue service content based on menu option
                if (!mPrivateVenues) {
                    setVenueServiceContent(false, false) // Public only
                } else {
                    setVenueServiceContent(true, true) // Private + public
                }
                m_navigationManager = NavigationManager.getInstance()


            } else {
                AlertDialog.Builder(context).setMessage(
                    """
                            Error : ${error.name}
                            
                            ${error.details}
                            """.trimIndent()
                )
                    .setTitle(R.string.engine_init_error)
                    .setNegativeButton(android.R.string.cancel,
                        DialogInterface.OnClickListener { dialog, which -> mActivity.finish() })
                    .create().show()
            }
        }) { result ->
            Log.v(
                TAG,
                "VenueServiceListener: OnInitializationCompleted with result: %s",
                result
            )
            when (result) {
                InitStatus.IN_PROGRESS -> {
                    Log.v(
                        TAG,
                        "Initialization of venue service is in progress..."
                    )
                    Toast.makeText(
                        context,
                        "Initialization of venue service is in progress...",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                InitStatus.OFFLINE_SUCCESS, InitStatus.ONLINE_SUCCESS -> {
                    // Adding venue listener to map fragment
                    mVenueMapFragment!!.addListener(this)
                    // Set animations on for floor change and venue entering
                    mVenueMapFragment!!.setFloorChangingAnimation(true)
                    mVenueMapFragment!!.setVenueEnteringAnimation(true)
                    // Ask notification when venue visible; this notification is
                    // part of VenueMapFragment.VenueListener
                    mVenueMapFragment!!.setVenuesInViewportCallback(true)

                    // Add Gesture Listener for map fragment
                    mVenueMapFragment?.mapGesture?.addOnGestureListener(this, 0, false)

                    // retrieve a reference of the map from the map fragment
                    mMap = mVenueMapFragment?.map!!
                    mMap.addTransformListener(this)
                    mMap.zoomLevel = (mMap.maxZoomLevel - 3).toDouble()


                    // Start of Position Updates
                    try {
                        startPositionUpdates()
                        mVenueMapFragment!!.positionIndicator.isVisible = true
                    } catch (ex: Exception) {
                        Log.w(
                            TAG,
                            "startPositionUpdates: Could not register for location updates: %s",
                            Log.getStackTraceString(ex)
                        )
                    }
                    if (mLastMapCenter == null) {
                        mMap.setCenter(
                            GeoCoordinate(
                                61.497961,
                                23.763606,
                                0.0
                            ), Map.Animation.NONE
                        )
                    } else {
                        mMap.setCenter(
                            mLastMapCenter!!,
                            Map.Animation.NONE
                        )
                    }

                    //display indicator

                    // Set positioning indicator visible
                    val positionIndicator: PositionIndicator = mMap.getPositionIndicator()
                    positionIndicator.isVisible = true

                }
            }
        }
    }

    /**
     * Initialization of Position Updates
     * Called after map initialization
     */
    private fun startPositionUpdates() {
        Log.v(TAG, "Start of Positioning Updates")
        mPositioningManager = PositioningManager.getInstance()
        if (mPositioningManager == null) {
            Log.w(TAG, "startPositionUpdates: PositioningManager is null")
            return
        }


        //Google location datasource
        mGoogleLocation = LocationDataSourceGoogleServices.getInstance()
        if (mGoogleLocation != null) {
            mPositioningManager!!.dataSource = mGoogleLocation
            //mPositioningManager?.addListener(WeakReference(this))
            mPositioningManager?.addListener(
                WeakReference<PositioningManager.OnPositionChangedListener>(
                    this
                )
            )


            mLocationMethod = LocationMethod.GPS_NETWORK


            if (mPositioningManager!!.start(mLocationMethod)) {
                // Position updates started successfully.
                Log.e(
                    TAG,
                    "startPositionUpdates: Position updates started successfully."
                )
            }
        }


        //HERE location data source
       /* mHereLocation = LocationDataSourceHERE.getInstance(mPositioningStatusListener)
        if (mHereLocation == null) {
            Log.w(TAG, "startPositionUpdates: LocationDataSourceHERE is null")
            mActivity.finish()
            return
        }
        mHereLocation?.setDiagnosticsListener(
            DiagnosticsListener { event ->
                Log.v(
                    TAG,
                    "onDiagnosticEvent: %s",
                    event.description
                )
            }
        )


        mPositioningManager?.dataSource = mHereLocation
        try {
            mPositioningManager?.addListener(WeakReference<OnPositionChangedListener>(this))
            if (mIndoorPositioning) {
                mLocationMethod = LocationMethod.GPS_NETWORK_INDOOR
                Log.v(TAG, "Location method set to GPS_NETWORK_INDOOR")
            } else {
                mLocationMethod = LocationMethod.GPS_NETWORK
                Log.v(TAG, "Location method set to GPS_NETWORK")
            }
            if (!mPositioningManager!!.start(mLocationMethod)) {
                Log.e(
                    TAG,
                    "startPositionUpdates: PositioningManager.start returned error"
                )
            }
        } catch (ex: java.lang.Exception) {
            Log.w(
                TAG,
                "startPositionUpdates: Could not register for location updates: %s",
                Log.getStackTraceString(ex)
            )
        }
        */



        try {
            mRadioMapLoader =
                LocationDataSourceHERE.getInstance().radioMapLoader?.let {
                    RadioMapLoadHelper(
                        it,
                        object : RadioMapLoadHelper.Listener {
                            override fun onError(venue: Venue, status: RadioMapLoader.Status?) {
                                // Radio map loading failed with status.
                            }

                            override fun onProgress(venue: Venue, progress: Int) {
                                // Radio map loading progress.
                            }

                            override fun onCompleted(
                                venue: Venue,
                                status: RadioMapLoader.Status
                            ) {
                                Log.i(
                                    TAG,
                                    "Radio map for venue: " + venue.id + ", completed with status: " + status
                                )
                                // Radio map loading completed with status.
                            }
                        })
                }
            mVenueService?.addVenueLoadListener(mVenueLoadListener)
        } catch (ex: java.lang.Exception) {
            Log.e(
                TAG,
                "startPositionUpdates: setting up radio map loader failed",
                ex
            )
            mRadioMapLoader = null
        }

    }


    /**
     * Clears the route if shown
     */
    private fun clearRoute() {
        if (mRouteShown) {
            if (mRoutingController != null) {
                mRoutingController?.hideRoute()
            }
            mRouteShown = false
        }
    }

    /**
     * Positioning status listener.
     */
    private val mPositioningStatusListener: StatusListener = object : StatusListener {
        override fun onOfflineModeChanged(offline: Boolean) {
            Log.v(TAG, "StatusListener.onOfflineModeChanged: %b", offline)
        }

        override fun onAirplaneModeEnabled() {
            Log.v(TAG, "StatusListener.onAirplaneModeEnabled")
        }

        override fun onWifiScansDisabled() {
            Log.v(TAG, "StatusListener.onWifiScansDisabled")
        }

        override fun onBluetoothDisabled() {
            Log.v(TAG, "StatusListener.onBluetoothDisabled")
        }

        override fun onCellDisabled() {
            Log.v(TAG, "StatusListener.onCellDisabled")
        }

        override fun onGnssLocationDisabled() {
            Log.v(TAG, "StatusListener.onGnssLocationDisabled")
        }

        override fun onNetworkLocationDisabled() {
            Log.v(TAG, "StatusListener.onNetworkLocationDisabled")
        }

        override fun onServiceError(error: ServiceError) {
            Log.v(TAG, "StatusListener.onServiceError: %s", error)
        }

        override fun onPositioningError(error: StatusListener.PositioningError) {
            Log.v(TAG, "StatusListener.onPositioningError: %s", error)
        }

        override fun onWifiIndoorPositioningNotAvailable() {
            Log.v(TAG, "StatusListener.onWifiIndoorPositioningNotAvailable")
        }

        override fun onWifiIndoorPositioningDegraded() {
            // called when running on Android 9.0 (Pie) or newer
        }
    }
    /**
     * Setting usage of Private Venues along with Public Venues
     *
     * @param mCombined used for setting Combined content
     * @param mPrivate  used for setting Private content
     * VenueServiceListener should be initialized after setting this content
     * More details in documentation
     */
    private fun setVenueServiceContent(mCombined: Boolean, mPrivate: Boolean) {
        try {
            Log.v(
                TAG,
                "setVenueServiceContent: Combined = %s, Private = %s",
                mCombined,
                mPrivate
            )
            mVenueService!!.isPrivateContent = mPrivate
            mVenueService!!.setIsCombinedContent(mCombined)
        } catch (e: AccessControlException) {
            e.printStackTrace()
            Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
            Log.e(
                TAG,
                "SetVenueServiceContent error: %s",
                Log.getStackTraceString(e)
            )
        }
    }
    private fun setMilestones(size: Int) {
        when (size) {
            0 -> {
                mDataBinding.aapBtn.isEnabled = true
            }
            1 -> {
                aapBtnDialog()
                mDataBinding.aapBtn.isEnabled = false
                mDataBinding.rpcBtn.isEnabled = true
            }
            2 -> {
                aapBtnDialog()
                rpcBtnDialog()
                mDataBinding.aapBtn.isEnabled = false
                mDataBinding.rpcBtn.isEnabled = false
                mDataBinding.htlBtn.isEnabled = true
            }
            3 -> {
                aapBtnDialog()
                rpcBtnDialog()
                htlBtnDialog()
                mDataBinding.aapBtn.isEnabled = false
                mDataBinding.rpcBtn.isEnabled = false
                mDataBinding.htlBtn.isEnabled = false
                mDataBinding.outgatedBtn.isEnabled = true
            }
            4 -> {
                aapBtnDialog()
                rpcBtnDialog()
                htlBtnDialog()
                outgatedBtnDialog()
                mDataBinding.aapBtn.isEnabled = false
                mDataBinding.rpcBtn.isEnabled = false
                mDataBinding.htlBtn.isEnabled = false
                mDataBinding.outgatedBtn.isEnabled = false
                mDataBinding.atdlBtn.isEnabled = true
            }
            5 -> {
                aapBtnDialog()
                rpcBtnDialog()
                htlBtnDialog()
                outgatedBtnDialog()
                atdlBtnDialog()
                mDataBinding.aapBtn.isEnabled = false
                mDataBinding.rpcBtn.isEnabled = false
                mDataBinding.htlBtn.isEnabled = false
                mDataBinding.outgatedBtn.isEnabled = false
                mDataBinding.atdlBtn.isEnabled = false
                mDataBinding.dcBtn.isEnabled = true
            }
            6 -> {
                aapBtnDialog()
                rpcBtnDialog()
                htlBtnDialog()
                outgatedBtnDialog()
                atdlBtnDialog()
                dcBtnDialog()
                mDataBinding.aapBtn.isEnabled = false
                mDataBinding.rpcBtn.isEnabled = false
                mDataBinding.htlBtn.isEnabled = false
                mDataBinding.outgatedBtn.isEnabled = false
                mDataBinding.atdlBtn.isEnabled = false
                mDataBinding.dcBtn.isEnabled = false
                mDataBinding.podBtn.isEnabled = true
            }
            7 -> {
            }

        }
    }

    private fun initObserver() {
        /*dashboardViewModel.milestonesModel.observe(viewLifecycleOwner, {
        })*/
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.aapBtn -> {
                clearImg()
                /*mActivity.addFragments(
                    AAPFragment.newInstance(freshBatteryData),
                    R.id.dashboardContainer,
                    true
                )*/

                mActivity.replaceFragment(
                    AAPFragment.newInstance(freshBatteryData),
                    R.id.dashboardContainer
                )

            }
            R.id.rpcBtn -> {
                clearImg()
                /*mActivity.addFragments(
                    RpcFragment.newInstance(freshBatteryData),
                    R.id.dashboardContainer,
                    true
                )*/

                mActivity.replaceFragment(
                    RpcFragment.newInstance(freshBatteryData),
                    R.id.dashboardContainer
                )
            }
            R.id.htlBtn -> {
                clearImg()
                /* mActivity.addFragments(
                    HtlFragment.newInstance(freshBatteryData),
                    R.id.dashboardContainer,
                    true
                )*/
                mActivity.replaceFragment(
                    HtlFragment.newInstance(freshBatteryData),
                    R.id.dashboardContainer
                )
            }
            R.id.outgatedBtn -> {
                clearImg()
                /*mActivity.addFragments(
                    OutgatedFragment.newInstance(freshBatteryData),
                    R.id.dashboardContainer,
                    true
                )*/
                mActivity.replaceFragment(
                    OutgatedFragment.newInstance(freshBatteryData),
                    R.id.dashboardContainer
                )
            }
            R.id.atdlBtn -> {
                clearImg()
                /*mActivity.addFragments(
                    AtdlFragment.newInstance(freshBatteryData),
                    R.id.dashboardContainer,
                    true
                )*/

                mActivity.replaceFragment(
                    AtdlFragment.newInstance(freshBatteryData),
                    R.id.dashboardContainer
                )
            }
            R.id.dcBtn -> {
                clearImg()
                /*mActivity.addFragments(
                    DcFragment.newInstance(freshBatteryData),
                    R.id.dashboardContainer,
                    true
                )*/

                mActivity.replaceFragment(
                    DcFragment.newInstance(freshBatteryData),
                    R.id.dashboardContainer
                )
            }
            R.id.podBtn -> {
                clearImg()
                podBtnDialog()
            }
            R.id.myLocation -> {
                getCurrentLocation()
            }
            R.id.naviCtrlButton -> {

                /*
                 * To start a turn-by-turn navigation, a concrete route object is required.We use
                 * the same steps from Routing sample app to create a route from 4350 Still Creek Dr
                 * to Langley BC without going on HWY.
                 *
                 * The route calculation requires local map data.Unless there is pre-downloaded map
                 * data on device by utilizing MapLoader APIs,it's not recommended to trigger the
                 * route calculation immediately after the MapEngine is initialized.The
                 * INSUFFICIENT_MAP_DATA error code may be returned by CoreRouter in this case.
                 *
                 */if (m_route == null) {
                    //createRoute()

                    Log.d("LATLong", "{Size}" + freshBatteryData?.milestones?.size)

                    if (size != null && size!! >= 1) {

                        Log.d(
                            "LATLong",
                            "<Size Delivery>" + freshBatteryData?.deliveryLatitude.toString() + " " + freshBatteryData?.deliveryLongitude.toString()
                        )
                        doSearch(
                            freshBatteryData?.deliveryLatitude.toString(),
                            freshBatteryData?.deliveryLongitude.toString()
                        )

                    } else {

                        if (freshBatteryData?.milestones?.size != null && freshBatteryData?.milestones?.size != 0) {
                            Log.d(
                                "LATLong",
                                "<Delivery>" + freshBatteryData?.deliveryLatitude.toString() + " " + freshBatteryData?.deliveryLongitude.toString()
                            )
                            doSearch(
                                freshBatteryData?.deliveryLatitude.toString(),
                                freshBatteryData?.deliveryLongitude.toString()
                            )
                        } else {
                            Log.d(
                                "LATLong",
                                "<Pickup>" + freshBatteryData?.pickupLatitude.toString() + " " + freshBatteryData?.pickupLongitude.toString()
                            )
                            doSearch(
                                freshBatteryData?.pickupLatitude.toString(),
                                freshBatteryData?.pickupLongitude.toString()
                            )
                        }

                    }

                    /*if(freshBatteryData?.milestones?.size != null && freshBatteryData?.milestones?.size!=0){
                        Log.d("LATLong","<Delivery>"+freshBatteryData?.deliveryLatitude.toString() + " " + freshBatteryData?.deliveryLongitude.toString())
                        doSearch(freshBatteryData?.deliveryLatitude.toString(),freshBatteryData?.deliveryLongitude.toString())
                     }else{
                        Log.d("LATLong","<Pickup>"+freshBatteryData?.pickupLatitude.toString() + " " + freshBatteryData?.pickupLongitude.toString())
                        doSearch(freshBatteryData?.pickupLatitude.toString(),freshBatteryData?.pickupLongitude.toString())
                     }*/

                } else {
                    startNavigation()
                }
            }
            R.id.stopCtrlButton -> {
                stopNavigation()
            }
        }
    }

    private fun clearImg() {
        dashboardViewModel.savePicturePath(null)
        dashboardViewModel.savePicturePath1(null)
        dashboardViewModel.savePicturePath2(null)
    }

    private fun aapBtnDialog() {


        mDataBinding.reportPortConditions.background =
            mActivity.resources.getDrawable(R.drawable.fill)
        mDataBinding.reportPortConditionsView.setBackgroundColor(
            mActivity.resources.getColor(
                R.color.button_shade
            )
        )

        ColorStateList.valueOf(mActivity.resources.getColor(R.color.dsp_greyish))
            .also { mDataBinding.rpcBtn.rippleColor = it }

        mDataBinding.rpcBtn.setBackgroundColor(resources.getColor(R.color.button_shade))
        mDataBinding.rpcBtn.setTextColor(resources.getColor(R.color.dsp_white_two))

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun rpcBtnDialog() {
        mDataBinding.hookedToLoad.background =
            mActivity.resources.getDrawable(R.drawable.fill)
        mDataBinding.hookedToLoadView.setBackgroundColor(mActivity.resources.getColor(R.color.button_shade))

        ColorStateList.valueOf(mActivity.resources.getColor(R.color.dsp_greyish))
            .also { mDataBinding.htlBtn.rippleColor = it }

        mDataBinding.htlBtn.setBackgroundColor(resources.getColor(R.color.button_shade))
        mDataBinding.htlBtn.setTextColor(resources.getColor(R.color.dsp_white_two))

    }

    private fun htlBtnDialog() {
        mDataBinding.outgated.background = mActivity.resources.getDrawable(R.drawable.fill)
        mDataBinding.outgatedView.setBackgroundColor(mActivity.resources.getColor(R.color.button_shade))

        ColorStateList.valueOf(mActivity.resources.getColor(R.color.dsp_greyish))
            .also { mDataBinding.outgatedBtn.rippleColor = it }

        mDataBinding.outgatedBtn.setBackgroundColor(resources.getColor(R.color.button_shade))
        mDataBinding.outgatedBtn.setTextColor(resources.getColor(R.color.dsp_white_two))


    }

    private fun outgatedBtnDialog() {
        mDataBinding.atdlBtn.isEnabled = true
        mDataBinding.arriveToDeliveryLoc.background =
            mActivity.resources.getDrawable(R.drawable.fill)
        mDataBinding.arriveToDeliveryLocView.setBackgroundColor(mActivity.resources.getColor(R.color.button_shade))
        ColorStateList.valueOf(mActivity.resources.getColor(R.color.dsp_greyish))
            .also { mDataBinding.atdlBtn.rippleColor = it }

        mDataBinding.atdlBtn.setBackgroundColor(resources.getColor(R.color.button_shade))
        mDataBinding.atdlBtn.setTextColor(resources.getColor(R.color.dsp_white_two))

    }

    private fun atdlBtnDialog() {

        mDataBinding.dcBtn.setTextAppearance(context, R.style.button_style2)
        mDataBinding.droppedContianer.background =
            mActivity.resources.getDrawable(R.drawable.fill)
        mDataBinding.droppedContianerView.setBackgroundColor(mActivity.resources.getColor(R.color.button_shade))

        ColorStateList.valueOf(mActivity.resources.getColor(R.color.dsp_greyish))
            .also { mDataBinding.dcBtn.rippleColor = it }

        mDataBinding.dcBtn.setBackgroundColor(resources.getColor(R.color.button_shade))
        mDataBinding.dcBtn.setTextColor(resources.getColor(R.color.dsp_white_two))

    }


    private fun dcBtnDialog() {


        mDataBinding.podSigned.background = mActivity.resources.getDrawable(R.drawable.fill)

        ColorStateList.valueOf(mActivity.resources.getColor(R.color.dsp_greyish))
            .also { mDataBinding.podBtn.rippleColor = it }

        mDataBinding.podBtn.setBackgroundColor(resources.getColor(R.color.button_shade))
        mDataBinding.podBtn.setTextColor(resources.getColor(R.color.dsp_white_two))


    }

    private fun podBtnDialog() {
        val layoutInflater =
            mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val builder = AlertDialog.Builder(mActivity, R.style.Theme_Dialog)
        val layout: View = layoutInflater.inflate(R.layout.pod_container, null)
        builder.setView(layout)
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 20)
        alertDialog.window!!.setBackgroundDrawable(inset)
        //alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.cancel()
        alertDialog.show()
        alertDialog.setCancelable(false)
        val okButton = layout.findViewById<Button>(R.id.okButton)

        var request = PODRequest()
        request.userId = dashboardViewModel.getUserId()?.toInt()
        request.loadId = freshBatteryData?.loadId

        okButton.setOnClickListener { view: View? ->
            dashboardViewModel.createPOD(request)
            dashboardViewModel.createPodModel.observe(viewLifecycleOwner, {
                when (it.status) {
                    Status.SUCCESS -> {
                        mActivity.toast("Milestone Completed.")
                        mActivity.startActivity(Intent(mActivity, NavigationActivity::class.java))
                        mActivity.finish()
                    }
                    Status.ERROR -> {
                        mActivity.toast(it.message.toString())
                    }
                }
            })
            alertDialog.dismiss()
        }

    }

    class ModalBottomSheet : BottomSheetDialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? = inflater.inflate(R.layout.modal_bottom_sheet_content, container, false)

        companion object {
            const val TAG = "ModalBottomSheet"
        }
    }

    override fun onVenueTapped(venue: Venue?, x: Float, y: Float) {
        Log.v(TAG, "onVenueTapped")
        mMap.pixelToGeo(PointF(x, y))
        mVenueMapFragment!!.selectVenue(venue)
    }

    override fun onVenueSelected(venue: Venue?) {
        Log.v(TAG, "onVenueSelected: %s", venue?.id)
        if (mVenueMapFragment == null) {
            return
        }
        val venueId: String? = venue?.id
        val venueName: String? = venue?.content?.name
        Toast.makeText(context, "$venueId: $venueName", Toast.LENGTH_SHORT)
            .show()
        Log.v(TAG, "Venue selected: %s: %s", venueId, venueName)
    }

    override fun onVenueDeselected(p0: Venue?, p1: DeselectionSource?) {
        mUserControl = true
        mActivity.invalidateOptionsMenu()
    }

    override fun onSpaceSelected(venue: Venue?, space: Space?) {
        Log.v(TAG, "onSpaceSelected")
        if (space != null) {
            onSpaceSelectedMapMode(space)
        }
    }

    /**
     * Showing information about space selected from the venue map
     * and logging it
     *
     * @param space selected space
     */
    private fun onSpaceSelectedMapMode(space: Space) {
        Log.v(TAG, "onSpaceSelectedMapMode")
        val spaceName = space.content.name
        val parentCategory = space.content.parentCategoryId
        val placeCategory = space.content.placeCategoryId
        Toast.makeText(
            context, "Space " + spaceName + ", parent category: "
                    + parentCategory + ", place category: " + placeCategory, Toast.LENGTH_SHORT
        ).show()
        Log.v(
            TAG,
            "Space selected: %s, Parent category: %s, Place category: %s",
            spaceName,
            parentCategory,
            placeCategory
        )
    }
    override fun onSpaceDeselected(p0: Venue?, p1: Space?) {
    }

    override fun onFloorChanged(p0: Venue?, p1: Level?, p2: Level?) {
    }

    override fun onVenueVisibleInViewport(p0: Venue?, p1: Boolean) {
    }

    override fun onPanStart() {
        if (mMap != null) {
            // User takes control of map instead of position updates
            mUserControl = true
            mActivity.invalidateOptionsMenu()
        }
    }

    override fun onPanEnd() {
    }

    override fun onMultiFingerManipulationStart() {
    }

    override fun onMultiFingerManipulationEnd() {
    }

    override fun onMapObjectsSelected(p0: MutableList<ViewObject>): Boolean {
        return false
    }

    override fun onTapEvent(p: PointF): Boolean {
        if (mMap == null) {
            Toast.makeText(
                context,
                "Initialization of venue service is in progress...",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }
        val touchLocation: GeoCoordinate? = mMap.pixelToGeo(p)
        val lat = touchLocation?.latitude
        val lon = touchLocation?.longitude
        val StrGeo = String.format("%.6f, %.6f", lat, lon)
        Toast.makeText(context, StrGeo, Toast.LENGTH_SHORT).show()
        mUserControl = true
        mActivity.invalidateOptionsMenu()
        return false
    }

    override fun onDoubleTapEvent(p0: PointF): Boolean {
        return false
    }

    override fun onPinchLocked() {
    }

    override fun onPinchZoomEvent(p0: Float, p1: PointF): Boolean {
        return false
    }

    override fun onRotateLocked() {
    }

    override fun onRotateEvent(p0: Float): Boolean {
        return false
    }

    override fun onTiltEvent(p0: Float): Boolean {
        return false
    }

    override fun onLongPressEvent(p0: PointF): Boolean {
        return false
    }

    override fun onLongPressRelease() {
    }

    override fun onTwoFingerTapEvent(p0: PointF): Boolean {
        return false
    }

    override fun onPositionUpdated(
        locationMethod: LocationMethod?,
        geoPosition: GeoPosition?,
        mapMatched: Boolean
    ) {

        rlButtons.visibility = View.VISIBLE

        Log.d(TAG, " {}{}{}{} " + geoPosition?.coordinate)

        m_navigationManager?.getTta(Route.TrafficPenaltyMode.DISABLED, true);
        m_navigationManager?.getDestinationDistance();



        mLastReceivedPosition = geoPosition
        val receivedCoordinate = mLastReceivedPosition!!.coordinate
        if (mTransforming) {
            mPendingUpdate = Runnable { onPositionUpdated(locationMethod, geoPosition, mapMatched) }
        } else {
            if (mVenueMapFragment != null) {
                mLastMapCenter = receivedCoordinate
                if (!mUserControl) {
                    // when "follow position" options selected than map centered according to position updates
                    mMap.setCenter(receivedCoordinate, Map.Animation.NONE)
                    // Correctly displaying indoor position inside the venue
                    if (geoPosition?.getPositionSource() == GeoPosition.SOURCE_INDOOR) {
                        if (!geoPosition.getBuildingId()?.isEmpty()!! && mPrivateVenues) {
                            mVenueMapFragment!!.selectVenueAsync(geoPosition.getBuildingId())
                            mVenueMapFragment!!.getVenueController(mVenueMapFragment!!.selectedVenue)
                            selectLevelByFloorId(geoPosition.getFloorId()!!)
                        }
                    }
                }


            }
            // Write updated position to the log and to info view
            if (geoPosition != null) {
                if (locationMethod != null) {
                    updateLocationInfo(locationMethod, geoPosition)
                }
            }
        }


    }

    /*override fun onPositionUpdated(locationMethod: LocationMethod?, geoPosition: GeoPosition?, mapMatched: Boolean) {
        mLastReceivedPosition = geoPosition
        val receivedCoordinate: GeoCoordinate? = mLastReceivedPosition?.coordinate
        if (mTransforming) {
            mPendingUpdate = Runnable { onPositionUpdated(locationMethod, geoPosition, mapMatched) }
        } else {
            if (mVenueMapFragment != null) {
                mLastMapCenter = receivedCoordinate
                if (!mUserControl) {
                    // when "follow position" options selected than map centered according to position updates
                    if (receivedCoordinate != null) {
                        mMap.setCenter(receivedCoordinate, Map.Animation.NONE)
                    }
                    // Correctly displaying indoor position inside the venue
                    if (geoPosition?.positionSource == GeoPosition.SOURCE_INDOOR) {
                        if (geoPosition?.buildingId?.isNotEmpty() == true && mPrivateVenues) {
                            mVenueMapFragment!!.selectVenueAsync(geoPosition.buildingId)
                            mVenueMapFragment!!.getVenueController(mVenueMapFragment!!.selectedVenue)
                            geoPosition.floorId?.let { selectLevelByFloorId(it) }
                        }
                    }
                }
            }
            // Write updated position to the log and to info view
            if (locationMethod != null) {
                if (geoPosition != null) {
                    updateLocationInfo(locationMethod, geoPosition)
                }
            }
        }
    }*/

        /**
     * Update location information to the log and location view.
     *
     * @param geoPosition Latest geo position update.
     */
    private fun updateLocationInfo(
            locationMethod: PositioningManager.LocationMethod,
            geoPosition: GeoPosition
        ) {
        /*if (mLocationInfo == null) {
            return;
        }*/

            val sb = StringBuffer()
            val coord: GeoCoordinate = geoPosition.coordinate;
            sb.append("Coordinate:").append(
                String.format(
                    Locale.US,
                    "%.6f, %.6f\n",
                    coord.latitude,
                    coord.longitude
                )
            );
            if (geoPosition.latitudeAccuracy != GeoPosition.UNKNOWN.toFloat()) {
                sb.append("Uncertainty:").append(
                    String.format(
                        Locale.US,
                        "%.2fm\n",
                        geoPosition.latitudeAccuracy
                    )
                )
            }


            sb.deleteCharAt(sb.length - 1);
            Log.v(TAG, "Position Updated:\n%s", sb.toString());

            //check if user's current location is within 200 meter radius
            val distance = FloatArray(1)
            val radiusInMeters = 200.0 //1 KM = 1000 Meter

            if(size!=null && size!! >=1){
                mDataBinding.aapBtn.isEnabled = false

            }else{

                if(freshBatteryData?.milestones?.size != null && freshBatteryData?.milestones?.size!=0){
                    mDataBinding.aapBtn.isEnabled = false

                }else{
                   // Log.d("Radius","<Pickup>"+freshBatteryData?.pickupLatitude.toString() + " " + freshBatteryData?.pickupLongitude.toString())
                   // Log.d("Radius","<Current>"+coord.latitude.toString() + " " + coord.longitude.toString())

                    Location.distanceBetween(
                        freshBatteryData?.pickupLatitude!!, freshBatteryData?.pickupLongitude!!,
                        coord.latitude, coord.longitude, distance
                    )

                    if( distance[0] > radiusInMeters){
                        Log.d(
                            "Radius",
                            "Outside, distance from center: " + distance[0] + " radius: " + radiusInMeters
                        )
                        mDataBinding.aapBtn.isEnabled = false

                    } else {
                        mDataBinding.aapBtn.isEnabled = true
                        Log.d(
                            "Radius",
                            "Inside, distance from center: " + distance[0] + " radius: " + radiusInMeters
                        )
                    }
                }

            }




    }
    /**
     * Selecting venue level by given floorID
     *
     * @param floorId current indoor position
     */
    protected fun selectLevelByFloorId(floorId: Int) {
        if (mVenueMapFragment != null) {
            val venue = mVenueMapFragment!!.selectedVenue
            if (venue != null) {
                mVenueMapFragment!!.setFloorChangingAnimation(true)
                val levels = venue.levels
                for (item in levels) {
                    if (item != null) {
                        if (item.floorNumber == floorId) {
                            mVenueMapFragment!!.getVenueController(venue)!!.selectLevel(item)
                            break
                        }
                    }
                }
            }
        }
    }
    override fun onPositionFixChanged(
        locationMethod: LocationMethod?,
        locationStatus: PositioningManager.LocationStatus?
    ) {
        if (locationStatus == PositioningManager.LocationStatus.OUT_OF_SERVICE) {
            // Out of service, last received position no longer valid for route calculation
            mLastReceivedPosition = null
        }
    }

    override fun onMapTransformStart() {
        Log.v(TAG, "onMapTransformStart")
        mTransforming = true
    }

    override fun onMapTransformEnd(mapState: MapState?) {
        Log.v(TAG, "onMapTransformEnd")
        mTransforming = false
        if (mPendingUpdate != null) {
            mPendingUpdate?.run()
            mPendingUpdate = null
        }
    }

    override fun onCombinedRouteCompleted(route: CombinedRoute?) {
        Log.v(TAG, "onCombinedRouteCompleted")
        val error: VenueRoutingError? = route?.error
        if (error == VenueRoutingError.NO_ERROR) {
            if (mVenueMapFragment != null) {
                val selectedVenueController = mVenueMapFragment!!.getVenueController(mSelectedVenue)
                if (selectedVenueController != null && mRoutingController != null) {
                    Log.i(TAG, "onCombinedRouteCompleted route found")
                    Toast.makeText(context, "Route found", Toast.LENGTH_SHORT).show()
                    // Use RoutingController to show route
                    mRoutingController!!.showRoute(route)
                    mRouteShown = true
                }
            }
        } else {
            Toast.makeText(context, "No route found", Toast.LENGTH_SHORT).show()
        }
    }

       val geoLocationList =  ArrayList<HashMap<String, String>>()

    open fun nearestDataList(context: Context?, latitude: String, longitude: String): ArrayList<HashMap<String, String>> {
        val formList = ArrayList<HashMap<String, String>>()
        var m_li: HashMap<String, String>
        m_li = HashMap()
        m_li["Latitude"] = latitude
        m_li["Longitude"] = longitude
        formList.add(m_li)

        /*m_li = HashMap()
        m_li["Latitude"] = freshBatteryData?.deliveryLatitude.toString()
        m_li["Longitude"] = freshBatteryData?.deliveryLongitude.toString()
        formList.add(m_li)*/

        return formList
    }

    private fun doSearch(latitude: String, longitude: String) {

        Log.d(TAG, "create route called....")


        mNavigate = true

        val data: ArrayList<HashMap<String, String>> = nearestDataList(context, latitude, longitude)

        geoLocationList.clear()
        if(mMap!=null){
            mMap.removeAllMapObjects()
        }


        if ((if (data != null) data.size else 0) != 0) {
            for (i in data.indices) {
                val locationData: HashMap<String, String> = data.get(i)
                try {
                    Log.d("nearestDataList", locationData["Latitude"])
                    Log.d("nearestDataList", locationData["Longitude"])
                    Log.d("nearestDataList", "========")

                    geoLocationList.add(data.get(i))
                    if (mNavigate) {
                        mNavigate = false
                        geoLocationListtemp = geoLocationList
                        createRoute()
                        geoLocationList.clear()
                        mMap.removeAllMapObjects()
                    }
                    mMap.setCenter(
                        GeoCoordinate(
                            locationData["Latitude"]!!.toDouble(),
                            locationData["Longitude"]!!.toDouble(),
                            0.0
                        ), Map.Animation.NONE
                    )
                    val defaultMarker = MapMarker()
                    defaultMarker.coordinate = GeoCoordinate(
                        locationData["Latitude"]!!.toDouble(),
                        locationData["Longitude"]!!.toDouble(),
                        0.0
                    )
                    //defaultMarker.title = locationData["Description"]
                    mMap.addMapObject(defaultMarker)
                    mMap.addTransformListener(this)
                    mMap.zoomLevel = mMap.maxZoomLevel - 6
                    val coord = GeoCoordinate(
                        locationData["Latitude"]!!.toDouble(),
                        locationData["Longitude"]!!.toDouble()
                    )
                    val geoPosition = GeoPosition(coord)
                    updateLocationInfo(mLocationMethod!!, geoPosition)
                    //stopPositioningUpdates() // original added
                } catch (ex: IllegalArgumentException) {
                    // Handle invalid create search request parameters
                }
            }
        }










                /*try {
                    Log.d("description: ", locationData["Description"])

                        geoLocationList.add(data[i])
                        if (mNavigate) {
                            mNavigate = false
                            geoLocationListtemp = geoLocationList
                            createRoute()
                            geoLocationList.clear()
                            mMap.removeAllMapObjects()
                        }
                        mMap.setCenter(
                            GeoCoordinate(
                                locationData["Latitude"]!!.toDouble(),
                                locationData["Longitude"]!!.toDouble(),
                                0.0
                            ), Map.Animation.NONE
                        )
                        val defaultMarker = MapMarker()
                        defaultMarker.coordinate = GeoCoordinate(
                            locationData["Latitude"]!!.toDouble(),
                            locationData["Longitude"]!!.toDouble(),
                            0.0
                        )
                        defaultMarker.title = locationData["Description"]
                        mMap.addMapObject(defaultMarker)
                        mMap.addTransformListener(this)
                        mMap.zoomLevel = mMap.maxZoomLevel - 6
                        val coord = GeoCoordinate(
                            locationData["Latitude"]!!.toDouble(), locationData["Longitude"]!!
                                .toDouble()
                        )
                        val geoPosition = GeoPosition(coord)
                        updateLocationInfo(mLocationMethod!!, geoPosition)
                        stopPositioningUpdates()

                } catch (ex: IllegalArgumentException) {
                    // Handle invalid create search request parameters
                }
*/
    }


    private var geoLocationListtemp: List<HashMap<String, String>>? = null

    private  fun createRoute() {

        val coreRouter = CoreRouter()
        val routePlan = RoutePlan()
        val routeOptions = RouteOptions()
        /* Other transport modes are also available e.g Pedestrian */
        routeOptions.transportMode = RouteOptions.TransportMode.CAR
        /* Disable highway in this route. */
        routeOptions.setHighwaysAllowed(false)
        /* Calculate the shortest route available. */
        routeOptions.routeType = RouteOptions.Type.SHORTEST
        /* Calculate 1 route. */
        routeOptions.routeCount = 1
        /* Finally set the route option */
        routePlan.routeOptions = routeOptions


        Log.d("LocationData", "=Pickup Latitude : " + freshBatteryData?.pickupLatitude)
        Log.d("LocationData", "=Pickup Longitude : " + freshBatteryData?.pickupLongitude)
        Log.d("LocationData", "=Delivery Latitude : " + freshBatteryData?.deliveryLatitude)
        Log.d("LocationData", "=Delivery Longitude : " + freshBatteryData?.deliveryLongitude)
        Log.d(
            "LocationData",
            "Pickup => Delivery : " + freshBatteryData?.pickupLocationAddress + " ::: " + freshBatteryData?.deliveryLocationAddress
        )
        Log.d(
            "LocationData",
            "Pickup = Delivery : " + freshBatteryData?.pickupLocationName + " ::: " + freshBatteryData?.deliveryLocationName
        )


        if (geoLocationListtemp != null && geoLocationListtemp!!.size == 1) {
            var startPoint: RouteWaypoint? = null
            var destination: RouteWaypoint? = null
            for (i in geoLocationListtemp!!.indices) {
                val receivedCoordinate = mLastReceivedPosition!!.coordinate
                if (receivedCoordinate != null) {
                    startPoint = RouteWaypoint(receivedCoordinate)
                    destination = RouteWaypoint(
                        GeoCoordinate(
                            geoLocationListtemp!![0]["Latitude"]!!.toDouble(),
                            geoLocationListtemp!![0]["Longitude"]!!
                                .toDouble()
                        )
                    )
                }
            }

            routePlan.addWaypoint(startPoint!!)
            routePlan.addWaypoint(destination!!)

            /* Trigger the route calculation,results will be called back via the listener */
            coreRouter
                .calculateRoute(
                    routePlan,
                    object : Router.Listener<List<RouteResult>, RoutingError> {
                        override fun onProgress(i: Int) {
                            /* The calculation progress can be retrieved in this callback. */

                            Log.d("onProgressRoute", "" + i)

                        }

                        override fun onCalculateRouteFinished(
                            routeResults: List<RouteResult>,
                            routingError: RoutingError
                        ) {
                            /* Calculation is done.Let's handle the result */
                            if (routingError == RoutingError.NONE) {
                                if (routeResults[0].route != null) {
                                    m_route = routeResults[0].route
                                    val mapRoute = MapRoute(
                                        routeResults[0].route
                                    )

                                    mapRoute.isManeuverNumberVisible = true
                                    mMap.addMapObject(mapRoute)
                                    m_geoBoundingBox = routeResults[0].route.boundingBox
                                    mMap.zoomTo(
                                        m_geoBoundingBox!!, Map.Animation.NONE,
                                        Map.MOVE_PRESERVE_ORIENTATION
                                    )
                                    mDataBinding.stopCtrlButton.setVisibility(View.VISIBLE)
                                    mDataBinding.naviCtrlButton.setVisibility(View.GONE)

                                    startNavigation()
                                } else {
                                    Toast.makeText(
                                        mActivity,
                                        "Error:route results returned is not valid",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            } else {
                                Toast.makeText(
                                    mActivity, "Error:route calculation returned error code: "
                                            + routingError,
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    })
        }







        /*if (freshBatteryData?.pickupLatitude != null && freshBatteryData?.pickupLongitude != null &&
            freshBatteryData?.deliveryLatitude != null && freshBatteryData?.deliveryLongitude != null) {
            var startPoint: RouteWaypoint? = null
            var destination: RouteWaypoint? = null

            startPoint = RouteWaypoint(
                GeoCoordinate(
                    freshBatteryData?.pickupLatitude!!,
                    freshBatteryData?.pickupLongitude!!
                )
            )
            destination = RouteWaypoint(
                GeoCoordinate(
                    freshBatteryData?.deliveryLatitude!!,
                    freshBatteryData?.deliveryLongitude!!
                )
            )
            routePlan.addWaypoint(startPoint)
            routePlan.addWaypoint(destination)

            coreRouter
                .calculateRoute(
                    routePlan,
                    object : Router.Listener<List<RouteResult>, RoutingError> {
                        override fun onProgress(i: Int) {
                            *//* The calculation progress can be retrieved in this callback. *//*
                        }

                        override fun onCalculateRouteFinished(
                            routeResults: List<RouteResult>,
                            routingError: RoutingError
                        ) {
                            *//* Calculation is done.Let's handle the result *//*
                            if (routingError == RoutingError.NONE) {
                                if (routeResults[0].route != null) {
                                    m_route = routeResults[0].route
                                    *//* Create a MapRoute so that it can be placed on the map *//*
                                    val mapRoute = MapRoute(
                                        routeResults[0].route
                                    )

                                    *//* Show the maneuver number on top of the route *//*
                                    mapRoute.isManeuverNumberVisible = true

                                    *//* Add the MapRoute to the map *//*
                                    mMap.addMapObject(mapRoute)

                                    *//*
                                     * We may also want to make sure the map view is orientated properly
                                     * so the entire route can be easily seen.
                                     *//*
                                    m_geoBoundingBox = routeResults[0].route.boundingBox
                                    m_geoBoundingBox?.let {
                                        mMap.zoomTo(
                                            it, Map.Animation.NONE,
                                            Map.MOVE_PRESERVE_ORIENTATION
                                        )
                                    }
                                    mDataBinding.stopCtrlButton.setVisibility(View.VISIBLE)
                                    startNavigation()
                                } else {
                                    Toast.makeText(
                                        mActivity,
                                        "Error:route results returned is not valid",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            } else {
                                Toast.makeText(
                                    mActivity, "Error:route calculation returned error code: "
                                            + routingError,
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    })
        }*/




    }

    private fun removeMapObjects() {
        geoLocationList.clear()
        mMap.removeAllMapObjects()
    }

    private  fun startNavigation() {
        // m_naviControlButton.setText(R.string.stop_navi);
        /* Configure Navigation manager to launch navigation on current map */
        m_navigationManager!!.setMap(mMap)

        /*
         * Start the turn-by-turn navigation.Please note if the transport mode of the passed-in
         * route is pedestrian, the NavigationManager automatically triggers the guidance which is
         * suitable for walking. Simulation and tracking modes can also be launched at this moment
         * by calling either simulate() or startTracking()
         */

        /* Choose navigation modes between real time navigation and simulation */
        val alertDialogBuilder = AlertDialog.Builder(mActivity)
        alertDialogBuilder.setTitle("Navigation")
        alertDialogBuilder.setMessage("Choose Mode")
        alertDialogBuilder.setNegativeButton(
            "Navigation"
        ) { dialoginterface, i ->
            m_navigationManager!!.startNavigation(m_route!!)
            mMap.tilt = 60f
            startForegroundService()
        }
        alertDialogBuilder.setPositiveButton(
            "Simulation"
        ) { dialoginterface, i ->
            m_navigationManager!!.simulate(m_route!!, 60) //Simualtion speed is set to 60 m/s
            mMap.tilt = 60f
            startForegroundService()



        }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()


        /*
         * Set the map update mode to ROADVIEW.This will enable the automatic map movement based on
         * the current location.If user gestures are expected during the navigation, it's
         * recommended to set the map update mode to NONE first. Other supported update mode can be
         * found in HERE Mobile SDK for Android (Premium) API doc
         */m_navigationManager!!.mapUpdateMode = NavigationManager.MapUpdateMode.ROADVIEW

        /*
         * NavigationManager contains a number of listeners which we can use to monitor the
         * navigation status and getting relevant instructions.In this example, we will add 2
         * listeners for demo purpose,please refer to HERE Android SDK API documentation for details
         */
        addNavigationListeners()
    }

    open fun stopNavigation() {
        m_navigationManager!!.stop()
        /*
         * Restore the map orientation to show entire route on screen
         */
        mMap.zoomTo(m_geoBoundingBox!!, Map.Animation.NONE, 0f)
        mDataBinding.stopCtrlButton.setVisibility(View.GONE)
        mDataBinding.naviCtrlButton.setVisibility(View.VISIBLE)

        m_route = null
    }


    private fun addNavigationListeners() {
        /*
         * Register a NavigationManagerEventListener to monitor the status change on
         * NavigationManager
         */


//        m_navigationManager!!.addNavigationManagerEventListener(
//            WeakReference(m_navigationManagerEventListener)
//        )
//        m_navigationManager!!.addPositionListener(
//            WeakReference(m_positionListener)
//        )

         //Register a AudioPlayerDelegate to monitor TTS text
        /*m_navigationManager!!.audioPlayer.setDelegate(
            m_audioPlayerDelegate
        )*/


        m_navigationManager!!.addNewInstructionEventListener(WeakReference(instructListener))


        val voiceCatalog = VoiceCatalog.getInstance()
        voiceCatalog.downloadCatalog(object : OnDownloadDoneListener {
            override fun onDownloadDone(p0: VoiceCatalog.Error?) {

            }

        })

        val voicePackages = VoiceCatalog.getInstance().catalogList

        var id: Long = -1

        for (vPackage in voicePackages) {
            if (vPackage.marcCode.compareTo("eng", ignoreCase = true) == 0) {
                if (vPackage.isTts) {
                    id = vPackage.id

                    if (!voiceCatalog.isLocalVoiceSkin(id)) {
                        voiceCatalog.downloadVoice(id, object : OnDownloadDoneListener {
                            override fun onDownloadDone(p0: VoiceCatalog.Error?) {
                                TODO("Not yet implemented")
                            }

                        })
                    }
                    break
                }
            }
        }



        val voiceGuidanceOptions: VoiceGuidanceOptions = m_navigationManager!!.getVoiceGuidanceOptions()
        voiceCatalog.getLocalVoiceSkin(id)?.let { voiceGuidanceOptions.setVoiceSkin(it) }



        //Lane Information
        guidanceManeuverPresenter = GuidanceManeuverPresenter(context, getInstance(), m_route)
        guidanceManeuverPresenter!!.addListener(object : GuidanceManeuverListener {
            override fun onDataChanged(guidanceManeuverData: GuidanceManeuverData?) {
                if (guidanceManeuverData == null) {
                    mDataBinding.guidanceManeuverView.setViewState(GuidanceManeuverView.State.UPDATING)
                } else {
                    Log.d(TAG, "onDataChanged: 1st line: " + guidanceManeuverData.info1)
                    Log.d(TAG, "onDataChanged: 2nd line: " + guidanceManeuverData.info2)
                    mDataBinding.guidanceManeuverView.setViewState(
                        GuidanceManeuverView.State(
                            guidanceManeuverData
                        )
                    )
                }
            }

            override fun onDestinationReached() {
                Log.d(TAG, "onDestinationReached")
                mDataBinding.guidanceManeuverView.highLightManeuver(Color.BLUE)
            }
        })

        startGuidanceSimulation()


    }

    fun startGuidanceSimulation() {
        if (m_route == null || guidanceManeuverPresenter == null) {
            return
        }
        guidanceManeuverPresenter!!.resume()
        //GuidanceSimulator.getInstance().startGuidanceSimulation(route, map)
    }

    fun stopGuidanceSimulation() {
        if (guidanceManeuverPresenter == null) {
            return
        }
        guidanceManeuverPresenter!!.pause()
        //GuidanceSimulator.getInstance().stopGuidance()
    }


    private fun startForegroundService() {
        if (!m_foregroundServiceStarted) {
            m_foregroundServiceStarted = true
            val startIntent = Intent(mActivity, ForegroundService::class.java)
            startIntent.action = START_ACTION
            mActivity.applicationContext.startService(startIntent)
            mDataBinding.stopCtrlButton.setVisibility(View.VISIBLE)
            mDataBinding.naviCtrlButton.setVisibility(View.GONE)
        }
    }

    var START_ACTION = "com.here.android.service.fs.action.start"
    var STOP_ACTION = "com.here.android.service.fs.action.stop"
    private  fun stopForegroundService() {
        if (m_foregroundServiceStarted) {
            m_foregroundServiceStarted = false
            val stopIntent = Intent(mActivity, ForegroundService::class.java)
            stopIntent.action = STOP_ACTION
            mActivity.applicationContext.startService(stopIntent)
        }
    }
    /**
     * Stop position updates
     * and remove listener
     */
    protected fun stopPositioningUpdates() {
        Log.v(TAG, "stopPositioningUpdates")
        if (mPositioningManager != null) {
            mPositioningManager!!.stop()
            mPositioningManager!!.removeListener(this)
        }
        if (mRadioMapLoader != null) {
            mVenueService!!.removeVenueLoadListener(mVenueLoadListener)
            mRadioMapLoader = null
        }
    }

    private fun getCurrentLocation() {

        mDataBinding.stopCtrlButton.setVisibility(View.GONE)
        mDataBinding.naviCtrlButton.setVisibility(View.VISIBLE)

        mMap.removeAllMapObjects()
        m_navigationManager!!.stop()
        mMap.addTransformListener(this)
        mMap.zoomLevel = mMap.maxZoomLevel - 3
        try {
            startPositionUpdates()
            mVenueMapFragment!!.positionIndicator.isVisible = true
        } catch (ex: java.lang.Exception) {
            Log.w(
                TAG,
                "startPositionUpdates: Could not register for location updates: %s",
                Log.getStackTraceString(ex)
            )
        }

        Log.d("mLastMapCenter", "" + mLastMapCenter!!.latitude + " = " + mLastMapCenter!!.longitude)



        mMap.setCenter(mLastMapCenter!!, Map.Animation.NONE)

        m_route = null
    }

}