package com.dispachio.data.model.response.loadSearch


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoadStatus(
    @SerializedName("loadStatusId")
    val loadStatusId: Int,
    @SerializedName("statusValue")
    val statusValue: String
) : Parcelable