package com.dispachio.ui.view.activity

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.amazonaws.mobile.client.AWSMobileClient
import com.dispachio.R
import com.dispachio.databinding.ActivityNavigationBinding
import com.dispachio.ui.base.*
import com.dispachio.ui.view.fragment.DashboardFragment
import com.dispachio.ui.view.fragment.LedgersFragment
import com.dispachio.ui.view.fragment.ProfileFragment
import com.dispachio.ui.view.fragment.milestone.MilestoneFragment
import com.dispachio.ui.viewmodel.DashboardViewModel
import com.dispachio.utils.DeleteCache
import com.dispachio.utils.SharedPreference
import com.dispachio.utils.setSafeOnClickListener
import com.google.android.material.navigation.NavigationView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.nav_header_main.view.*


@AndroidEntryPoint
class NavigationActivity : BaseActivity<ActivityNavigationBinding>(R.layout.activity_navigation),
    NavigationView.OnNavigationItemSelectedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration

    private val dashBordViewModel: DashboardViewModel by viewModels()
    override fun initView() {
        try {
            AWSMobileClient.getInstance().initialize(this).execute()
        } catch (e: Exception) {
            Log.i("Exception", e.printStackTrace().toString())
        }

        com.dispachio.utils.Log.mEnabled = true



        //mDataBinding.navView.nameTxt?.text = dashBordViewModel.getName()

        //original
        addFragments(DashboardFragment.newInstance(null), R.id.containerView, false)
    }

    var drawerLayout: DrawerLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        allowForLoad()

        setSupportActionBar(mDataBinding.appBarMain.toolbar)

        drawerLayout = mDataBinding.drawerLayout

        val headerView: View = mDataBinding.navView.inflateHeaderView(R.layout.nav_header_main)
        val userText: TextView = headerView.findViewById(R.id.nameTxt) as TextView

        userText.setText(dashBordViewModel.getName())

        val navView: NavigationView = mDataBinding.navView
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_ledger, R.id.nav_profile
            ), drawerLayout
        )

        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        if (navView != null) {
            navView.setNavigationItemSelectedListener(this)
        }
    }

    fun disableSideNavigation(){
        drawerLayout!!.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED) // disable swipe
    }

    fun enableSideNavigation(){
        drawerLayout!!.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED) // enable swipe
    }

    fun openSideDrawer(){
        drawerLayout?.open()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun allowForLoad() {
        try {
            if (ContextCompat.checkSelfPermission(
                    applicationContext,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ),
                    101
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private val GET_FROM_GALLERY = 3

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GET_FROM_GALLERY && resultCode == RESULT_OK && null != data) {
            val selectedImage: Uri? = data.data
            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
            val cursor: Cursor? =
                selectedImage?.let {
                    this.contentResolver.query(
                        it,
                        filePathColumn,
                        null,
                        null,
                        null
                    )
                }
            cursor?.moveToFirst()
            val columnIndex: Int? = cursor?.getColumnIndex(filePathColumn[0])
            val picturePath: String? = columnIndex?.let { cursor.getString(it) }

            when {
                dashBordViewModel.getPicturePath().isNullOrEmpty() -> {
                    dashBordViewModel.savePicturePath(picturePath)
                }
                dashBordViewModel.getPicturePath1().isNullOrEmpty() -> {
                    dashBordViewModel.savePicturePath1(picturePath)
                }
                dashBordViewModel.getPicturePath2().isNullOrEmpty() -> {
                    dashBordViewModel.savePicturePath2(picturePath)
                }
            }

        }
    }

    private val PERMISSION_REQUEST_CODE = 200

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            enableSideNavigation()
            when (getCurrentFragment()) {
                is DashboardFragment -> logoutDialogue()
                is MilestoneFragment -> startActivity(Intent(this, NavigationActivity::class.java))
                //is StartRoutingFragment -> startActivity(Intent(this, NavigationActivity::class.java))

                else -> popBackStack()
            }
        } else {
            logoutDialogue()
        }
    }

    private fun logoutDialogue() {
        val layoutInflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val builder = AlertDialog.Builder(this, R.style.Theme_Dialog)
        assert(layoutInflater != null)
        val layout: View = layoutInflater.inflate(R.layout.generic_dialogue, null)
        builder.setView(layout)
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 20)
        alertDialog.window!!.setBackgroundDrawable(inset)
        alertDialog.cancel()
        alertDialog.show()
        alertDialog.setCancelable(false)
        val okButton = layout.findViewById<Button>(R.id.okButton)
        val cancelButton = layout.findViewById<Button>(R.id.cancelButton)
        cancelButton.visibility = View.VISIBLE
        okButton.setSafeOnClickListener {
            logout()
            this.toast("You've logged out completely.")
            alertDialog.dismiss()
        }
        cancelButton.setSafeOnClickListener {
            alertDialog.dismiss()
        }
    }

    private fun logout() {
        startActivity(Intent(this, SplashAndLoginActivity::class.java))
        SharedPreference(this).deleteAllSharedPreference()
        DeleteCache(this).deleteCache()
        finishAffinity()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        when (id) {
            R.id.nav_home -> {
                // addFragments(DashboardFragment.newInstance(null), R.id.containerView, false)
                startActivity(
                    Intent(
                        this,
                        NavigationActivity::class.java
                    )
                )
                finish()
                drawerLayout?.closeDrawers()
                return true
            }
            R.id.nav_profile -> {
                addFragments(
                    ProfileFragment.newInstance(),
                    R.id.containerView,
                    true
                )
                drawerLayout?.closeDrawers()
                return true
            }
            R.id.nav_ledger -> {
                addFragments(
                    LedgersFragment.newInstance(),
                    R.id.containerView,
                    true
                )
                drawerLayout?.closeDrawers()
                return true
            }
            R.id.nav_logout1 -> {
                logoutDialogue()
            }
        }
        return true
    }


}