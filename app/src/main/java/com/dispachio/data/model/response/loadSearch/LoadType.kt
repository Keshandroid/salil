package com.dispachio.data.model.response.loadSearch


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoadType(
    @SerializedName("loadTypeId")
    val loadTypeId: Int,
    @SerializedName("typeValue")
    val typeValue: String
) : Parcelable