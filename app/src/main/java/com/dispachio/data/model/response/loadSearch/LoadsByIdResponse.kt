package com.dispachio.data.model.response.loadSearch

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class LoadsByIdResponse(
    @SerializedName("accessorialSetupIdList")
    val accessorialSetupIdList: String?=null,
    @SerializedName("accessorialSetupList")
    val accessorialSetupList: List<AccessorialSetup>?=null,
    @SerializedName("bookingNo")
    val bookingNo: String?=null,
    @SerializedName("carrierPrice")
    val carrierPrice: Double?=null,
    @SerializedName("chassisType")
    val chassisType: ChassisType?=null,
    @SerializedName("chassisTypeId")
    val chassisTypeId: Int?=null,
    @SerializedName("commissionPercentage")
    val commissionPercentage: String?=null,
    @SerializedName("containerId")
    val containerId: Int?=null,
    @SerializedName("containerNumber")
    val containerNumber: String?=null,
    @SerializedName("deliveryDate")
    val deliveryDate: String?=null,
    @SerializedName("deliveryLatitude")
    val deliveryLatitude: Double?=null,
    @SerializedName("deliveryLocationAddress")
    val deliveryLocationAddress: String?=null,
    @SerializedName("deliveryLocationName")
    val deliveryLocationName: String?=null,
    @SerializedName("deliveryLongitude")
    val deliveryLongitude: Double?=null,
    @SerializedName("deliveryTime")
    val deliveryTime: String?=null,
    @SerializedName("deliveryType")
    val deliveryType: DeliveryType?=null,
    @SerializedName("deliveryTypeId")
    val deliveryTypeId: Int?=null,
    @SerializedName("height")
    val height: Height?=null,
    @SerializedName("heightId")
    val heightId: Int?=null,
    @SerializedName("isNotifyAccessorials")
    val isNotifyAccessorials: Boolean?=null,
    @SerializedName("isNotifyMilestones")
    val isNotifyMilestones: Boolean?=null,
    @SerializedName("isProfileCompleted")
    val isProfileCompleted: String?=null,
    @SerializedName("length")
    val length: Length?=null,
    @SerializedName("lengthId")
    val lengthId: Int?=null,
    @SerializedName("loadContainer")
    val loadContainer: LoadContainer?=null,
    @SerializedName("loadCreated")
    val loadCreated: String?=null,
    @SerializedName("loadId")
    val loadId: Int?=null,
    @SerializedName("loadStatus")
    val loadStatus: LoadStatus?=null,
    @SerializedName("loadStatusId")
    val loadStatusId: Int?=null,
    @SerializedName("loadType")
    val loadType: LoadType?=null,
    @SerializedName("loadTypeId")
    val loadTypeId: Int?=null,
    @SerializedName("milestones")
    val milestones: List<Milestone>?=null,
    @SerializedName("notifyEmail")
    val notifyEmail: String?=null,
    @SerializedName("pickupDate")
    val pickupDate: String?=null,
    @SerializedName("pickupLatitude")
    val pickupLatitude: Double?=null,
    @SerializedName("pickupLocationAddress")
    val pickupLocationAddress: String?=null,
    @SerializedName("pickupLocationName")
    val pickupLocationName: String?=null,
    @SerializedName("pickupLongitude")
    val pickupLongitude: Double?=null,
    @SerializedName("pickupTime")
    val pickupTime: String?=null,
    @SerializedName("price")
    val price: Double?=null,
    @SerializedName("truckingUserId")
    val truckingUserId: Int?=null
) : Parcelable