package com.dispachio.ui.view.fragment

import android.content.Intent
import androidx.fragment.app.activityViewModels
import com.dispachio.R
import com.dispachio.databinding.FragmentPastLoadsBinding
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.base.toast
import com.dispachio.ui.view.activity.SplashAndLoginActivity
import com.dispachio.ui.viewmodel.DashboardViewModel
import com.dispachio.utils.SharedPreference
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PastLoadsFragment : BaseFragment<FragmentPastLoadsBinding>(R.layout.fragment_past_loads) {

    private val dashboardViewModel: DashboardViewModel by activityViewModels()

    override fun initView() {
        mActivity.startActivity(Intent(mActivity, SplashAndLoginActivity::class.java))
        SharedPreference(requireContext()).deleteAllSharedPreference()
        mActivity.toast("You've logged out completely.")
    }

}