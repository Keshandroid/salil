package com.dispachio.data.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ApplyForLoadResponse(
    @SerializedName("loadApprovalId")
    val loadApprovalId: Int? = 0,
    @SerializedName("loadId")
    val loadId: Int? = 0,
    @SerializedName("appliedOn")
    val appliedOn: String? = null,
    @SerializedName("loadStatusId")
    val loadStatusId: Int? = 0,
    @SerializedName("carrierUserId")
    val carrierUserId: Int? = null,
    @SerializedName("truckingUserId")
    val truckingUserId: String? = null,
    @SerializedName("remarks")
    val remarks: String? = null,
    @SerializedName("loadStatus")
    val loadStatus: String? = null,
    @SerializedName("loads")
    val loads: String? = null,
    @SerializedName("carrierUser")
    val carrierUser: String? = null
): Parcelable
