package com.dispachio.data.model.response.login


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoginResponse(
    @SerializedName("additionalInsuredFileName")
    val additionalInsuredFileName: String,
    @SerializedName("companyCANumber")
    val companyCANumber: String,
    @SerializedName("companyCity")
    val companyCity: String,
    @SerializedName("companyInsuranceAgentContactNo")
    val companyInsuranceAgentContactNo: String,
    @SerializedName("companyInsuranceAgentEmail")
    val companyInsuranceAgentEmail: String,
    @SerializedName("companyInsuranceAgentName")
    val companyInsuranceAgentName: String,
    @SerializedName("companyMCNumber")
    val companyMCNumber: String,
    @SerializedName("companyName")
    val companyName: String,
    @SerializedName("companyScac")
    val companyScac: String,
    @SerializedName("companyState")
    val companyState: String,
    @SerializedName("companyType")
    val companyType: CompanyType,
    @SerializedName("companyTypeId")
    val companyTypeId: Int,
    @SerializedName("companyUsDoot")
    val companyUsDoot: String,
    @SerializedName("companyZipCode")
    val companyZipCode: String,
    @SerializedName("contactNumber")
    val contactNumber: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("entityId")
    val entityId: String,
    @SerializedName("federalTaxId")
    val federalTaxId: String,
    @SerializedName("insuranceFileName")
    val insuranceFileName: String,
    @SerializedName("isActive")
    val isActive: Boolean,
    @SerializedName("isEmailVerified")
    val isEmailVerified: String,
    @SerializedName("isProfileCompleted")
    val isProfileCompleted: Boolean,
    @SerializedName("lastLoginTime")
    val lastLoginTime: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("registeredOn")
    val registeredOn: String,
    @SerializedName("registrantionFileName")
    val registrantionFileName: String,
    @SerializedName("truckLicenceNumber")
    val truckLicenceNumber: String,
    @SerializedName("truckMake")
    val truckMake: String,
    @SerializedName("truckYear")
    val truckYear: String,
    @SerializedName("userId")
    val userId: Int
) : Parcelable