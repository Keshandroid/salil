package com.dispachio.ui.view.fragment


import android.Manifest
import android.app.Dialog
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.activityViewModels
import com.dispachio.R
import com.dispachio.data.model.request.RegistrationRequestModel
import com.dispachio.databinding.CarrierRegisterationBinding
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.base.addFragments
import com.dispachio.ui.base.toast
import com.dispachio.ui.viewmodel.LoginRegisterViewModel
import com.dispachio.utils.Log
import com.dispachio.utils.Utility.openGallery
import com.dispachio.utils.Utility.sendLongSMS
import com.dispachio.utils.setSafeOnClickListener
import com.example.dispachio.common.Status
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class RegistrationFragment :
    BaseFragment<CarrierRegisterationBinding>(R.layout.carrier_registeration) {

    private val loginRegisterViewModel: LoginRegisterViewModel by activityViewModels()

    @Inject
    lateinit var progressDialog: Dialog

    private var submitclick: Boolean = false

    override fun initView() {

        Log.mEnabled = true

        checkSMSPermission()
        initObserver()
        mDataBinding.verifyOtp.setSafeOnClickListener {
            if (!mDataBinding.editTextTextContactNumber.text.isNullOrEmpty()) {
                val otp = sendLongSMS(mDataBinding.editTextTextContactNumber.text.toString())
                loginRegisterViewModel.otpSent(otp)
                mDataBinding.editTextTextOTP.isEnabled = true
            } else {
                mActivity.toast(mActivity.resources.getString(R.string.enter_number))
            }
        }
        mDataBinding.editTextTextOTP.doAfterTextChanged {
            if (!mDataBinding.editTextTextOTP.text.isNullOrEmpty()) {
                if (loginRegisterViewModel.otpChecked()?.equals(it.toString(), true) == true) {
                    mActivity.toast(mActivity.resources.getString(R.string.otp_verified))
                }
            }
        }
        mDataBinding.submitBtn.setSafeOnClickListener {

            var registrationRequestModel = RegistrationRequestModel()
            registrationRequestModel.name = mDataBinding.editTextTextEnterName.text.toString()
            registrationRequestModel.contactNumber =
                mDataBinding.editTextTextContactNumber.text.toString()
            registrationRequestModel.email = mDataBinding.editTextTextEmailAddress.text.toString()
            registrationRequestModel.password = mDataBinding.editTextPassword.text.toString()
            registrationRequestModel.userName = mDataBinding.editTextPassword.text.toString()
            registrationRequestModel.isProfileCompleted = false
            registrationRequestModel.isActive = true
            submitclick = true
            loginRegisterViewModel.setRegistrationData(registrationRequestModel)


        }

        mDataBinding.loginLink.setSafeOnClickListener {
            mActivity.addFragments(
                LoginScreenFragment.newInstance(),
                R.id.logingContainer,
                false
            )
        }
    }

    private fun checkSMSPermission() {

        if (ContextCompat.checkSelfPermission(
                mActivity.applicationContext,
                Manifest.permission.SEND_SMS
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            mActivity.toast("SMS permission allowed.")
        } else {
            ActivityCompat.requestPermissions(
                mActivity, arrayOf(Manifest.permission.SEND_SMS),
                PERMISSIONS_REQUEST_SMS
            )
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        loginRegisterViewModel.registrationResponse.removeObservers(viewLifecycleOwner)
    }

    fun initObserver() {

        loginRegisterViewModel.registrationResponse.observe(
            viewLifecycleOwner, androidx.lifecycle.Observer {
                loginRegisterViewModel.otpSent(null)
                when (it.status) {
                    Status.SUCCESS -> {
                       progressDialog.dismiss()
                        mActivity.toast(mActivity.resources.getString(R.string.successfully_registred))
                        clearForm()
                        if (submitclick) {
                            submitclick = false
                            loginRegisterViewModel.registrationResponse.removeObservers(
                                viewLifecycleOwner
                            )
                            mActivity.addFragments(
                                LoginScreenFragment.newInstance(),
                                R.id.logingContainer,
                                false
                            )

                        }
                    }
                    Status.LOADING -> {
                       progressDialog.show()
                    }
                    Status.ERROR -> {
                        progressDialog.dismiss()
                        mActivity.toast(it.message.toString())
                    }

                }
            }
        )
    }

    private fun clearForm() {
        mDataBinding.editTextTextEnterName.setText("")
        mDataBinding.editTextTextContactNumber.setText("")
        mDataBinding.editTextTextOTP.setText("")
        mDataBinding.editTextTextOTP.isEnabled = false
        mDataBinding.editTextPassword.setText("")
        mDataBinding.editTextConfPassword.setText("")
        progressDialog.dismiss()

    }

    companion object {
        private const val PERMISSIONS_REQUEST_SMS = 2

        @JvmStatic
        fun newInstance() =
            RegistrationFragment()
                .apply {

                }
    }

}