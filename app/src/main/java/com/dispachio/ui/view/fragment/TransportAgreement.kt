package com.dispachio.ui.view.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.activityViewModels
import com.dispachio.R
import com.dispachio.data.model.request.ApplyForLoadRequest
import com.dispachio.databinding.TransportAgreementBinding
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.base.addFragments
import com.dispachio.ui.base.toast
import com.dispachio.ui.view.activity.NavigationActivity
import com.dispachio.ui.viewmodel.DashboardViewModel
import com.dispachio.utils.Log
import com.dispachio.utils.SharedPreference
import com.dispachio.utils.setSafeOnClickListener
import com.example.dispachio.common.Status
import com.google.gson.GsonBuilder


private const val ARG_PARAM1 = "param1"


class TransportAgreement : BaseFragment<TransportAgreementBinding>(R.layout.transport_agreement) {
    private val dashboardViewModel: DashboardViewModel by activityViewModels()

    private var loadId: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            loadId = it.getInt(ARG_PARAM1)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: Int) =
            TransportAgreement().apply {
                arguments = Bundle().apply {
                    putInt(ARG_PARAM1, param1)
                }
            }
    }

    override fun initView() {
        initObserver()

        val userid = dashboardViewModel.getUserId()

        Log.d("loadApplyParams"," =carrierUserId="+ userid?.toInt() + " =loadId= "+loadId)


        mDataBinding.submitBtn.setSafeOnClickListener {
            if (mDataBinding.checkBox.isChecked) {
                val userId = dashboardViewModel.getUserId()
                val applyForLoadRequest = ApplyForLoadRequest()
                
                Log.d("loadApplyParams"," == "+GsonBuilder().setPrettyPrinting().create().toJson(applyForLoadRequest))

                applyForLoadRequest.carrierUserId = userId?.toInt()
                applyForLoadRequest.loadId = loadId
                dashboardViewModel.applyForLoads(applyForLoadRequest)
            } else {
                mActivity.toast(mActivity.resources.getString(R.string.action))
            }
        }
    }

    private fun initObserver() {
        dashboardViewModel.applyForLoadsModel.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    mActivity.startActivity(Intent(context, NavigationActivity::class.java))
                    mActivity.finish()
                    mActivity.toast("Successfully applied for this Load.")

                    Log.d("loadApplyParams",""+"Success...")
                }
                Status.ERROR -> {

                    Log.d("loadApplyParams",""+it.message.toString())

                    mActivity.toast(it.message.toString())
                }
            }

        })
    }

}