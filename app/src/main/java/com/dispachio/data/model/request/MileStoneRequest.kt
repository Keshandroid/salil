package com.dispachio.data.model.request


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MileStoneRequest(
    @SerializedName("answer1")
    var answer1: Int?=null,
    @SerializedName("answer2")
    var answer2: Int?=null,
    @SerializedName("answer3")
    var answer3: Int?=null,
    @SerializedName("answer4")
    var answer4: Int?=null,
    @SerializedName("answer5")
    var answer5: String?=null,

    @SerializedName("curDate")
    var curDate: String?=null,
    @SerializedName("curTime")
    var curTime: String?=null,
    @SerializedName("firstImageName")
    var firstImageName: String?=null,
    @SerializedName("latitude")
    var latitude: Double?=null,
    @SerializedName("loadId")
    var loadId: Int?=null,
    @SerializedName("longitude")
    var longitude: Double?=null,
    @SerializedName("milestoneStatusId")
    var milestoneStatusId: Int?=null,
    @SerializedName("question1")
    var question1: Int?=null,
    @SerializedName("question2")
    var question2: Int?=null,
    @SerializedName("question3")
    var question3: Int?=null,
    @SerializedName("question4")
    var question4: Int?=null,
    @SerializedName("question5")
    var question5: Int?=null,
    @SerializedName("secondImageName")
    var secondImageName: String?=null
) : Parcelable