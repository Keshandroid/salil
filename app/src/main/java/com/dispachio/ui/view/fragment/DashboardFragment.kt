package com.dispachio.ui.view.fragment

import android.Manifest
import android.app.AlertDialog
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dispachio.R
import com.dispachio.data.model.response.loadSearch.LoadsByDistanceResponse
import com.dispachio.data.model.response.loadSearch.LoadsByDistanceResponseItem
import com.dispachio.databinding.HomeScreenBinding
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.base.addFragments
import com.dispachio.ui.base.replaceFragment
import com.dispachio.ui.base.toast
import com.dispachio.ui.view.activity.NavigationActivity
import com.dispachio.ui.view.activity.SplashAndLoginActivity
import com.dispachio.ui.view.fragment.milestone.NOTIFY_MILESTONE_FRAGMENT
import com.dispachio.ui.viewmodel.DashboardViewModel
import com.dispachio.utils.DeleteCache
import com.dispachio.utils.Log
import com.dispachio.utils.SharedPreference
import com.dispachio.utils.setSafeOnClickListener
import com.example.dispachio.common.Status
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 * Use the [DashboardFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class DashboardFragment : BaseFragment<HomeScreenBinding>(R.layout.home_screen) {
    private var lastKnownLocation: Location? = null

    private val dashboardViewModel: DashboardViewModel by activityViewModels()
    lateinit var loadList: LoadsByDistanceResponse
    lateinit var awaitingApprovalLoadList: LoadsByDistanceResponse
    lateinit var activeLoadList: LoadsByDistanceResponse

    lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    var fiteredData: LoadsByDistanceResponse? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            fiteredData = it.getParcelable(ARG_PARAM1)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as NavigationActivity).enableSideNavigation()
    }

    override fun onResume() {
        super.onResume()
    }

    @Inject
    lateinit var progressDialog: Dialog
    override fun initView() {

        mDataBinding.filterOpt.setOnClickListener {
            /*mActivity.addFragments(
                FilterFragment.newInstance(),
                R.id.dashboardContainer,
                true
            )*/
            mActivity.replaceFragment(
                FilterFragment.newInstance(),
                R.id.dashboardContainer
            )
        }

        mDataBinding.include.imageView2.setSafeOnClickListener {
            (activity as NavigationActivity).openSideDrawer()
        }
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mActivity)

        getLocationPermission()
        getDeviceLocation()
        progressDialog = Dialog(mActivity)
        initObserver()
        //    dashboardViewModel.setCurentValueFragment("This is Ledgers page")

        var userId = dashboardViewModel.getUserId()

        userId?.let { dashboardViewModel.getApprovalLoads(it.toInt(), 3) }
        userId?.let { dashboardViewModel.getApprovalLoads(it.toInt(), 4) }

        //originally added
        /*if (fiteredData != null) {
            loadList = fiteredData!!
        }*/
        mDataBinding.constraintLayout2.setSafeOnClickListener {
            if (!mDataBinding.numberTxt.text.isNullOrEmpty() && mDataBinding.numberTxt.text.toString()
                    .toInt() != 0
            ) {

                /*mActivity.addFragments(
                    LoadListFragment.newInstance(
                        loadList,
                        mActivity.resources.getString(R.string.load_details)
                    ),
                    R.id.dashboardContainer,
                    true
                )*/

                if (fiteredData != null) {
                    loadList = fiteredData!!
                }

                mActivity.replaceFragment(
                    LoadListFragment.newInstance(
                        loadList,
                        mActivity.resources.getString(R.string.load_details)
                    ),
                    R.id.dashboardContainer
                )

            } else {
                mActivity.toast(mActivity.resources.getString(R.string.no_data_assign))
            }


        }
        mDataBinding.constraintLayout3.setSafeOnClickListener {
            if (!mDataBinding.numberTxt3.text.isNullOrEmpty() && mDataBinding.numberTxt3.text.toString()
                    .toInt() != 0
            ) {
                /*mActivity.addFragments(
                    LoadListFragment.newInstance(
                        activeLoadList,
                        mActivity.resources.getString(R.string.start_routing)
                    ),
                    R.id.dashboardContainer,
                    true
                )*/

                mActivity.replaceFragment(
                    LoadListFragment.newInstance(
                        activeLoadList,
                        mActivity.resources.getString(R.string.start_routing)
                    ),
                    R.id.dashboardContainer
                )

            } else {
                mActivity.toast(mActivity.resources.getString(R.string.no_data_assign))
            }
        }
        mDataBinding.constraintLayout.setSafeOnClickListener {
            if (!mDataBinding.numberTxt2.text.isNullOrEmpty() && mDataBinding.numberTxt2.text.toString()
                    .toInt() != 0
            ) {
                /*mActivity.addFragments(
                    LoadListFragment.newInstance(
                        awaitingApprovalLoadList,
                        mActivity.resources.getString(R.string.awaiting_approval)
                    ),
                    R.id.dashboardContainer,
                    true
                )*/


                mActivity.replaceFragment(
                    LoadListFragment.newInstance(
                        awaitingApprovalLoadList,
                        mActivity.resources.getString(R.string.awaiting_approval)
                    ),
                    R.id.dashboardContainer
                )

            } else {
                mActivity.toast(mActivity.resources.getString(R.string.no_data_assign))
            }
        }
    }

    private var locationPermissionGranted = false

    // [START maps_current_place_location_permission]
    private fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(
                mActivity.applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            locationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(
                mActivity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    private fun getDeviceLocation() {

        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (locationPermissionGranted) {
                val locationResult = fusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(mActivity) { task ->
                    if (task.isSuccessful) {
                        // Set the map's camera position to the current location of the device.
                        lastKnownLocation = task.result
                        if (lastKnownLocation != null) {
                            getLoadsByLocation(
                                lastKnownLocation!!.latitude,
                                lastKnownLocation!!.longitude
                            )

                            /*  map?.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                  LatLng(lastKnownLocation!!.latitude,
                                      lastKnownLocation!!.longitude), DEFAULT_ZOOM.toFloat()))*/
                        }
                    }
                }
            }
        } catch (e: SecurityException) {
            //Log.e("Exception: %s", e.message, e)
        }
    }

    private fun getLoadsByLocation(latitude: Double, longitude: Double) {
        val stringLatitude = latitude
        val stringLongitude = longitude
        /*       val stringLatitude: String = "21.15411690396548"
               val stringLongitude: String = "72.88280470711373"*/

        dashboardViewModel.getLoadsByDistanceCall(
            stringLatitude,
            stringLongitude,
            10
        )
    }

    fun initObserver() {
        dashboardViewModel.awaitingApprovalLoadsModel.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    mDataBinding.numberTxt2.text = it.data?.size.toString()
                    awaitingApprovalLoadList = it.data!!
                    progressDialog.dismiss()
                }
                Status.LOADING -> {
                    progressDialog.show()

                }
                Status.ERROR -> {
                    progressDialog.dismiss()
                }
            }
        })
        dashboardViewModel.activeLoadsModel.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    mDataBinding.numberTxt3.text = it.data?.size.toString()
                    activeLoadList = it.data!!
                    progressDialog.dismiss()
                }
                Status.LOADING -> {
                    progressDialog.show()

                }
                Status.ERROR -> {
                    progressDialog.dismiss()
                }
            }
        })
        dashboardViewModel.getLoadsByDistanceModel.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    mDataBinding.numberTxt.text = it.data?.size.toString()
                    loadList = it.data!!
                    progressDialog.dismiss()
                }
                Status.LOADING -> {
                    progressDialog.show()

                }
                Status.ERROR -> {
                    progressDialog.dismiss()
                }
            }
        })

        /*   dashboardViewModel.loadListModel.observe(viewLifecycleOwner, {
               when (it.status) {
                   Status.SUCCESS -> {
                       if (it.data?.numberOfElements != null) {
                           loadList = it.data
                           mDataBinding.numberTxt.text = it.data.numberOfElements.toString()
                       } else {
                           mDataBinding.numberTxt.text = 0.toString()
                       }
                       progressDialog.dismiss()
                   }

                   Status.ERROR -> {
                       progressDialog.dismiss()
                   }

                   Status.LOADING -> {
                       progressDialog.show()
                   }
               }
           })*/

        dashboardViewModel.applyFilterModel.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    mDataBinding.numberTxt.text = it.data?.size.toString()
                    loadList = it.data!!
                    progressDialog.dismiss()
                }
                Status.LOADING -> {
                    progressDialog.show()

                }
                Status.ERROR -> {
                    progressDialog.dismiss()
                }
            }
        }
        )

    }

    companion object {
        private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1

        @JvmStatic
        fun newInstance(param1: LoadsByDistanceResponse?) =
            DashboardFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM1, param1)
                }
            }
    }

}