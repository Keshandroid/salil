package com.dispachio.data.model.response

import android.os.Parcelable
import com.dispachio.data.model.response.loadSearch.LoadsByDistanceResponse
import com.dispachio.data.model.response.loadSearch.LoadsByIdResponse
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PODResponse(
    @SerializedName("ledgerId")
    val ledgerId: Int? = null,
    @SerializedName("loadId")
    val loadId: Int? = null,
    @SerializedName("price")
    val price: Int? = null,
    @SerializedName("carrierPrice")
    val carrierPrice: Double? = null,
    @SerializedName("truckingUserId")
    val truckingUserId: Int? = null,
    @SerializedName("carrierUserId")
    val carrierUserId: Int? = null,
    @SerializedName("transactionId")
    val transactionId: Int? = null,
    @SerializedName("paymentStatusId")
    val paymentStatusId: Int? = 0,
    @SerializedName("loads")
    val loads: LoadsByIdResponse? = null,
    @SerializedName("paymentStatus")
    val paymentStatus: PaymentStatus? = null
): Parcelable
