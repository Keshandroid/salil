package com.dispachio.ui.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dispachio.applicationint.TempUserData

/**
 * Created by 1000292 on 06-10-2020
 *
 * PUNE.
 */

abstract class BaseViewModel : ViewModel() {

    val mCartCountLiveData by lazy { MutableLiveData<Int>().also {
        TempUserData.getCartCount()
    } }

    fun setLiveCartCount(count:Int){
        TempUserData.setCount(count)
        mCartCountLiveData.value=TempUserData.getCartCount()
    }

}