package com.dispachio.applicationint.di



import com.dispachio.BuildConfig
import com.dispachio.applicationint.AuthInterceptor
import com.dispachio.utils.SharedPreference
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by 1000292 on 06-10-2020
 *
 * PUNE.
 */

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
/*    fun provideBaseUrl(): String = "http://dispachio-env.eba-tupzqbqx.us-east-2.elasticbeanstalk.com/"*/
//    fun provideBaseUrl(): String = "http://despachiodevelopement-env.eba-2dqwcpy7.us-east-2.elasticbeanstalk.com/"

    fun provideBaseUrl(): String = "http://dispachio-env.eba-bpnqrp9x.us-east-2.elasticbeanstalk.com/"


    @Provides
    @Singleton
    fun provideOkHttpClient(sharedPreference: SharedPreference): OkHttpClient = OkHttpClient
        .Builder().apply {
            addInterceptor(AuthInterceptor(sharedPreference))
            if (BuildConfig.DEBUG) {
                addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
            }
        }
        .connectTimeout(10, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .build()

    @Singleton
    @Provides
    fun providesRetrofit(okHttpClient: OkHttpClient, BASE_URL: String): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .build()


    /*@Singleton
    @Provides
    fun provideAemApi(retrofit: Retrofit): AEMApi = retrofit.create(AEMApi::class.java)


    @Singleton
    @Provides
    fun provideMFDb(@ApplicationContext context: Context): MFDatabase =
        MFDatabase.getDatabase(context)*/

}