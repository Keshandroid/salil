package com.dispachio.ui.viewmodel

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.dispachio.data.model.request.ApplyForLoadRequest
import com.dispachio.data.model.request.MileStoneRequest
import com.dispachio.data.model.request.PODRequest
import com.dispachio.data.model.response.ApplyForLoadResponse
import com.dispachio.data.model.response.LedgerListResponse
import com.dispachio.data.model.response.PODResponse
import com.dispachio.data.model.response.loadSearch.LoadListResponse
import com.dispachio.data.model.response.loadSearch.LoadsByDistanceResponse
import com.dispachio.data.model.response.loadSearch.LoadsByIdResponse
import com.dispachio.data.model.response.loadSearch.MileStoneResponse
import com.dispachio.ui.base.BaseViewModel
import com.dispachio.ui.repository.DashboardRepository
import com.dispachio.utils.NetworkHelper
import com.example.dispachio.common.ResponseData
import com.example.dispachio.common.setError
import com.example.dispachio.common.setLoading
import com.example.dispachio.common.setSuccess
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber

class DashboardViewModel @ViewModelInject constructor(
    private val dashBordRepository: DashboardRepository,
    networkHelper: NetworkHelper
) : BaseViewModel() {

    private var _text = MutableLiveData<String>().apply {
        value = "This is Profile page"
    }
    val text: LiveData<String> = _text

    fun setCurentValueFragment(fragmentName: String) {
        _text.value = fragmentName
    }

    val loadByIdModel = MutableLiveData<ResponseData<LoadsByIdResponse>>()
    fun getLoadByUserId() {
        loadByIdModel.setLoading(null)
        viewModelScope.launch(Dispatchers.IO) {
            dashBordRepository.loadByIDCall(
                { success -> loadByIdModel.setSuccess(success) },
                { error -> loadByIdModel.setError(error) }
            )
        }
    }

    val loadListModel = MutableLiveData<ResponseData<LoadListResponse>>()
    fun getLoadListIdCall(id: Int) {
        loadListModel.setLoading(null)
        viewModelScope.launch(Dispatchers.IO) {
            dashBordRepository.getLoadListIdCall(
                id,
                { success -> loadListModel.setSuccess(success) },
                { error -> loadListModel.setError(error) }
            )
        }
    }

    val milestonesModel = MutableLiveData<ResponseData<MileStoneResponse>>()
    fun milestonesCall(req: MileStoneRequest) {
        milestonesModel.setLoading(null)
        Log.e("miestone_req====>" , Gson().toJson(req))
        viewModelScope.launch(Dispatchers.IO) {
            dashBordRepository.milestonesCall(
                req,
                { success -> milestonesModel.setSuccess(success) },
                { error -> milestonesModel.setError(error) }
            )
        }
    }

    val getLoadsByDistanceModel = MutableLiveData<ResponseData<LoadsByDistanceResponse>>()
    fun getLoadsByDistanceCall(
        latitude: Double,
        longitude: Double,
        distance: Int,
    ) {
        getLoadsByDistanceModel.setLoading(null)
        viewModelScope.launch(Dispatchers.IO) {
            dashBordRepository.getLoadsByDistanceCall(
                latitude, longitude, distance,
                { success -> getLoadsByDistanceModel.setSuccess(success) },
                { error -> getLoadsByDistanceModel.setError(error) }
            )
        }
    }

    val applyFilterModel = MutableLiveData<ResponseData<LoadsByDistanceResponse>>()
    fun applyFilter(
        latitude: Double?,
        longitude: Double?,
        distance: Int?,
        pickupDate: String?,
        deliveryDate: String?,
        containerId: Int?,
        loadTypeId: Int?,
        deliveryTypeId: Int?,
        minRate: Int?,
        maxRate: Int?
    ) {
        applyFilterModel.setLoading(null)
        viewModelScope.launch(Dispatchers.IO) {
            dashBordRepository.applyForFilter(
                latitude,
                longitude,
                distance,
                pickupDate,
                deliveryDate,
                containerId,
                loadTypeId,
                deliveryTypeId,
                minRate,
                maxRate,
                { success -> applyFilterModel.setSuccess(success) },
                { error -> applyFilterModel.setError(error) }
            )
        }
    }

    val applyForLoadsModel = MutableLiveData<ResponseData<ApplyForLoadResponse>>()
    fun applyForLoads(request: ApplyForLoadRequest) {
        applyForLoadsModel.setLoading(null)
        viewModelScope.launch(Dispatchers.IO) {
            dashBordRepository.applyForLoads(
                request,
                { success -> applyForLoadsModel.setSuccess(success) },
                { error -> applyForLoadsModel.setError(error) }
            )
        }
    }

    val createPodModel = MutableLiveData<ResponseData<PODResponse>>()
    fun createPOD(request: PODRequest) {
        createPodModel.setLoading(null)
        viewModelScope.launch(Dispatchers.IO) {
            dashBordRepository.createPOD(
                request,
                { success -> createPodModel.setSuccess(success) },
                { error -> createPodModel.setError(error) }
            )
        }
    }

    val getAllLedgerListModel = MutableLiveData<ResponseData<LedgerListResponse>>()
    fun getAllLedgerList(request: Int) {
        getAllLedgerListModel.setLoading(null)
        viewModelScope.launch(Dispatchers.IO) {
            dashBordRepository.getAllLedgerList(
                request,
                { success -> getAllLedgerListModel.setSuccess(success) },
                { error -> getAllLedgerListModel.setError(error) }
            )
        }
    }

    val awaitingApprovalLoadsModel = MutableLiveData<ResponseData<LoadsByDistanceResponse>>()
    val activeLoadsModel = MutableLiveData<ResponseData<LoadsByDistanceResponse>>()
    fun getApprovalLoads(
        userId: Int,
        loadStatusId: Int,
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            if (loadStatusId == 3) {
                dashBordRepository.getApprovalLoads(
                    userId, loadStatusId,
                    { success -> awaitingApprovalLoadsModel.setSuccess(success) },
                    { error -> awaitingApprovalLoadsModel.setError(error) }
                )
            } else if (loadStatusId == 4) {

                Log.d("Req_data===>","$userId, $loadStatusId")
                dashBordRepository.getApprovalLoads(
                    userId, loadStatusId,
                    { success -> activeLoadsModel.setSuccess(success) },
                    { error -> activeLoadsModel.setError(error) }
                )
            }
        }
    }


    fun getName(): String? {
        return dashBordRepository.getName()
    }

    fun isRegistrationCompleted(): Boolean {

        return dashBordRepository.isRegistrationCompleted()
    }

    fun savePicturePath(picturePath: String?) {
        dashBordRepository.savePicturePath(picturePath)
    }

    fun getPicturePath(): String? {
        return dashBordRepository.getPicturePath()
    }

    fun savePicturePath1(picturePath: String?) {
        dashBordRepository.savePicturePath1(picturePath)
    }

    fun getPicturePath1(): String? {
        return dashBordRepository.getPicturePath1()
    }

    fun savePicturePath2(picturePath: String?) {
        dashBordRepository.savePicturePath2(picturePath)
    }

    fun getPicturePath2(): String? {
        return dashBordRepository.getPicturePath2()
    }

    fun getUserId(): String? {
        return dashBordRepository.getUserId()
    }

    fun getEmailID():String{
        return dashBordRepository.getEmailId()
    }

    fun getPassword(): String {
        return dashBordRepository.getPassword()
    }

    fun getContactNumber(): String {
        return dashBordRepository.getContactNumber()
    }


}