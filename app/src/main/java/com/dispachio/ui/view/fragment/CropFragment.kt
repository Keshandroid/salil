package com.dispachio.ui.view.fragment

import android.app.Dialog
import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.dispachio.R
import com.dispachio.databinding.FragmentCropBinding
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.view.activity.DocumentListener
import com.dispachio.ui.view.activity.ImageLoaderActivity
import com.dispachio.utils.*
import com.isseiaoki.simplecropview.callback.CropCallback
import com.isseiaoki.simplecropview.callback.LoadCallback
import com.isseiaoki.simplecropview.callback.SaveCallback
import timber.log.Timber
import java.io.IOException

class CropFragment : BaseFragment<FragmentCropBinding>(R.layout.fragment_crop) {

    private var mSourceUri: Uri? = null
    private var mDocumentListener: DocumentListener? = null
    lateinit var mProgressDialog: Dialog
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
    }


    override fun initView() {
        mProgressDialog = Utility.showCommonProgressDialog(mActivity)
        (requireActivity() is ImageLoaderActivity).let {
            mDocumentListener = (activity as ImageLoaderActivity)
        }
        mProgressDialog.show()
        mSourceUri = requireArguments().getParcelable(SELECTED_IMAGE)

        mSourceUri?.let { uri ->
            Glide.with(mActivity).load(uri).listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    mLoadCallback.onError(e)
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    mLoadCallback.onSuccess()
                    return false
                }
            }).into(mDataBinding.cropImage)
        } ?: kotlin.run {
            mProgressDialog.dismiss()
        }

        mDataBinding.tvDone.setSafeOnClickListener {
            mProgressDialog.show()
            mDataBinding.cropImage.crop(mSourceUri).execute(mCropCallback)
        }

        mDataBinding.tvRetake.setSafeOnClickListener {
            mDocumentListener?.retake(requireArguments().getInt(OPERATION_CODE, PICK_GALLERY_IMAGE))
        }
    }


    /**
     *  LoadCallback
     *  for load pick image from gallery or camera
     */
    private val mLoadCallback: LoadCallback = object : LoadCallback {
        override fun onSuccess() {
            Timber.d("Successfully load image ")
            mProgressDialog.dismiss()
        }

        override fun onError(e: Throwable) {
            Timber.e(e)
            mProgressDialog.dismiss()
        }
    }

    /**
     * CropCallback
     * callback when cropping completes
     */
    private val mCropCallback: CropCallback = object : CropCallback {
        override fun onSuccess(cropped: Bitmap) {
            Timber.d("Successfully cropped image")
            mSourceUri?.let { uri ->
                when (requireArguments().getInt(OPERATION_CODE)) {
                    PICK_GALLERY_IMAGE -> {
                        Utility.getCacheImagePath(
                            mActivity,
                            "${System.currentTimeMillis()}".plus(FILE_TYPE),
                            cropped
                        )?.let {
                            mSaveCallback.onSuccess(it)
                        } ?: kotlin.run {
                            mSaveCallback.onError(IOException())
                        }
                    }
                    PICK_CAMERA_IMAGE -> {
                        Utility.exifRotation(Utility.getFileProviderFilePath(mActivity, uri))
                            .let { angle ->
                                Utility.getCacheImagePath(
                                    mActivity,
                                    Utility.queryName(context?.contentResolver!!, mSourceUri!!)
                                        ?: "",
                                    if (angle == 0) {
                                        cropped
                                    } else {
                                        Bitmap.createBitmap(
                                            cropped,
                                            0,
                                            0,
                                            cropped.width,
                                            cropped.height,
                                            Matrix().apply {
                                                postRotate(angle.toFloat())
                                            },
                                            true
                                        )
                                    }
                                ).apply {
                                    mSaveCallback.onSuccess(this)
                                } ?: kotlin.run {
                                    mSaveCallback.onError(IOException())
                                }
                            }
                    }
                    else -> {
                        Timber.e("Something went wrong! wile saving image")
                    }
                }

            }
            mProgressDialog.dismiss()
        }

        override fun onError(e: Throwable) {
            Timber.e(e)
            mProgressDialog.dismiss()
        }
    }

    /**
     * SaveCallback
     * callback when cropped image was saved
     */
    private val mSaveCallback: SaveCallback = object : SaveCallback {
        override fun onSuccess(uri: Uri?) {
            Timber.d("Successfully save cropped image")
            mDocumentListener?.done(uri)
            mProgressDialog.dismiss()
        }

        override fun onError(e: Throwable?) {
            Timber.e(e)
            mProgressDialog.dismiss()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mProgressDialog.dismiss()
    }

    companion object {
        @JvmStatic
        fun newInstance() = CropFragment()
    }

}
