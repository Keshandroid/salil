package com.dispachio.ui.base


import android.os.Build
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.dispachio.R


/**
 * Created by 1000292 on 06-10-2020
 *
 * PUNE.
 */
abstract class BaseActivity<DB : ViewDataBinding>(@LayoutRes val layoutRes: Int) :
    AppCompatActivity() {

    protected lateinit var mDataBinding: DB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDataBinding = DataBindingUtil.setContentView(this, layoutRes)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor =
                ContextCompat.getColor(applicationContext, R.color.dsp_dark_three)
        }
        initView()
    }

    abstract fun initView()
    fun setUpToolBar(toolbar: Toolbar, toolBarTitle: String, textView: AppCompatTextView) {
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        textView.text = toolBarTitle
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            setDisplayShowTitleEnabled(false)
        }
    }

}