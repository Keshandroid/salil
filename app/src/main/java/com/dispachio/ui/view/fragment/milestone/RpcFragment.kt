package com.dispachio.ui.view.fragment.milestone

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dispachio.R
import com.dispachio.data.model.request.MileStoneRequest
import com.dispachio.data.model.response.loadSearch.LoadsByDistanceResponseItem
import com.dispachio.databinding.ReportPortConditionsBinding
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.base.addFragments
import com.dispachio.ui.base.toast
import com.dispachio.ui.view.activity.NavigationActivity
import com.dispachio.ui.viewmodel.DashboardViewModel
import com.dispachio.utils.Utility
import com.dispachio.utils.setSafeOnClickListener
import com.example.dispachio.common.Status


private const val ARG_PARAM1 = "param1"

/**
 * A simple [Fragment] subclass.
 * Use the [RpcFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RpcFragment : BaseFragment<ReportPortConditionsBinding>(R.layout.report_port_conditions) {

    private val dashboardViewModel: DashboardViewModel by activityViewModels()
    private var freshBatteryData: LoadsByDistanceResponseItem? = null
    protected var localBroadcastManager: LocalBroadcastManager? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        localBroadcastManager = activity?.let { LocalBroadcastManager.getInstance(it) }

        arguments?.let {
            freshBatteryData = it.getParcelable(ARG_PARAM1)!!
        }
    }


    companion object {

        private const val LOCATION_PERMISSION_REQUEST_CODE = 1

        @JvmStatic
        fun newInstance(param1: LoadsByDistanceResponseItem?) =
            RpcFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM1, param1)
                }
            }
    }


    override fun initView() {



        mDataBinding.confirmBtn.setOnClickListener { view: View? ->


            var path = dashboardViewModel.getPicturePath()
            var userid = dashboardViewModel.getUserId()
            var fileName: String? = null

            if (path != null) {
                fileName = userid + "_rpc1_" + System.currentTimeMillis()
                Utility.sendProfileImage(mActivity, path, fileName)
            }

            if (!mDataBinding.a1.isSelected && !mDataBinding.a2.isSelected && !mDataBinding.a3.text.isNullOrEmpty()) {
                var mileStoneRequest = MileStoneRequest()
                mileStoneRequest.milestoneStatusId = 2
                mileStoneRequest.question1 = 1

                val ans1 = when (mDataBinding.a1.selectedItemPosition) {
                    0 -> {
                        1
                    }
                    1 -> {
                        2
                    }
                    2 -> {
                        3
                    }
                    3 -> {
                        4
                    }
                    else -> 0
                }


                mileStoneRequest.answer1 = ans1
                mileStoneRequest.question2 = 2
                val ans2 = when (mDataBinding.a2.selectedItemPosition) {
                    0 -> {
                        5
                    }
                    1 -> {
                        6
                    }
                    2 -> {
                        7
                    }
                    3 -> {
                        8
                    }
                    else -> 0
                }
                mileStoneRequest.answer2 = ans2
                mileStoneRequest.question5 = 3
                mileStoneRequest.answer5 = mDataBinding.a3.text.toString()
                mileStoneRequest.firstImageName = fileName
                mileStoneRequest.latitude = freshBatteryData?.pickupLatitude
                mileStoneRequest.longitude = freshBatteryData?.pickupLongitude
                mileStoneRequest.curDate = freshBatteryData?.pickupDate
                mileStoneRequest.curTime = freshBatteryData?.pickupTime
                mileStoneRequest.loadId = freshBatteryData?.loadId
                dashboardViewModel.milestonesCall(mileStoneRequest)

                initObserver()
            } else {
                mActivity.toast(mActivity.resources.getString(R.string.please_enter_all_detail))
            }
        }
        mDataBinding.uploadImgBtn.setSafeOnClickListener {
            activity?.let { it1 -> Utility.openGallery(it1) }
        }
        setView()
    }

    private fun setView() {
        mDataBinding.data = freshBatteryData
    }

    private fun initObserver() {
        dashboardViewModel.milestonesModel.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    /*mActivity.addFragments(
                        MilestoneFragment.newInstance(freshBatteryData, 2),
                        R.id.dashboardContainer,
                        true
                    )*/
                    notifyData(freshBatteryData,2)
                    (activity as NavigationActivity).onBackPressed()
                }
                Status.ERROR -> {
                    mActivity.toast(it.message.toString())
                }
            }
        })
    }

    protected fun notifyData(freshBatteryData: LoadsByDistanceResponseItem?, i: Int) {
        val intent = Intent(NOTIFY_MILESTONE_FRAGMENT)
        intent.putExtra(ARG_PARAM1,i)
        localBroadcastManager?.sendBroadcast(intent)
    }
}