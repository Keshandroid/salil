package com.dispachio.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.dispachio.R
import com.dispachio.ui.view.activity.NavigationActivity

class ForegroundService: Service() {
    var FOREGROUND_SERVICE_ID = 101

     public var START_ACTION = "com.here.android.service.fs.action.start"
     var STOP_ACTION = "com.here.android.service.fs.action.stop"

    private val CHANNEL = "default"
    override fun onBind(intent: Intent?): IBinder? {
        // Used only in case of bound services.

        // Used only in case of bound services.
        return null
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onCreate() {
        super.onCreate()
        initChannels(this.applicationContext)
    }

    private fun initChannels(applicationContext: Context?) {
        if (Build.VERSION.SDK_INT < 26) {
            return
        }
        val channel = NotificationChannel(
            CHANNEL, "Foreground channel",
            NotificationManager.IMPORTANCE_DEFAULT
        )
        channel.description = "Channel for foreground service"
        var notificationManager = this.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent!!.action == START_ACTION) {
            val notificationIntent = Intent(this, NavigationActivity::class.java)
            notificationIntent.action = Intent.ACTION_MAIN
            notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER)
            val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)
            val notification =
                NotificationCompat.Builder(this.applicationContext, CHANNEL)
                    .setContentTitle("Guidance")
                    .setContentText("Guidance in progress ...")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .setLocalOnly(true)
                    .build()
            startForeground(FOREGROUND_SERVICE_ID, notification)
        } else if (intent!!.action == STOP_ACTION) {
            stopForeground(true)
            stopSelf()
        }

        return START_NOT_STICKY
    }
}