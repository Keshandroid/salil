package com.dispachio.ui.di

import com.dispachio.ui.base.BaseActivityModule
import com.dispachio.data.ApiCalls
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import retrofit2.Retrofit


@Module(includes = [BaseActivityModule::class])
@InstallIn(ActivityComponent::class)
object LoginRegistrationModule {
@Provides
fun provideLoginRegisterApi(retrofit: Retrofit): ApiCalls =
    retrofit.create(ApiCalls::class.java)
}