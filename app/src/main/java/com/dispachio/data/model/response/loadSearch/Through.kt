package com.dispachio.data.model.response.loadSearch


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Through(
    @SerializedName("milestoneStatusId")
    val milestoneStatusId: Int,
    @SerializedName("name")
    val name: String
) : Parcelable