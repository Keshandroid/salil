package com.dispachio.ui.view.fragment

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.RecyclerView
import com.dispachio.R
import com.dispachio.data.model.response.loadSearch.LoadsByDistanceResponse
import com.dispachio.data.model.response.loadSearch.LoadsByDistanceResponseItem
import com.dispachio.databinding.FragmentLoadListBinding
import com.dispachio.databinding.FragmentTabBinding
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.base.addFragments
import com.dispachio.ui.base.replaceFragment
import com.dispachio.ui.base.toast
import com.dispachio.ui.view.activity.NavigationActivity
import com.dispachio.ui.view.adapter.GenericAdapter
import com.dispachio.ui.viewmodel.DashboardViewModel
import com.google.android.gms.maps.MapFragment


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [LoadListFragment1.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoadListFragment : BaseFragment<FragmentLoadListBinding>(R.layout.fragment_load_list) {

    private val dashboardViewModel: DashboardViewModel by activityViewModels()

    private var freshBatteryData: LoadsByDistanceResponse? = null
    private var next_screen: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            freshBatteryData = it.getParcelable(ARG_PARAM1)!!
            next_screen = it.getString(ARG_PARAM2)!!
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }


    companion object {

        @JvmStatic
        fun newInstance(param1: LoadsByDistanceResponse, param2: String) =
            LoadListFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun initView() {
        addDataIntoList()
    }

    private fun addDataIntoList() {

        if (freshBatteryData?.isNotEmpty() != true) {
            mDataBinding.noDataFound.visibility = View.VISIBLE
            mDataBinding.list.visibility = View.GONE
        } else {
            mDataBinding.noDataFound.visibility = View.GONE
            mDataBinding.list.visibility = View.VISIBLE
            mDataBinding.list.adapter = mTabAdapter
            setDataToAdapter()
        }
    }

    private fun setDataToAdapter() {
        freshBatteryData?.let { mTabAdapter.setItems(it) }
        mTabAdapter.notifyDataSetChanged()
    }


    private val mTabAdapter by lazy {
        object : GenericAdapter<LoadsByDistanceResponseItem>() {
            lateinit var data: LoadsByDistanceResponseItem
            override fun getLayoutId(position: Int, obj: LoadsByDistanceResponseItem): Int {
                data = obj
                return R.layout.fragment_tab
            }

            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                val inflater = LayoutInflater.from(view.context)
                val itemBinding = FragmentTabBinding.inflate(inflater)
                val lp = ConstraintLayout.LayoutParams(
                    ConstraintLayout.LayoutParams.MATCH_PARENT,
                    ConstraintLayout.LayoutParams.WRAP_CONTENT
                )

                lp.setMargins(16, 25, 16, 0)
                itemBinding.root.layoutParams = lp

                return TabViewHolder(itemBinding)
            }
        }
    }

    inner class TabViewHolder(private val viewBinding: FragmentTabBinding) :
        RecyclerView.ViewHolder(
            viewBinding.root
        ), GenericAdapter.Binder<LoadsByDistanceResponseItem> {


        override fun bind(data: LoadsByDistanceResponseItem) {

            viewBinding.tests = data

            if (data.deliveryType.typeValue.contains("One")) {
                viewBinding.imageView7.visibility = View.GONE
                viewBinding.imageView8.visibility = View.GONE
            } else if (data.deliveryType.typeValue.contains("Round")) {
                viewBinding.imageView7.visibility = View.VISIBLE
                viewBinding.imageView8.visibility = View.VISIBLE
            }
            viewBinding.btnDetail.setOnClickListener {

                /*       dashboardViewModel.getLoadByUserId()

                dashboardViewModel.loadByIdModel.observe(viewLifecycleOwner,
                    {
                        when (it.status) {
                            Status.SUCCESS -> {
                                it.data?.let { it ->*/

                if (dashboardViewModel.isRegistrationCompleted()) {
                    if (next_screen.equals(mActivity.resources.getString(R.string.load_details))) {
                        /*mActivity.addFragments(
                            LoadDetailFragment.newInstance(data),
                            R.id.dashboardContainer,
                            true
                        )*/

                        mActivity.replaceFragment(
                            LoadDetailFragment.newInstance(data),
                            R.id.dashboardContainer
                        )

                    } else if (next_screen.equals(
                            mActivity.resources.getString(
                                R.string.start_routing
                            )
                        )
                    ) {

                        Log.d("start_routing","121212")

                        /*mActivity.addFragments(
                            StartRoutingFragment.newInstance(data),
                            R.id.dashboardContainer,
                            true
                        )*/

                        mActivity.replaceFragment(
                            StartRoutingFragment.newInstance(data),
                            R.id.dashboardContainer
                        )

                    } else if (next_screen.equals(
                            mActivity.resources.getString(
                                R.string.awaiting_approval
                            )
                        )
                    ) {
                        showAwaitingDialog()
                    }
                } else {
                    mActivity.toast(mActivity.resources.getString(R.string.registration_not_completed))
                    mActivity.addFragments(
                        CompleteRegistration.newInstance(),
                        R.id.dashboardContainer,
                        true
                    )
                }
            }
            /*}
                            Status.ERROR -> {
                                mActivity.toast(it.message.toString())
                            }
                            Status.LOADING -> {
                            }
                        }
                    */

        }
    }

    private fun showAwaitingDialog() {
        val layoutInflater =
            mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val builder = AlertDialog.Builder(mActivity, R.style.Theme_Dialog)
        val layout: View = layoutInflater.inflate(R.layout.awaiting_dialog, null)
        builder.setView(layout)
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 20)
        alertDialog.window!!.setBackgroundDrawable(inset)
        //alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.cancel()
        alertDialog.setCancelable(false)
        alertDialog.show()
        val okButton = layout.findViewById<Button>(R.id.okButton)
        okButton.setOnClickListener { view: View? ->
            mActivity.startActivity(Intent(context, NavigationActivity::class.java))
            mActivity.finish()
            alertDialog.dismiss()
        }

    }

}
