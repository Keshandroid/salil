package com.dispachio.ui.base

import android.R
import android.content.Context
import android.os.Bundle
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.dispachio.ui.view.activity.NavigationActivity
import com.dispachio.utils.Log
import java.lang.Exception


/**
 * Created by 1000292 on 06-10-2020
 *
 * PUNE.
 */
abstract class BaseFragment<DB : ViewDataBinding>(@LayoutRes val layoutRes: Int):Fragment() {

    protected lateinit var mDataBinding:DB

    protected lateinit var mActivity: AppCompatActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context as AppCompatActivity).let {
            this.mActivity=it
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mDataBinding = DataBindingUtil.inflate(inflater, layoutRes, container, false)
        mDataBinding.root.isClickable=true
        return mDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try {
            //(activity as NavigationActivity).disableSideNavigation()
        }catch (e: Exception){
            e.printStackTrace()
        }
        initView()
    }

    abstract fun initView()
    }