package com.dispachio.ui.view.activity


import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import androidx.activity.viewModels
import androidx.fragment.app.FragmentManager
import com.amazonaws.mobile.client.AWSMobileClient
import com.dispachio.R
import com.dispachio.databinding.ActivityMainBinding
import com.dispachio.ui.base.BaseActivity
import com.dispachio.ui.base.addFragments
import com.dispachio.ui.view.fragment.SplashScreenFragment
import com.dispachio.ui.viewmodel.LoginRegisterViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class SplashAndLoginActivity : BaseActivity<ActivityMainBinding>(R.layout.activity_main),
    FragmentManager.OnBackStackChangedListener {
    private val loginViewModel: LoginRegisterViewModel by viewModels()

    override fun initView() {
        try {
            AWSMobileClient.getInstance().initialize(this).execute()
        } catch (e: Exception) {
            // Log.i("Exception", e.getMessage());
        }
         addFragments(SplashScreenFragment.newInstance(), R.id.logingContainer, true)
    }

    override fun onBackStackChanged() {

    }

    private val GET_FROM_GALLERY = 3

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GET_FROM_GALLERY && resultCode == RESULT_OK && null != data) {
            val selectedImage: Uri? = data.data
            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
            val cursor: Cursor? =
                selectedImage?.let {
                    this.contentResolver.query(
                        it,
                        filePathColumn,
                        null,
                        null,
                        null
                    )
                }
            cursor?.moveToFirst()
            val columnIndex: Int? = cursor?.getColumnIndex(filePathColumn[0])
            val picturePath: String? = columnIndex?.let { cursor.getString(it) }

            when {
                loginViewModel.getPicturePath().isNullOrEmpty() -> {
                    loginViewModel.savePicturePath(picturePath)
                }
                loginViewModel.getPicturePath1().isNullOrEmpty() -> {
                    loginViewModel.savePicturePath1(picturePath)
                }
                loginViewModel.getPicturePath2().isNullOrEmpty() -> {
                    loginViewModel.savePicturePath2(picturePath)
                }
            }
        }
    }

}