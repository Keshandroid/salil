package com.dispachio.applicationint

/**
 * Created by 1000292 on 23-11-2020
 *
 * PUNE.
 */

/**
 * Class will removed once all api working with dynamic environment
 */
object TempUserData {
    const val CUSTOMER_ID=26414
    const val USER_NAME="8328368497"
    const val AUTH_TOKEN="token 78a966b4c9f304f66c9434ea0aa2be66d2cd3d3f"
    const val BANK_ID=10394
    const val AMCFUND_ID = 30
    const val BASE_URL = "https://bfsd.dev.bfsgodirect.com"

    private var mCartCount=0
    fun getCartCount():Int{
        return this.mCartCount
    }
    fun setCount(count:Int){
        this.mCartCount=count
    }
}