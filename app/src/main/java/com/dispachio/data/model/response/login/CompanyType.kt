package com.dispachio.data.model.response.login


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CompanyType(
    @SerializedName("CompanyTypeId")
    val companyTypeId: Int,
    @SerializedName("typeName")
    val typeName: String
) : Parcelable