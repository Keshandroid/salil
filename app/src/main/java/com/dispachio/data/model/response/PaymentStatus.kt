package com.dispachio.data.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PaymentStatus (
    @SerializedName("paymentStatusId")
    val paymentStatusId: Int? = null,
    @SerializedName("paymentStatusValue")
    val paymentStatusValue: String? = null
): Parcelable