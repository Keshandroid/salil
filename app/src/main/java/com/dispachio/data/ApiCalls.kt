package com.dispachio.data

import com.dispachio.data.model.request.LoginRequest
import com.dispachio.data.model.request.MileStoneRequest
import com.dispachio.data.model.request.RegistrationRequestModel
import com.dispachio.data.model.response.RegisterResponceModel
import com.dispachio.data.model.response.loadSearch.LoadListResponse
import com.dispachio.data.model.response.loadSearch.LoadsByDistanceResponse
import com.dispachio.data.model.response.loadSearch.LoadsByIdResponse
import com.dispachio.data.model.response.loadSearch.MileStoneResponse
import com.dispachio.data.model.response.login.LoginResponse
import com.dispachio.ui.base.BaseResponseModel
import retrofit2.Call
import retrofit2.http.*

interface ApiCalls {

    //login
    @POST("carrierUser/login")
    fun loginCall(@Body request: LoginRequest):
            Call<BaseResponseModel<LoginResponse>>

    //forgot password
    @POST("carrierUser/forgotPassword")
    fun forgotPasswordCall(@Body request: LoginRequest):
            Call<BaseResponseModel<LoginResponse>>

    //First Carrier registration
    @POST("carrierUser/registration")
    fun firstRgsCall(@Body request: RegistrationRequestModel):
            Call<BaseResponseModel<RegisterResponceModel>>

    //Complete Carrier registration
    @PUT("carrierUser/completeRegistration")
    fun completeRegisCall(@Body request: RegistrationRequestModel):
            Call<BaseResponseModel<RegisterResponceModel>>


    //Update carrier profile data
    @PUT("carrierUser/updateUserDetails")
    fun updateCarrierProfileData(@Body request: RegistrationRequestModel):
            Call<BaseResponseModel<RegisterResponceModel>>


}