package com.dispachio.data

import com.dispachio.data.model.request.ApplyForLoadRequest
import com.dispachio.data.model.request.MileStoneRequest
import com.dispachio.data.model.request.PODRequest
import com.dispachio.data.model.response.ApplyForLoadResponse
import com.dispachio.data.model.response.LedgerListResponse
import com.dispachio.data.model.response.PODResponse
import com.dispachio.data.model.response.loadSearch.LoadListResponse
import com.dispachio.data.model.response.loadSearch.LoadsByDistanceResponse
import com.dispachio.data.model.response.loadSearch.LoadsByIdResponse
import com.dispachio.data.model.response.loadSearch.MileStoneResponse
import com.dispachio.ui.base.BaseResponseModel
import retrofit2.Call
import retrofit2.http.*

interface DashboardApi {

    //milestones api call
    @POST("milestone")
    fun milestonesCall(@Body request: MileStoneRequest):
            Call<BaseResponseModel<MileStoneResponse>>

    //load by id
    @GET("loads/{id}")
    fun getLoadByIdCall(@Path("id") id: Int?):
            Call<BaseResponseModel<LoadsByIdResponse>>

    //load all list
    @GET("loads/list")
    fun getLoadListIdCall(@Query("truckingUserId") id: Int?):
            Call<BaseResponseModel<LoadListResponse>>


    //get loads by distance
    @GET("loads/listByDistance")
    fun getLoadsByDistanceCall(
        @Query("latitude") latitude: Double?,
        @Query("longitude") longitude: Double?,
        @Query("distance") distance: Int?
    ): Call<BaseResponseModel<LoadsByDistanceResponse>>

    //get ApprovalLoads
    @GET("loads/carrier/getApprovalLoads")
    fun getApprovalLoads(
        @Query("userId") userId: Int?,
        @Query("loadStatusId") loadStatusId: Int?
    ): Call<BaseResponseModel<LoadsByDistanceResponse>>

    //apply for loads
    @POST("loads/applyForLoad")
    fun applyForLoads(@Body request: ApplyForLoadRequest):
            Call<BaseResponseModel<ApplyForLoadResponse>>


    //create pod
    @POST("pod")
    fun createPOD(@Body request: PODRequest):
            Call<BaseResponseModel<PODResponse>>

    //filter
    @GET("loads/filter")
    fun applyFilter(
        @Query("latitude") latitude: Double?,
        @Query("longitude") longitude: Double?,
        @Query("distance") distance: Int?,
        @Query("pickupDate") pickupDate: String?,
        @Query("deliveryDate") deliveryDate: String?,
        @Query("containerId") containerId: Int?,
        @Query("loadTypeId") loadTypeId: Int?,
        @Query("deliveryTypeId") deliveryTypeId: Int?,
        @Query("minRate") minRate: Int?,
        @Query("maxRate") maxRate: Int?
    ):
            Call<BaseResponseModel<LoadsByDistanceResponse>>


    //ledger list by id
    @GET("carrierUser/ledger/list")
    fun getAllLedgerList(@Query("userId") userId: Int?):
            Call<BaseResponseModel<LedgerListResponse>>
}