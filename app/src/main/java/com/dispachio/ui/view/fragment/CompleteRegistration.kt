package com.dispachio.ui.view.fragment

import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.dispachio.R
import com.dispachio.data.model.request.RegistrationRequestModel
import com.dispachio.databinding.CompleteProfilePg1Binding
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.base.addFragments
import com.dispachio.ui.base.toast
import com.dispachio.utils.CompanyType
import com.dispachio.utils.setSafeOnClickListener


/**
 * A simple [Fragment] subclass.
 * Use the [CompleteRegistration.newInstance] factory method to
 * create an instance of this fragment.
 */
class CompleteRegistration :
    BaseFragment<CompleteProfilePg1Binding>(R.layout.complete_profile_pg1) {


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CompleteRegistration.
         */
        @JvmStatic
        fun newInstance() =
            CompleteRegistration().apply {
            }
    }

    override fun initView() {
        mDataBinding.companyNameEdt3.adapter = ArrayAdapter(
            mActivity,
            android.R.layout.simple_spinner_item,
            CompanyType.values()
        )
        mDataBinding.companyNameEdt3.setSelection(0)

        mDataBinding.nextBtn.setSafeOnClickListener {
            if (!mDataBinding.companyNameEdt.text.isNullOrEmpty() && !mDataBinding.editTextCity.text.isNullOrEmpty()
                && !mDataBinding.editTextState.text.isNullOrEmpty()
                && !mDataBinding.editTextZip.text.isNullOrEmpty() && !mDataBinding.caEdt.text.isNullOrEmpty()
                && !mDataBinding.fedralTaxIdEdt.text.isNullOrEmpty() && !mDataBinding.edtEntityId.text.isNullOrEmpty()
                && !mDataBinding.mc.text.isNullOrEmpty() && !mDataBinding.usdotEdt.text.isNullOrEmpty()
                && !mDataBinding.scacEdt.text.isNullOrEmpty() && !mDataBinding.agentNameEdt.text.isNullOrEmpty()
                && !mDataBinding.editTelephone.text.isNullOrEmpty() && !mDataBinding.editTextEmail.text.isNullOrEmpty()
            ) {

                var requestModel = RegistrationRequestModel()
                requestModel.companyName = mDataBinding.companyNameEdt.text.toString()
                requestModel.companyCity = mDataBinding.editTextCity.text.toString()
                requestModel.companyState = mDataBinding.editTextState.text.toString()
                requestModel.companyZipCode = mDataBinding.editTextZip.text.toString()
                requestModel.companyCANumber = mDataBinding.caEdt.text.toString()
                requestModel.companyMCNumber = mDataBinding.mc.text.toString()
                requestModel.companyUsDoot = mDataBinding.usdotEdt.text.toString()
                requestModel.entityId = mDataBinding.edtEntityId.text.toString()
                requestModel.companyScac = mDataBinding.scacEdt.text.toString()
                requestModel.companyInsuranceAgentName = mDataBinding.agentNameEdt.text.toString()
                requestModel.companyInsuranceAgentEmail = mDataBinding.editTextEmail.text.toString()
                requestModel.companyInsuranceAgentContactNo =
                    mDataBinding.editTelephone.text.toString()
                requestModel.companyTypeId = mDataBinding.companyNameEdt3.selectedItemId.toInt()+1

                requestModel.federalTaxId = mDataBinding.fedralTaxIdEdt.text.toString()

                mActivity.addFragments(
                    CompleteRegistration2Fragment.newInstance(requestModel),
                    R.id.dashboardContainer
                )

            } else {
                mActivity.toast(mActivity.resources.getString(R.string.please_complete_profile))
            }
        }

    }
}