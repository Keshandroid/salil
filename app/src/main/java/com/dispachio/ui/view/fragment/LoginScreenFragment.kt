package com.dispachio.ui.view.fragment

import android.Manifest
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import com.dispachio.R
import com.dispachio.data.model.request.LoginRequest
import com.dispachio.databinding.FragmentLoginScreenBinding
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.base.addFragments
import com.dispachio.ui.base.toast
import com.dispachio.ui.view.activity.NavigationActivity
import com.dispachio.ui.viewmodel.LoginRegisterViewModel
import com.dispachio.utils.setSafeOnClickListener
import com.example.dispachio.common.Status
import javax.inject.Inject


class LoginScreenFragment :
    BaseFragment<FragmentLoginScreenBinding>(R.layout.fragment_login_screen) {

    private val loginRegisterViewModel: LoginRegisterViewModel by activityViewModels()

    @Inject
    lateinit var progressDialog: Dialog

    override fun initView() {
        initObserver()
        getLocationPermission()
        checkStorageLocation()
        mDataBinding.loginBtn.setSafeOnClickListener {
            var request = LoginRequest()
            request.email = mDataBinding.editTextEnterName.text.toString()
            request.password = mDataBinding.editTextPassword.text.toString()
            loginRegisterViewModel.loginCall(request)

        }

        mDataBinding.registerLink.setSafeOnClickListener {
            mActivity.addFragments(
                RegistrationFragment.newInstance(),
                R.id.logingContainer,
                false
            )
        }

        mDataBinding.forgotPwd.setSafeOnClickListener {
            resetDialog()
        }
    }

    private fun checkStorageLocation() {
        /*
       * Request EXTERNAL_STORAGE permission, so that we can get the EXTERNAL_STORAGE of the
       * device. The result of the permission request is handled by a callback,
       * onRequestPermissionsResult.
       */
        if (ContextCompat.checkSelfPermission(
                mActivity.applicationContext,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                mActivity.applicationContext,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            mActivity.toast("Storage permission allowed.")
        } else {
            ActivityCompat.requestPermissions(
                mActivity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE),
                PERMISSIONS_REQUEST_EXTERNAL_STORAGE
            )
        }

    }

    fun initObserver() {

        progressDialog = Dialog(mActivity)
        loginRegisterViewModel.loginResponse.observe(
            viewLifecycleOwner, {
                when (it.status) {
                    Status.SUCCESS -> {
                        progressDialog.dismiss()
                        startActivity(Intent(context, NavigationActivity::class.java))
                       // mActivity.toast(it.message.toString())
                        mActivity.toast("Login Successfully!!")
                        it.data?.let { it1 ->
                        if(mDataBinding.rememberMeCb.isChecked) {
                                loginRegisterViewModel.saveLoginData(
                                    it1.userId.toString(),
                                    it1.password,
                                    it1.isActive,
                                    it1.email,
                                    it1.name,
                                    it1.isProfileCompleted,
                                    it1.contactNumber
                                )
                        }else{
                                loginRegisterViewModel.saveLoginData(
                                    it1.userId.toString(),
                                    null,
                                    it1.isActive,
                                    it1.email,
                                    it1.name,
                                    it1.isProfileCompleted,
                                    it1.contactNumber
                                )
                            }
                        }
                    }
                    Status.LOADING -> {
                        progressDialog.show()
                    }
                    Status.ERROR -> {
                        progressDialog.dismiss()
                        mActivity.toast(it.message.toString())
                    }

                }
            }
        )

        loginRegisterViewModel.resetResponse.observe(
            viewLifecycleOwner, {
                when (it.status) {
                    Status.SUCCESS -> {
                        progressDialog.dismiss()
                        mActivity.toast("Password reset successfully")
                    }
                    Status.LOADING -> {
                        progressDialog.show()
                    }
                    Status.ERROR -> {
                        progressDialog.dismiss()
                        mActivity.toast(it.message.toString())
                    }

                }
            }
        )
    }

    companion object {
        private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
        private const val PERMISSIONS_REQUEST_EXTERNAL_STORAGE = 2

        @JvmStatic
        fun newInstance() =
            LoginScreenFragment()
                .apply {

                }
    }


    private fun resetDialog() {
        val layoutInflater =
            mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val builder = AlertDialog.Builder(mActivity, R.style.Theme_Dialog)
        val layout: View = layoutInflater.inflate(R.layout.alert, null)
        builder.setView(layout)
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 20)
        alertDialog.window!!.setBackgroundDrawable(inset)
        //alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.cancel()
        alertDialog.show()
        val okButton = layout.findViewById<Button>(R.id.okButton)
        val cancelButton = layout.findViewById<Button>(R.id.cancelButton)
        val emailId = layout.findViewById<EditText>(R.id.editTextEmailID)
        val password = layout.findViewById<EditText>(R.id.editTextPassword)
        cancelButton.visibility = View.VISIBLE
        okButton.setOnClickListener { view: View? ->
            if (!emailId.text.isNullOrBlank() && !password.text.isNullOrBlank()) {
                resetCall(emailId.text.toString(), password.text.toString())
            } else {
                mActivity.toast(mActivity.resources.getString(R.string.enter_detail))
            }
            alertDialog.dismiss()
        }
        cancelButton.setOnClickListener { view: View? ->
            alertDialog.dismiss()
        }

    }

    private fun resetCall(emailId: String, password: String) {
        var request = LoginRequest()
        request.email = emailId
        request.password = password
        loginRegisterViewModel.resetCall(request)
    }

    private var locationPermissionGranted = false


    private fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(
                mActivity.applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            locationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(
                mActivity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    // [START maps_current_place_on_request_permissions_result]
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        locationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    locationPermissionGranted = true
                }
            }
        }
    }

}