package com.dispachio.data.model.response
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import com.dispachio.data.model.response.login.CompanyType
import com.google.gson.annotations.SerializedName

@Parcelize
data class RegisterResponceModel(
    @SerializedName("additionalInsuredFileName")
    val additionalInsuredFileName: String? = null,
    @SerializedName("commissionPercentage")
    val commissionPercentage: String? = null,
    @SerializedName("companyCANumber")
    val companyCANumber: String? = null,
    @SerializedName("companyCity")
    val companyCity: String? = null,
    @SerializedName("companyInsuranceAgentContactNo")
    val companyInsuranceAgentContactNo: String? = null,
    @SerializedName("companyInsuranceAgentEmail")
    val companyInsuranceAgentEmail: String? = null,
    @SerializedName("companyInsuranceAgentName")
    val companyInsuranceAgentName: String? = null,
    @SerializedName("companyMCNumber")
    val companyMCNumber: String? = null,
    @SerializedName("companyName")
    val companyName: String? = null,
    @SerializedName("companyScac")
    val companyScac: String? = null,
    @SerializedName("companyState")
    val companyState: String? = null,
    @SerializedName("companyType")
    val companyType: CompanyType? = null,
    @SerializedName("companyTypeId")
    val companyTypeId: String? = null,
    @SerializedName("companyUsDoot")
    val companyUsDoot: String? = null,
    @SerializedName("companyZipCode")
    val companyZipCode: String? = null,
    @SerializedName("contactNumber")
    val contactNumber: String? = null,
    @SerializedName("email")
    val email: String? = null,
    @SerializedName("entityId")
    val entityId: String? = null,
    @SerializedName("federalTaxId")
    val federalTaxId: String? = null,
    @SerializedName("insuranceFileName")
    val insuranceFileName: String? = null,
    @SerializedName("isActive")
    val isActive: Boolean? = null, // true
    @SerializedName("isEmailVerified")
    val isEmailVerified: String? = null, // null
    @SerializedName("isProfileCompleted")
    val isProfileCompleted: Boolean? = null, // false
    @SerializedName("lastLoginTime")
    val lastLoginTime: String? = null, // null
    @SerializedName("loginIp")
    val loginIp: String? = null, // null
    @SerializedName("name")
    val name: String? = null, // carrier4
    @SerializedName("password")
    val password: String? = null, // car2
    @SerializedName("registeredOn")
    val registeredOn: String? = null, // 2021-07-14 16:40:23.260
    @SerializedName("registrantionFileName")
    val registrantionFileName: String? = null, // null
    @SerializedName("truckLicenceNumber")
    val truckLicenceNumber: String? = null, // null
    @SerializedName("truckMake")
    val truckMake: String? = null, // null
    @SerializedName("truckYear")
    val truckYear: String? = null, // null
    @SerializedName("userId")
    val userId: Int? = null, // 7
    @SerializedName("userName")
    val userName: String? = null // car2
) : Parcelable