package com.dispachio.data.model.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PODRequest(
    @SerializedName("loadId")
    var loadId: Int? = null,
    @SerializedName("userId")
    var userId: Int? = null
) : Parcelable
