package com.dispachio.ui.view.fragment.milestone

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dispachio.R
import com.dispachio.data.model.request.MileStoneRequest
import com.dispachio.data.model.response.loadSearch.LoadsByDistanceResponseItem
import com.dispachio.databinding.HookedToLoadBinding
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.base.addFragments
import com.dispachio.ui.base.toast
import com.dispachio.ui.view.activity.NavigationActivity
import com.dispachio.ui.viewmodel.DashboardViewModel
import com.dispachio.utils.Utility
import com.dispachio.utils.setSafeOnClickListener
import com.example.dispachio.common.Status
import kotlinx.android.synthetic.main.hooked_to_load.*

private const val ARG_PARAM1 = "param1"


/**
 * A simple [Fragment] subclass.
 * Use the [HtlFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HtlFragment : BaseFragment<HookedToLoadBinding>(R.layout.hooked_to_load),
    RadioGroup.OnCheckedChangeListener {

    private val dashboardViewModel: DashboardViewModel by activityViewModels()
    private var freshBatteryData: LoadsByDistanceResponseItem? = null
    protected var localBroadcastManager: LocalBroadcastManager? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        localBroadcastManager = activity?.let { LocalBroadcastManager.getInstance(it) }


        arguments?.let {
            freshBatteryData = it.getParcelable(ARG_PARAM1)!!
        }
    }


    companion object {

        private const val LOCATION_PERMISSION_REQUEST_CODE = 1

        @JvmStatic
        fun newInstance(param1: LoadsByDistanceResponseItem?) =
            HtlFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM1, param1)
                }
            }
    }

    override fun initView() {

        mDataBinding.tiresRg.setOnCheckedChangeListener(this)
        mDataBinding.chassisRg.setOnCheckedChangeListener(this)
        mDataBinding.roadableRg.setOnCheckedChangeListener(this)
        mDataBinding.isEmptyRg.setOnCheckedChangeListener(this)

        mDataBinding.roadableRg.getChildAt(0).isEnabled = false
        mDataBinding.roadableRg.getChildAt(1).isEnabled = false


        mDataBinding.confirmBtn.setOnClickListener { view: View? ->


            var tiresRgButton =
                mDataBinding.tiresRg.findViewById(mDataBinding.tiresRg.checkedRadioButtonId) as? RadioButton
            var chassisRgButton =
                mDataBinding.chassisRg.findViewById(mDataBinding.chassisRg.checkedRadioButtonId) as? RadioButton
            var roadableRgButton =
                mDataBinding.roadableRg.findViewById(mDataBinding.roadableRg.checkedRadioButtonId) as? RadioButton
            var isEmptyRgButton =
                mDataBinding.isEmptyRg.findViewById(mDataBinding.isEmptyRg.checkedRadioButtonId) as? RadioButton

            if (tiresRgButton != null && tiresRgButton.isChecked && chassisRgButton != null
                && chassisRgButton.isChecked && roadableRgButton != null &&
                roadableRgButton.isChecked && isEmptyRgButton != null &&
                isEmptyRgButton.isChecked && !mDataBinding.a8.text.isNullOrEmpty()
            ) {
                var path = dashboardViewModel.getPicturePath()
                var userid = dashboardViewModel.getUserId()

                var fileName: String? = null
                if (path != null) {
                    fileName = userid + "_htl1_" + System.currentTimeMillis()
                    Utility.sendProfileImage(mActivity, path, fileName)
                }
                var mileStoneRequest = MileStoneRequest()
                mileStoneRequest.question1 = 4
                var ans4: Int = 0
                if (tiresRgButton.text.toString() == "Good") {
                    ans4 = 9
                } else if (tiresRgButton.text.toString() == "Damage") {
                    ans4 = 10
                }
                mileStoneRequest.answer1 = ans4
                mileStoneRequest.question2 = 5
                var ans5: Int = 0
                if (chassisRgButton.text.toString() == "Good") {
                    ans5 = 11
                } else if (chassisRgButton.text.toString() == "Damage") {
                    ans5 = 12
                }
                mileStoneRequest.answer2 = ans5
                mileStoneRequest.question3 = 6
                var ans6: Int = 0
                if (roadableRgButton.text.toString() == "Yes") {
                    ans6 = 13
                } else if (roadableRgButton.text.toString() == "No") {
                    ans6 = 14
                }
                mileStoneRequest.answer3 = ans6
                mileStoneRequest.question4 = 7
                var ans7: Int = 0
                if (isEmptyRgButton.text.toString() == "Yes") {
                    ans7 = 15
                } else if (isEmptyRgButton.text.toString() == "No") {
                    ans7 = 16
                }
                mileStoneRequest.answer4 = ans7

                mileStoneRequest.question5 = 8
                mileStoneRequest.answer5 = mDataBinding.a8.text.toString()
                mileStoneRequest.milestoneStatusId = 3
                mileStoneRequest.firstImageName = fileName
                mileStoneRequest.latitude = freshBatteryData?.pickupLatitude
                mileStoneRequest.longitude = freshBatteryData?.pickupLongitude
                mileStoneRequest.curDate = freshBatteryData?.pickupDate
                mileStoneRequest.curTime = freshBatteryData?.pickupTime
                mileStoneRequest.loadId = freshBatteryData?.loadId


                dashboardViewModel.milestonesCall(mileStoneRequest)
                initObserver()
            } else {
                mActivity.toast(mActivity.resources.getString(R.string.please_enter_all_detail))
            }
        }

        mDataBinding.uploadImgBtn.setSafeOnClickListener {
            Utility.openGallery(mActivity)
        }
        setView()
    }

    private fun setView() {
        mDataBinding.data = freshBatteryData
    }

    private fun initObserver() {
        dashboardViewModel.milestonesModel.observe(viewLifecycleOwner, {

            when (it.status) {
                Status.SUCCESS -> {
                    /*mActivity.addFragments(
                        MilestoneFragment.newInstance(freshBatteryData, 3),
                        R.id.dashboardContainer,
                        true
                    )*/

                    notifyData(freshBatteryData,3)
                    (activity as NavigationActivity).onBackPressed()

                }
                Status.ERROR -> {
                    mActivity.toast(it.message.toString())
                }
            }
        })
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        var radioButton = group?.findViewById(group.checkedRadioButtonId) as? RadioButton
        when (radioButton) {
            badChassis -> {
                setRoadable()
            }
            goodChassis -> {
                setRoadable()
            }
            goodTires -> {
                setRoadable()
            }
            badTires -> {
                setRoadable()
            }
        }
    }

    private fun setRoadable() {
        if (mDataBinding.goodChassis.isChecked && mDataBinding.goodTires.isChecked) {
            mDataBinding.yesRoadable.isChecked = true
            mDataBinding.notRoadable.isChecked = false
        }

        if (mDataBinding.badChassis.isChecked && mDataBinding.badTires.isChecked) {
            mDataBinding.notRoadable.isChecked = true
            mDataBinding.yesRoadable.isChecked = false
        }

        if (mDataBinding.badChassis.isChecked && mDataBinding.goodTires.isChecked ||
            mDataBinding.goodChassis.isChecked && mDataBinding.badTires.isChecked
        ) {
            mDataBinding.notRoadable.isChecked = true
            mDataBinding.yesRoadable.isChecked = false
        }
    }

    protected fun notifyData(freshBatteryData: LoadsByDistanceResponseItem?, i: Int) {
        val intent = Intent(NOTIFY_MILESTONE_FRAGMENT)
        intent.putExtra(ARG_PARAM1,i)
        localBroadcastManager?.sendBroadcast(intent)
    }
}