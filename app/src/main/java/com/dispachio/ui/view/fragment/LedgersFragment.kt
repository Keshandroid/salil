package com.dispachio.ui.view.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.RecyclerView
import com.dispachio.R
import com.dispachio.data.model.response.LedgerListResponse
import com.dispachio.data.model.response.PODResponse
import com.dispachio.data.model.response.loadSearch.LoadsByDistanceResponse
import com.dispachio.ui.base.BaseFragment
import com.dispachio.ui.viewmodel.DashboardViewModel
import com.dispachio.databinding.FragmentLedgerBinding
import com.dispachio.databinding.LedgerContentBinding
import com.dispachio.ui.base.toast
import com.dispachio.ui.view.activity.SplashAndLoginActivity
import com.dispachio.ui.view.adapter.GenericAdapter
import com.dispachio.utils.DeleteCache
import com.dispachio.utils.SharedPreference
import com.dispachio.utils.setSafeOnClickListener
import com.example.dispachio.common.Status
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LedgersFragment : BaseFragment<FragmentLedgerBinding>(R.layout.fragment_ledger) {

    private val dashboardViewModel: DashboardViewModel by activityViewModels()

    override fun initView() {

        initObserver()
        val userId = dashboardViewModel.getUserId()
        userId?.toInt()?.let { dashboardViewModel.getAllLedgerList(it) }

        mDataBinding.include.logout.setSafeOnClickListener {
            mActivity.startActivity(Intent(mActivity, SplashAndLoginActivity::class.java))
            SharedPreference(requireContext()).deleteAllSharedPreference()
            DeleteCache(mActivity).deleteCache()
            mActivity.toast("You've logged out completely.")
        }
    }

    private fun initObserver() {
        dashboardViewModel.getAllLedgerListModel.observe(viewLifecycleOwner,{
            when(it.status){
                Status.ERROR->{
                    mActivity.toast(it.message.toString())
                }
                Status.SUCCESS->{
                    setDataToView(it.data)
                }
            }
        })
    }

    private fun setDataToView(listData: LedgerListResponse?) {
       // viewBinding.data = data

        if (listData?.isNotEmpty() != true) {
            mDataBinding.noDataFound.visibility = View.VISIBLE
            mDataBinding.list.visibility = View.GONE
        } else {
            mDataBinding.noDataFound.visibility = View.GONE
            mDataBinding.list.visibility = View.VISIBLE
            mDataBinding.list.adapter = mTabAdapter
            setDataToAdapter(listData)
        }
    }

    private fun setDataToAdapter(data: LedgerListResponse) {
        data.let { mTabAdapter.setItems(it) }
        mTabAdapter.notifyDataSetChanged()
    }

    private val mTabAdapter by lazy {
        object : GenericAdapter<PODResponse>() {
            lateinit var data: PODResponse
            override fun getLayoutId(position: Int, obj: PODResponse): Int {
                data = obj
                return R.layout.ledger_content
            }

            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                val inflater = LayoutInflater.from(view.context)
                val itemBinding = LedgerContentBinding.inflate(inflater)
                val lp = ConstraintLayout.LayoutParams(
                    ConstraintLayout.LayoutParams.MATCH_PARENT,
                    ConstraintLayout.LayoutParams.WRAP_CONTENT
                )

                lp.setMargins(16, 25, 16, 0)
                itemBinding.root.layoutParams = lp

                return TabViewHolder(itemBinding)
            }
        }
    }

    inner class TabViewHolder(private val viewBinding: LedgerContentBinding) :
        RecyclerView.ViewHolder(
            viewBinding.root
        ), GenericAdapter.Binder<PODResponse> {


        override fun bind(data: PODResponse) {

            viewBinding.data = data

        }
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            LedgersFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}