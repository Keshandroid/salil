package com.dispachio.data.model.request
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

@Parcelize
data class RegistrationRequestModel(
    @SerializedName("userId")
    var userID: Int? = null,
    @SerializedName("companyCANumber")
    var companyCANumber: String? = null,
    @SerializedName("companyCity")
    var companyCity: String? = null,
    @SerializedName("companyInsuranceAgentContactNo")
    var companyInsuranceAgentContactNo: String? = null,
    @SerializedName("companyInsuranceAgentEmail")
    var companyInsuranceAgentEmail: String? = null,
    @SerializedName("companyInsuranceAgentName")
    var companyInsuranceAgentName: String? = null,
    @SerializedName("companyMCNumber")
    var companyMCNumber: String? = null,
    @SerializedName("companyName")
    var companyName: String? = null,
    @SerializedName("companyScac")
    var companyScac: String? = null,
    @SerializedName("companyState")
    var companyState: String? = null,
    @SerializedName("companyTypeId")
    var companyTypeId: Int? = null,
    @SerializedName("companyUsDoot")
    var companyUsDoot: String? = null,
    @SerializedName("companyZipCode")
    var companyZipCode: String? = null,
    @SerializedName("contactNumber")
    var contactNumber: String? = null,
    @SerializedName("email")
    var email: String? = null,
    @SerializedName("entityId")
    var entityId: String? = null,
    @SerializedName("federalTaxId")
    var federalTaxId: String? = null,
    @SerializedName("isActive")
    var isActive: Boolean?=  null,
    @SerializedName("isProfileCompleted")
    var isProfileCompleted: Boolean? = null, // false
    @SerializedName("loginIp")
    var loginIp: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("password")
    var password: String? = null,
    @SerializedName("truckLicenceNumber")
    var truckLicenceNumber: String? = null,
    @SerializedName("truckMake")
    var truckMake: String? = null,
    @SerializedName("truckYear")
    var truckYear: Int? = null,
    @SerializedName("userName")
    var userName: String? = null,
    @SerializedName("firstImageName")
    var firstImageName: String? = null,
    @SerializedName("secondImageName")
    var secondImageName: String? = null,
    @SerializedName("thirdImageName")
    var thirdImageName: String? = null
) : Parcelable