package com.dispachio.applicationint


import com.dispachio.utils.Log
import com.dispachio.utils.SharedPreference
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by 1000292 on 06-11-2020
 *
 * PUNE.
 */
@Singleton
class AuthInterceptor @Inject constructor (private val sharedPreference: SharedPreference) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val requestBuilder = originalRequest.newBuilder().apply {
            header("Content-Type", "application/json")
            header("Authorization", sharedPreference.getToken()?:"")
        }

        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}