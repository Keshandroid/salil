package com.dispachio.ui.base

import com.google.gson.annotations.SerializedName

/**
 * Created by 1000292 on 27-11-2020
 *
 * PUNE.
 */
data class BaseResponseModel<P>(
    @field:SerializedName("entity")
    val entity: P?=null
):ParentBaseResponseModel()