package com.dispachio.ui.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.dispachio.data.model.request.LoginRequest
import com.dispachio.ui.base.BaseViewModel
import com.dispachio.utils.NetworkHelper
import com.dispachio.data.model.request.RegistrationRequestModel
import com.dispachio.data.model.response.RegisterResponceModel
import com.dispachio.data.model.response.login.LoginResponse
import com.dispachio.ui.repository.LoginRepository
import com.example.dispachio.common.ResponseData
import com.example.dispachio.common.setError
import com.example.dispachio.common.setLoading
import com.example.dispachio.common.setSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginRegisterViewModel @ViewModelInject constructor(
    private val loginRepository: LoginRepository,
    networkHelper: NetworkHelper
) : BaseViewModel() {
    val registrationResponse = MutableLiveData<ResponseData<RegisterResponceModel>>()
    fun setRegistrationData(registrationRequestModel: RegistrationRequestModel) {
        registrationResponse.setLoading(null)
        viewModelScope.launch(Dispatchers.IO) {
            loginRepository.setRegistrationDetail(registrationRequestModel,
                { success -> registrationResponse.setSuccess(success) },
                { error -> registrationResponse.setError(error) }
            )
        }
    }

    val completeRegisResponse = MutableLiveData<ResponseData<RegisterResponceModel>>()
    fun completeRegisCall(registrationRequestModel: RegistrationRequestModel) {
        completeRegisResponse.setLoading(null)
        viewModelScope.launch(Dispatchers.IO) {
            loginRepository.completeRegisCall(registrationRequestModel,
                { success -> completeRegisResponse.setSuccess(success) },
                { error -> completeRegisResponse.setError(error) }
            )
        }
    }

    val updateCarrierProfResponse = MutableLiveData<ResponseData<RegisterResponceModel>>()
    fun updateCarrierProfileData(registrationRequestModel: RegistrationRequestModel) {
        updateCarrierProfResponse.setLoading(null)
        viewModelScope.launch(Dispatchers.IO) {
            loginRepository.updateCarrierProfileData(registrationRequestModel,
                { success -> updateCarrierProfResponse.setSuccess(success) },
                { error -> updateCarrierProfResponse.setError(error) }
            )
        }
    }

    val loginResponse = MutableLiveData<ResponseData<LoginResponse>>()
    fun loginCall(request: LoginRequest) {
        loginResponse.setLoading(null)
        viewModelScope.launch(Dispatchers.IO) {
            loginRepository.loginCAll(request,
                { success -> loginResponse.setSuccess(success) },
                { error -> loginResponse.setError(error) }
            )
        }
    }


    val resetResponse = MutableLiveData<ResponseData<LoginResponse>>()
    fun resetCall(request: LoginRequest) {
        resetResponse.setLoading(null)
        viewModelScope.launch(Dispatchers.IO) {
            loginRepository.resetCall(request,
                { success -> resetResponse.setSuccess(success) },
                { error -> resetResponse.setError(error) }
            )
        }
    }

    fun saveLoginData(
        userId: String,
        password: String?,
        active: Boolean,
        email: String?,
        name: String?,
        profileCompleted: Boolean,
        contactNumber: String?
    ) {
        loginRepository.saveUserData(
            userId,
            password,
            active,
            email,
            name,
            profileCompleted,
            contactNumber
        )
    }

    fun getUserID(): String? {
        return loginRepository.getUserId()
    }

    fun otpSent(otp: String?) {
        loginRepository.otpSent(otp)
    }

    fun otpChecked():String? {
        return loginRepository.otpChecked()
    }

    fun savePicturePath(picturePath: String?) {
        loginRepository.savePicturePath(picturePath)
    }

    fun getPicturePath(): String? {
        return loginRepository.getPicturePath()
    }

    fun savePicturePath1(picturePath: String?) {
        loginRepository.savePicturePath1(picturePath)
    }

    fun getPicturePath1(): String? {
        return loginRepository.getPicturePath1()
    }

    fun savePicturePath2(picturePath: String?) {
        loginRepository.savePicturePath2(picturePath)
    }

    fun getPicturePath2(): String? {
        return loginRepository.getPicturePath2()
    }

    fun isProfileCompleted(b: Boolean) {
        loginRepository.isProfileCompleted(b)
    }

}